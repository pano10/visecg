This is VisECG program main repository. Check build_instructions.txt and .submodule file if you wish to develop additional functionality or contact developers to provide you with more information.

LICENCE: see LICENCE file
Using "Creative Commons Attribution NonCommercial"
Short description: 
 -you CAN modify and distribute this project
 -you MAY NOT use this project for commercial use, sublicese, place warranty od hold liable original authors
 - you MUST include copyright, state changes and give credit to original authors



VERSIONS: 
1.1.3
 - move project to a new repository
 - refresh overview when files change on HDD
 - user can choose number of annotations on report

1.1.2 
 - new installer (substitutes installer and updater)
 - report fixes (estimation of beat coverage, absolute timestamps added...)

1.1.1
 - Converter fixes and updates (sampling freqency read not estimated,...)
 - ms time saving precision
 - v2 & v3 of beat detector
 - new report styling and report file (XML)
 - some more localization fixes

1.1.0
 - installer and loader updates
 - bugfixes

1.0.3
 - Gui changes (folder overview buttons moved to taskbar)
 - moving resources to "resources" folder
 - licence update

1.0.2
 - Addition of loader
 - -w debug mode to converter added
 - some localization issues fixed
 - general stabilizy and bugfixes

1.0.1 
 - Addition of annotation channel
 - Additional statistic information
 - show EULA on first opening
 - absolute time when viewing files
 - big converter changes (updates and bugfixes)
 - --version option added to VisECG
 - general stabilizy and bugfixes

1.0.0 - first public release version 
 - basic PDF report functionality
 - reviewing and editing of NEKGE files


---------OLD PCARD versioning-------
1.6b 	ustvarjeno novo poddrevo (ta direktorij)
1.6b	Brez uradne spremembe verzije; dodatki za generiranje 12-kanalnega EKGja, ki jih je spisal Ivan Tomasic, in so delno nepreverjeni. 
	Tudi izklopljeno branje parametrov iz ukazne vrstice in tako onemogoene skripte
1.6c 	hitreje odpiranje dolgih fajlov (na zaetku se izrie do max prvih 30s podatkov), pohitrena koda za odpiranje HL7x (*.rep) fajlov
1.6d	makro za analizo MobECG datotek
	makroji iz ukazne vrstice so spet omogoceni
--- prva verzija MobEcg razlicice --- 
1.7	Fourierjeva transformacija do 10 Hz
