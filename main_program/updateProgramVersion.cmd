:: Use before builidng a version  of PCARD analysis,
:: it gets tag from git and uses it as a version in the about dialog

set /p bar="#define __GITVERSION__ "<nul > gitVersion.h "
git describe --abbrev=4 --dirty --always --tags >> gitVersion.h
:: to add closing quote
for /f "tokens=*" %%a in (gitVersion.h) do (
  echo %%a > gitVersion.h ^"
)