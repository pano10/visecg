// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "docForm.h"
#include "main.h"
#include "gitVersion.h"
#include <vcl\Clipbrd.hpp>

#include "fftForm1.h"
#include "brsForm1.h"
#include "averager.h"
#include "coherenceForm.h"
#include "measurementDataDlg.h"
#include "measurForm.h"
#include "recForm.h"
#include "dlgChannelProperties.h"
#include "filterParamsDialog.h"
#include "heartBeatParamForm.h"
#include "dlgViewY.h"
#include "TMecgOpenForm.h"
#include <set>
#include <algorithm>
#include <vector>
#include <iomanip>
#include <fstream>

#include <math.h>
#include "ini.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TDocumentForm *DocumentForm;
//---------------------------------------------------------------------------
__fastcall TDocumentForm::TDocumentForm(TComponent* Owner)
	: TForm(Owner)
{
	dirty = false;
	yScaleMode = LibCore::CMeasurement::yScaleModeAll;
//	trueYscale = false;
  	trueYscale = true;
	scrollbarTrackingEnabled = true;
	nextClickDefinesEventTime = false;
	valueOfEventToAdd = 0;
	fileName = "";
	gridChannelView->Cells[0][0] = "Ime";
	gridChannelView->Cells[1][0] = "V";
	gridChannelView->Cells[2][0] = "Y-zoom";
	gridChannelView->Cells[3][0] = "Odmik";
	gridChannelView->Cells[4][0] = "I";
	gridChannelView->ColWidths[0] = 115;
	gridChannelView->ColWidths[1] = 18;
	gridChannelView->ColWidths[2] = 30;
	gridChannelView->ColWidths[3] = 30;
	gridChannelView->ColWidths[4] = 18;
	comboCursorColor->ItemIndex = 0;
	Chart->Legend->ColorWidth = 40;
	numSelectedChannel = -1;
	mousePosOnChartX = mousePosOnChartY = -1;
	undoStack.resetStack();

	autoCopyToClipboard = false;
    additionalPainter = 0;
}

//---------------------------------------------------------------------------

//load measuremet from a file
void TDocumentForm::LoadMeasurement(const AnsiString _fileName)
{
	LibCore::CMeasurement::FileType fileType = LibCore::CMeasurement::other;
	AnsiString fileExtension = LibCore::extensionOfFilename(_fileName);
	//decode fileType from filename extension
	if (fileExtension != "")
	{
		if (fileExtension.AnsiCompareIC("nekg") == 0)
			fileType = LibCore::CMeasurement::nevroEKG;
		else if (fileExtension.AnsiCompareIC("nekge") == 0)
			fileType = LibCore::CMeasurement::nevroEKGeditable;
		else if (fileExtension.AnsiCompareIC("txt") == 0)
			fileType = LibCore::CMeasurement::txt;
		else if (fileExtension.AnsiCompareIC("afd") == 0)
			fileType = LibCore::CMeasurement::finapress;
		else if (fileExtension.AnsiCompareIC("dat") == 0)
			fileType = LibCore::CMeasurement::dekg_binary;
		else if (fileExtension.AnsiCompareIC("ecg") == 0)
			fileType = LibCore::CMeasurement::mecg_binary;
		else if (fileExtension.AnsiCompareIC("asc") == 0)
			fileType = LibCore::CMeasurement::mecg_ascii;
		else if (fileExtension.AnsiCompareIC("txt") == 0)
			fileType = LibCore::CMeasurement::dekg_ascii;
		else if (fileExtension.AnsiCompareIC("ppi") == 0)
			fileType = LibCore::CMeasurement::ppi_ascii;
		else if (fileExtension.AnsiCompareIC("rep") == 0)
			fileType = LibCore::CMeasurement::hl7x;
		else if (fileExtension.AnsiCompareIC("s2") == 0)
			fileType = LibCore::CMeasurement::s2;
	}

	measurement.loadFromFile(_fileName, fileType);

    // when dealing with mecg file, ask which channels to keep
    if ((fileType == LibCore::CMeasurement::mecg_binary) || 
        (fileType == LibCore::CMeasurement::mecg_ascii)  ) 
    {
        MecgOpenForm->NumChannelsLabel->Caption = AnsiString(measurement.getNumChannels());

	    AnsiString settingsFname("nastavitve.ini");
    	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
		    if (Application->ExeName[i] == '\\') {
                settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
	    		break;
            }
    	}
        
        Ini::File ini(settingsFname.c_str());
        std::string privzetiKanali("");
        static const char privzetiKanaliCharStr[] = "MECG privzeti kanali za uvoz";
        ini.loadVar(privzetiKanali, privzetiKanaliCharStr);
        MecgOpenForm->ChannelNumInput->Text = privzetiKanali.c_str();
        MecgOpenForm->SaveSettingsCheckBox->Checked = false;
        
        bool everythingOk = true;
        do {
            if (MecgOpenForm->ShowModal() == mrOk) {
                // ok pressed
                AnsiString channelSelectString = MecgOpenForm->ChannelNumInput->Text.TrimRight();
                std::istringstream iss(channelSelectString.c_str());
                std::set<int> setOfChannels;
                for (int i = 0; i < measurement.getNumChannels(); ++i)
                    setOfChannels.insert(i);
                
                everythingOk = true;
                
                while (iss) {
                    int value;
                    iss >> value;
                    if (!iss.fail()) {
                        // insert zero based index (input is one based)
                        if ((value <= 0) || (setOfChannels.count(value - 1) == 0)) {
                            everythingOk = false;
                            Application->MessageBox(
                                "Napaka pri izbiri kanalov - izbran je neobstoje� kanal (ali pa je isti kanal izbran ve�krat)!", 
                                "Opozorilo", MB_OK);
                            break;
                        } else {
                            setOfChannels.erase(value - 1);
                        }
                    } else {
                        break;
                    }
                }

                if (everythingOk) {
                    if (!iss.eof()) {
                        // all the numbers read were ok, but could not read the whole text -> non-numerical characters were present
                        everythingOk = false;
                        Application->MessageBox("Napaka pri izbiri kanalov - uporaba ne-numeri�nih znakov!", "Opozorilo", MB_OK);
                    } else {
                        if (MecgOpenForm->SaveSettingsCheckBox->Checked == true) {
                            privzetiKanali = MecgOpenForm->ChannelNumInput->Text.c_str();
                            if (!ini.storeVar(privzetiKanali, privzetiKanaliCharStr) || !ini.save())
                                Application->MessageBox("Napaka pri shranjevanju nastavitev. Izbrane nastavitve ne bodo shranjene!", "Opozorilo", MB_OK);
                        }
                            
                        // no error in channel select string, filter channels 
                        for (std::set<int>::reverse_iterator it = setOfChannels.rbegin(); it != setOfChannels.rend(); ++it)
                            measurement.deleteChannel(*it);
                    }
                } else {
                    // error in channel select string, show dialog again
                }
            } else {
                // cancel pressed
                while (measurement.getNumChannels() > 0)
                    measurement.deleteChannel(0);
            }
        } while (!everythingOk);
    }

    if (measurement.getNumChannels() == 0) {
        // nothing was loaded
        Close();
    } else {
    
        //load & set window components properties
	    Caption = (measurement.getMeasurementName().Length() > 0 ?
		    		measurement.getMeasurementName() : AnsiString("untitled"))
			    	+ " [" + _fileName + "]";
        if(fileType == LibCore::CMeasurement::txt) // because we don't know the name of the file if converted to multiple files
            Caption = (measurement.getMeasurementName().Length() > 0 ?
		    		measurement.getMeasurementName() : AnsiString("untitled"))
                    + " [" + (FileExists(_fileName+".nekge")? _fileName+".nekge]":_fileName+"_part_1.nekge]") ;
    	xLeft = 0;
	    xRight = measurement.getDuration();
        // in order to keep loading time of very long files to minimum (preparation to draw all the data takes a long time), limit the first view to 60 seconds
        if (xRight > 60)
            xRight = 60;
            
    	numSelectedChannel = -1;
	    for (int i = 0; i < measurement.getNumChannels(); i++)
		    if (measurement[i].viewProps.visible)
    		{
	    		eventChannelSelected = false;
		    	numSelectedChannel = i;
			    break;
	    	}

    	if (numSelectedChannel == -1)
		    for (int i = 0; i < measurement.getNumEventChannels(); i++)
    		if (measurement.getEventChannel(i).viewProps.visible)
	    	{
		    	selectChannel(i, true, false);
			    break;
    		}

        if (numSelectedChannel == -1)
		    for (int i = 0; i < measurement.getNumAnnotationChannels(); i++)
    		if (measurement.getAnnotationChannel(i).viewProps.visible)
	    	{
		    	selectChannel(i, true, true);
			    break;
    		}
            
	    if (numSelectedChannel == -1)
		    selectChannel(0, false, false);

    	refreshWholeWindow();
	    undoStack.resetStack();
    	dirty = false;
	    fileName = _fileName;
        if(fileType == LibCore::CMeasurement::txt) //handle overwriting files
            fileName = (FileExists(_fileName+".nekge")? _fileName+".nekge]":_fileName+"_part_1.nekge]") ;

    }
}

void TDocumentForm::SaveMeasurement(AnsiString _fileName, bool automatic)
{
	AnsiString extension = LibCore::extensionOfFilename(_fileName);
	if (!automatic && extension != "nekge")
	{
		_fileName = AnsiString(_fileName.SubString(1, _fileName.Length()-extension.Length()) + "nekge");
        AnsiString msg("Podatki bodo shranjeni v formatu Nevro-EKG v datoteko '" + _fileName + "'.");
		if (Application->MessageBox(msg.c_str(), "Potrditev shranjevanja", MB_OKCANCEL) == IDCANCEL)
		{
			Application->MessageBox("Datoteka ni shranjena.", "Opozorilo", MB_OK);
			return;
		}
	}
	measurement.saveToFile(_fileName, LibCore::CMeasurement::nevroEKGeditable);
	Caption = (measurement.getMeasurementName().Length() > 0 ?
				measurement.getMeasurementName() : AnsiString("untitled"))
				+ " [" + _fileName + "]";
	fileName = _fileName;
	dirty = false;
}

void TDocumentForm::exportText(const AnsiString _fileName, bool allChannels)
{
	if (allChannels)
		measurement.exportAllSignalChannels(_fileName);
	else if (eventChannelSelected)
		measurement.exportEventChannel(_fileName, numSelectedChannel);
	else
		measurement.exportSignalChannel(_fileName, numSelectedChannel);
}

void TDocumentForm::importText(const AnsiString _fileName)
{
   	if (measurement.importEventIntervals(_fileName)) {
    	dirty = true;
	    selectChannel(measurement.getNumEventChannels()-1, true, false);
   		refreshWholeWindow();
	} 
}

void TDocumentForm::filterChannels(const AnsiString& channelsToFilter) {
    
}

void TDocumentForm::prepareForExcelExport(bool warnAndShow)
{
	LibStdMeasurement::IExcelExporter *excelExporter = LibStdMeasurement::createSpecificExcelExporter(measurement);
	if (excelExporter)
	{
		int oldNumEventCh = measurement.getNumEventChannels();
		excelExporter->prepareForExport();
		delete excelExporter;
        if (warnAndShow)
    		refreshWholeWindow();

		if (measurement.getNumEventChannels() > oldNumEventCh)
		{
			char buf[1000];
			if (measurement.getNumEventChannels() > oldNumEventCh+1)
				sprintf(buf, "Prosim, preverite in po potrebi popravite novo nastale dogodkovne kanale D%d-D%d.",
								oldNumEventCh+1, measurement.getNumEventChannels());
			else
    			sprintf(buf, "Prosim, preverite in po potrebi popravite novo nastali dogodkovni kanal D%d.",
								measurement.getNumEventChannels());
			if (warnAndShow)
                Application->MessageBox(buf, "VisECG", MB_OK);
		}
	} else {
		Application->MessageBox("Izvoz meritev te vrste ni mo�en.", "VisECG", MB_OK);
	}
}

void TDocumentForm::exportToExcel()
{
	LibStdMeasurement::IExcelExporter *excelExporter = LibStdMeasurement::createSpecificExcelExporter(measurement);
	if (excelExporter)
	{
		excelExporter->exportData(LibCore::pathOfFilename(MainForm->exeFile), LibCore::pathOfFilename(fileName));
		delete excelExporter;
		refreshWholeWindow();
	} else {
		Application->MessageBox("Izvoz meritev te vrste ni mo�en.", "VisECG", MB_OK);
	}
}

void TDocumentForm::exportToWmf(const AnsiString& filename) {
    AnsiString resultStr;
    Chart->SaveToMetafileEnh(filename);
}

bool TDocumentForm::editMeasurementData()
{
	dlgMeasurementData->dataToDialog(measurement);
	if (dlgMeasurementData->ShowModal() == mrOk)
	{
		dataFromDlgMeasurementData();
		Caption = (measurement.getMeasurementName().Length() > 0 ?
					measurement.getMeasurementName() : AnsiString("untitled"))
					+ " [" + fileName + "]";
		dirty = true;
		return true;
	} else
		return false;
}

void TDocumentForm::dataFromDlgMeasurementData()
{
	dlgMeasurementData->dataFromDialog(measurement);
	Caption = measurement.getMeasurementName().Length() > 0 ?
				measurement.getMeasurementName() : AnsiString("untitled");
}

void __fastcall TDocumentForm::FormResize(TObject *Sender)
{
	ToolBar->Height = 32;
	Chart->Top = ToolBar->Height;
	gridChannelView->Top = ToolBar->Height;
	gridChannelView->Height = ClientHeight - gridChannelView->Top - 164;
	groupCursors->Top = gridChannelView->Top + gridChannelView->Height + 28;
    lblStartDateTime->Top = gridChannelView->Top + gridChannelView->Height + 8;
    lblStartDateTimeVar->Top = gridChannelView->Top + gridChannelView->Height + 8;
	Chart->Width = ClientWidth - 10 - (btnShowChannelTable->Down ? gridChannelView->Width : 0);
	Chart->Height = ClientHeight - Chart->Top - horzScroll->Height - 6;
	horzScroll->Top = Chart->Top + Chart->Height;
	horzScroll->Width = Chart->Width;
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnZoomClick(TObject *Sender)
{
	zoomOutX(0.5, (xLeft+xRight)/2);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnUnzoomClick(TObject *Sender)
{
	zoomOutX(2, (xLeft+xRight)/2);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::horzScrollChange(TObject *Sender)
{
	if (scrollbarTrackingEnabled)
	{
		double range = xRight - xLeft;
		xLeft = horzScroll->Position/double(horzScroll->Max)*measurement.getDuration();
		xRight = min(xLeft + range, measurement.getDuration());
		xLeft = min(xRight - range, xLeft);
		repaintGraph();
	}
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnViewAllClick(TObject *Sender)
{
	xLeft = 0;
	xRight = measurement.getDuration();
	repaintGraph();
}

void TDocumentForm::repaintGraph(bool genReport)
{
	xLeft = max(0.0, xLeft);
	xRight = min(measurement.getDuration(), xRight);
	if (xRight < xLeft + 10/measurement.getSamplingRate())
	{
		xRight = min(measurement.getDuration(), xLeft + 10/measurement.getSamplingRate());
		xLeft = max(0.0, min(xLeft, xRight - 10/measurement.getSamplingRate()));
	}
	horzScroll->PageSize = horzScroll->Max*(xRight-xLeft)/measurement.getDuration();
	horzScroll->LargeChange = max(10.0, 0.6*horzScroll->Max*(xRight-xLeft)/measurement.getDuration());
	horzScroll->SmallChange = max(1.0, 0.03*horzScroll->Max*(xRight-xLeft)/measurement.getDuration());
	scrollbarTrackingEnabled = false;
	horzScroll->Position = (horzScroll->Max*xLeft/measurement.getDuration()+0.5);
	scrollbarTrackingEnabled = true;
	measurement.refreshGraphValues(Chart, xLeft, xRight, yScaleMode, trueYscale,
							eventChannelSelected, annotationChannelSelected, numSelectedChannel, genReport);
	cursors.removeReferencesToLines();
	cursors.drawCursors(Chart, measurement, eventChannelSelected, annotationChannelSelected, numSelectedChannel);
}

bool TDocumentForm::measure(LibStdMeasurement::StdMeasurementType measurementType)
{
/* //commented so we can remove recForm.cpp
	//set measurement properties, delete any existing channels
	LibStdMeasurement::StdMeasurementProps measurementProps = LibStdMeasurement::getMeasurementProps(measurementType);
	measurement.setSamplingRate(500);

    AnsiString settingsFname("nastavitve.ini");
   	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
	    if (Application->ExeName[i] == '\\') {
            settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
    		break;
        }
  	}
        
    Ini::File ini(settingsFname.c_str());
    std::string commentText = "Meritev opravljena z napravo DMU - data multiplexing unit.";
    ini.loadVar(commentText, "privzeti komentar k meritvi");
    
	measurement.setComment(commentText.c_str());
	measurement.setMeasurementName(measurementProps.measurementNamePrefix);
	while (measurement.getNumChannels())
		measurement.deleteChannel(0);
	recordForm->pMeasurement = &measurement;
    recordForm->measurementType = measurementType;

	//show measurement properties form so that user can enter properties
	if (!editMeasurementData())
	{
		dirty = false;
		return false;
	}
	while (measurement.patientData.name == "" || measurement.patientData.dateOfBirth > Date())
	{
		Application->MessageBox("Pacient mora imeti priimek in ime in mora biti rojen danes ali prej.", "Napaka!", MB_OK);
		if (!editMeasurementData())
		{
			dirty = false;
			return false;
		}
	}
	dirty = false;

deviceStatusCheck: //GOTO label, hehehe
	//get device status
	LibCore::CDeviceStatus deviceStatus;
	deviceStatus.getDeviceStatus(false);
	bool useDevice[4] = {false, false, false, false};

	//select default combinations: channels with data are enabled, those without cannot be enabled
	//if a channel is required for standard measurement but not available, error
	if (deviceStatus.serialPortConnected[0])
	{
		MeasureForm->checkColinDigital->Checked = measurementProps.chCOLINdigital;
		MeasureForm->checkColinDigital->Enabled = true;
	} else {
		MeasureForm->checkColinDigital->Checked = false;
		MeasureForm->checkColinDigital->Enabled = false;
		if (measurementType != LibStdMeasurement::stdmNonstandard && measurementProps.chCOLINdigital)
		{
			if (Application->MessageBox("Zahtevan je kanal 'COLIN digital', vendar naprava ni priklju�ena na vrata 1. Odpravite napako in poskusite ponovno.", "Napaka!", MB_RETRYCANCEL) == IDRETRY)
				goto deviceStatusCheck;
			else
				return false;
		}
	}
	if (deviceStatus.dataAvailable[1])
	{
		MeasureForm->checkPressure->Checked = measurementProps.chPressure;
		MeasureForm->checkPressure->Enabled = true;
		MeasureForm->checkCH2->Checked = measurementProps.chCH2;
		MeasureForm->checkCH2->Enabled = true;
		MeasureForm->checkCH3->Checked = measurementProps.chCH3;
		MeasureForm->checkCH3->Enabled = true;
		MeasureForm->checkCH4->Checked = measurementProps.chCH4;
		MeasureForm->checkCH4->Enabled = true;
	} else {
		MeasureForm->checkPressure->Checked = false;
		MeasureForm->checkPressure->Enabled = false;
		MeasureForm->checkCH2->Checked = false;
		MeasureForm->checkCH2->Enabled = false;
		MeasureForm->checkCH3->Checked = false;
		MeasureForm->checkCH3->Enabled = false;
		MeasureForm->checkCH4->Checked = false;
		MeasureForm->checkCH4->Enabled = false;
		if (measurementType != LibStdMeasurement::stdmNonstandard
			&& (measurementProps.chPressure || measurementProps.chCH2 || measurementProps.chCH3 || measurementProps.chCH3))
		{
			if (Application->MessageBox("Zahtevan je eden od kanalov na vratih 2, ki niso priklju�ena. Odpravite napako in poskusite ponovno.", "Napaka!", MB_RETRYCANCEL) == IDRETRY)
				goto deviceStatusCheck;
			else
				return false;
		}
	}
	if (deviceStatus.dataAvailable[2] )//&& deviceStatus.EKGbatteryVoltage >= deviceStatus.EKGbatteryVoltageCritical)
	{
		MeasureForm->checkEKG->Checked = measurementProps.chEKG;
		MeasureForm->checkEKG->Enabled = true;
		MeasureForm->checkBreathing->Checked = measurementProps.chBreathing;
		MeasureForm->checkBreathing->Enabled = true;
	} else {
		if (deviceStatus.EKGbatteryVoltage < deviceStatus.EKGbatteryVoltageCritical) {
            AnsiString msg("Napetost baterije je prenizka (" + AnsiString(deviceStatus.EKGbatteryVoltage) + " V), zato snemanje signalov EKG in dihanja ni mo�no.");
			Application->MessageBox(msg.c_str(), "Opozorilo", MB_OK);
        }
		MeasureForm->checkEKG->Checked = false;
		MeasureForm->checkEKG->Enabled = false;
		MeasureForm->checkBreathing->Checked = false;
		MeasureForm->checkBreathing->Enabled = false;
		if (measurementType != LibStdMeasurement::stdmNonstandard
			&& (measurementProps.chEKG || measurementProps.chBreathing))
		{
			if (Application->MessageBox("Zahtevan je eden od kanalov na vratih 3, ki niso priklju�ena ali pa imajo prenizko napetost baterije. Odpravite napako in poskusite ponovno.", "Napaka!", MB_RETRYCANCEL) == IDRETRY)
				goto deviceStatusCheck;
			else
				return false;
		}
	}

	if (measurementType == LibStdMeasurement::stdmNonstandard)
	{
		//let user select ports and measurement duration
		if (MeasureForm->ShowModal() != mrOk)
			if (Application->MessageBox("Prekinili ste postopek nove meritve. �elite poskusiti ponovno?", "Opozorilo", MB_YESNO) == IDYES)
				goto deviceStatusCheck;
			else
				return false;
	}

	//use ports that are checked in the MeasureForm (for standard measurement types, this will be exactly the default combination)
	useDevice[0] = MeasureForm->checkColinDigital->Checked;
	useDevice[1] = MeasureForm->checkPressure->Checked || MeasureForm->checkCH2->Checked || MeasureForm->checkCH3->Checked || MeasureForm->checkCH4->Checked;
	useDevice[2] = MeasureForm->checkEKG->Checked || MeasureForm->checkBreathing->Checked;
	useDevice[3] = MeasureForm->checkP4->Checked;
	if (!useDevice[1] && !useDevice[2])
	{
		if (Application->MessageBox("Meritev ne vsebuje nobenega analognega kanala. Preverite, �e so vse naprave vklju�ene, in izberite vsaj enega od teh kanalov.",
								"Napaka!", MB_RETRYCANCEL) == IDRETRY)
			goto deviceStatusCheck;
		else
			return false;
	}

	if (useDevice[2])
	{
		//note battery voltage in measurement comment; warn user if battery low
		measurement.appendComment("\r\nNapetost baterije: za�etna " + AnsiString(deviceStatus.EKGbatteryVoltage) + " V");
		if (deviceStatus.EKGbatteryVoltage < deviceStatus.EKGbatteryVoltageLow) {
            AnsiString msg("Napetost baterije je nizka (" + AnsiString(deviceStatus.EKGbatteryVoltage) + " V).");
			Application->MessageBox(msg.c_str(), "Opozorilo", MB_OK);
        }
	}

	//set autoStop
	if (measurementType == LibStdMeasurement::stdmNonstandard && !MeasureForm->checkManualStop->Checked)
		recordForm->autoStopDuration = MeasureForm->spinDuration->Value*1000;
	else
		recordForm->autoStopDuration = (LibCore::CMeasurement::maxMeasurementLength-10)*1000; //maximum time limit - 10 seconds

	//create destination channels for all enabled ports
	int newChannel;
	recordForm->portChannel[0][0] = -1; //analog port 1 does not exist (because P1 is COLIN digital)
	recordForm->portChannel[0][1] = -1;
	recordForm->portChannel[0][2] = -1;
	recordForm->portChannel[0][3] = -1;

	//port 1: COLIN digital->4 cont. event channels (3 pressures, pulse rate)
	if (useDevice[0])
	{
		newChannel = measurement.addEventChannel("sist tlak", "mmHg");
		recordForm->colinDigitalFirstChannel = newChannel;

		newChannel = measurement.addEventChannel("srednji tlak", "mmHg");
		measurement.getEventChannel(newChannel).reallocTable(recordForm->autoStopDuration/250+100);
		newChannel = measurement.addEventChannel("diast tlak", "mmHg");
		measurement.getEventChannel(newChannel).reallocTable(recordForm->autoStopDuration/250);
		newChannel = measurement.addEventChannel("pulz", "min^-1");
		measurement.getEventChannel(newChannel).reallocTable(recordForm->autoStopDuration/250);
	} else
		recordForm->colinDigitalFirstChannel = -1;

	//port 2: 4 analog->4 double channels (COLIN pressure, 3 x free)
	if (useDevice[1] && MeasureForm->checkPressure->Checked)
	{
		newChannel = measurement.addChannel("tlak COLIN", "mmHg");
		measurement[newChannel].reallocTable(recordForm->autoStopDuration/2+1000);
		recordForm->channelGain[newChannel] = 0.1;
		recordForm->channelRangeMin[newChannel] = 0;
		recordForm->channelRangeMax[newChannel] = 240;
		recordForm->portChannel[1][0] = newChannel;
	} else
		recordForm->portChannel[1][0] = -1;
	if (useDevice[1] && MeasureForm->checkCH2->Checked)
	{
		newChannel = measurement.addChannel("analog ch2", "mV");
		measurement[newChannel].reallocTable(recordForm->autoStopDuration/2+1000);
		recordForm->channelGain[newChannel] = 1;
		recordForm->channelRangeMin[newChannel] = 0;
		recordForm->channelRangeMax[newChannel] = 4095;
		recordForm->portChannel[1][1] = newChannel;
	} else
		recordForm->portChannel[1][1] = -1;
	if (useDevice[1] && MeasureForm->checkCH3->Checked)
	{
		newChannel = measurement.addChannel("analog ch3", "mV");
		measurement[newChannel].reallocTable(recordForm->autoStopDuration/2+1000);
		recordForm->channelGain[newChannel] = 1;
		recordForm->channelRangeMin[newChannel] = 0;
		recordForm->channelRangeMax[newChannel] = 4095;
		recordForm->portChannel[1][2] = newChannel;
	} else
		recordForm->portChannel[1][2] = -1;
	if (useDevice[1] && MeasureForm->checkCH4->Checked)
	{
		newChannel = measurement.addChannel("analog ch4", "mV");
		measurement[newChannel].reallocTable(recordForm->autoStopDuration/2+1000);
		recordForm->channelGain[newChannel] = 1;
		recordForm->channelRangeMin[newChannel] = 0;
		recordForm->channelRangeMax[newChannel] = 4095;
		recordForm->portChannel[1][3] = newChannel;
	} else
		recordForm->portChannel[1][3] = -1;

	//port 3: 4 analog->4 double channels (3 EKG, 1 breathing)
	if (useDevice[2] && MeasureForm->checkEKG->Checked)
	{
		newChannel = measurement.addChannel("EKG I", "mV");
		measurement[newChannel].reallocTable(recordForm->autoStopDuration/2+1000);
		recordForm->channelGain[newChannel] = 1.0/450;
		recordForm->channelRangeMin[newChannel] = -2;
		recordForm->channelRangeMax[newChannel] = 2;
		recordForm->portChannel[2][0] = newChannel;

		newChannel = measurement.addChannel("EKG II", "mV");
		measurement[newChannel].reallocTable(recordForm->autoStopDuration/2+1000);
		recordForm->channelGain[newChannel] = 1.0/450;
		recordForm->channelRangeMin[newChannel] = -2;
		recordForm->channelRangeMax[newChannel] = 2;
		recordForm->portChannel[2][1] = newChannel;

		newChannel = measurement.addChannel("EKG III", "mV");
		measurement[newChannel].reallocTable(recordForm->autoStopDuration/2+1000);
		recordForm->channelGain[newChannel] = 1.0/450;
		recordForm->channelRangeMin[newChannel] = -2;
		recordForm->channelRangeMax[newChannel] = 2;
		recordForm->portChannel[2][2] = newChannel;
	} else {
		recordForm->portChannel[2][0] = -1;
		recordForm->portChannel[2][1] = -1;
		recordForm->portChannel[2][2] = -1;
	}
	if (useDevice[2] && MeasureForm->checkBreathing->Checked)
	{
		newChannel = measurement.addChannel("dihanje", "�C");
		measurement[newChannel].reallocTable(recordForm->autoStopDuration/2+1000);
		recordForm->channelGain[newChannel] = 1;
		recordForm->channelRangeMin[newChannel] = 15;
		recordForm->channelRangeMax[newChannel] = 40;
		recordForm->portChannel[2][3] = newChannel;
	} else
		recordForm->portChannel[2][3] = -1;

	//port 4 disabled for standard measurements
	if (measurementType == LibStdMeasurement::stdmNonstandard && useDevice[3])
		throw LibCore::EKGException(__LINE__, "TDocumentForm::measure: port 4 not supported");
	else {
		recordForm->portChannel[3][0] = -1;
		recordForm->portChannel[3][1] = -1;
		recordForm->portChannel[3][2] = -1;
		recordForm->portChannel[3][3] = -1;
	}

	if (measurementType != LibStdMeasurement::stdmNonstandard || MeasureForm->checkMarkers->Checked)
	{
		//markers: a non-contiguous event channel
		//when user presses 1..9 during measurement, marker is inserted
		newChannel = measurement.addEventChannel("zaznamki", "�t.");
		measurement.getEventChannel(newChannel).reallocTable(recordForm->autoStopDuration/250+100);
		measurement.getEventChannel(newChannel).setContiguous(false);
		measurement.getEventChannel(newChannel).setChannelName("zaznamki");
		recordForm->markersChannel = newChannel;
	} else
		recordForm->markersChannel = -1;

	//show recording form
	if (recordForm->ShowModal() != mrOk)
		return false;

	//report number of samples for each channel
	char buf[10000];
	sprintf(buf, "Meritev koncana.\n");
	for (int i = 0; i < measurement.getNumChannels(); i++)
		sprintf(buf+strlen(buf), "\tkanal %d: %d vzorcev\n", i, measurement[i].getNumElems());
	//report disabled
	//Application->MessageBox(buf, "Porocilo o meritvi", MB_OK);

	//delete all empty channels; relloc the rest
	for (int i = measurement.getNumChannels()-1; i >= 0; i--)
		if (measurement[i].getNumElems() == 0)
			measurement.deleteChannel(i);
		else measurement[i].reallocTable(measurement[i].getNumElems());
	for (int i = measurement.getNumEventChannels()-1; i >= 0; i--)
		if (measurement.getEventChannel(i).getNumElems() == 0)
			measurement.deleteEventChannel(i);
		else measurement.getEventChannel(i).reallocTable(measurement.getEventChannel(i).getNumElems());

	//trim all signal channels to equal length (trim ends because starts are synchronized)
	int minNumSamples = 1<<30;
	for (int i = 0; i < measurement.getNumChannels(); i++)
		minNumSamples = min(minNumSamples, measurement[i].getNumElems());
	for (int i = 0; i  < measurement.getNumChannels(); i++)
		if (measurement[i].getNumElems() > minNumSamples)
			measurement[i].deleteElems(minNumSamples, measurement[i].getNumElems() - 1);
	//trim preview period from beginning of signal channels (event channels already have valid times)
	int trimNumber = int((recordForm->measurementStartedTime
					- recordForm->previewStartedTime)/1000.0*measurement.getSamplingRate() + 0.5);
	for (int i = 0; i  < measurement.getNumChannels(); i++)
	{
		measurement[i].deleteElems(0, min(trimNumber - 1, measurement[i].getNumElems()-1) );
		if (measurement[i].getNumElems() == 0)
			return false;
	}

	dirty = true;

	//show measurement in Chart
	selectChannel(0, false, false);
	xLeft = 0;
	xRight = measurement.getDuration();
	refreshWholeWindow();
	undoStack.resetStack();

	unsigned short mYear, mMonth, mDay, mHour, mMin, mSec, mMsec;
	unsigned short pYear, pMonth, pDay;
	int age;
	measurement.patientData.dateOfBirth.DecodeDate(&pYear, &pMonth, &pDay);
	measurement.getDate().DecodeDate(&mYear, &mMonth, &mDay);
	measurement.getDate().DecodeTime(&mHour, &mMin, &mSec, &mMsec);
	age = (mMonth > pMonth || (mMonth == pMonth && mDay >= pDay)) ? mYear - pYear : mYear - pYear - 1;

	AnsiString ASCIIpatientName = LibCore::makeStringASCII(measurement.patientData.name);
	if (ASCIIpatientName.Pos(" "))
		ASCIIpatientName = ASCIIpatientName.SubString(1, ASCIIpatientName.Pos(" ")-1);

	if (measurementType == LibStdMeasurement::stdmNonstandard)
		sprintf(buf, "%s %d %c T%04d%02d%02d%02d%02d%02d.nekg", ASCIIpatientName, age,
				measurement.patientData.SexToChar(measurement.patientData.sex), mYear, mMonth, mDay, mHour, mMin, mSec);
	else
		sprintf(buf, "%s %s %d %c T%04d%02d%02d%02d%02d%02d.nekg", measurementProps.measurementNamePrefix, ASCIIpatientName, age,
				measurement.patientData.SexToChar(measurement.patientData.sex), mYear, mMonth, mDay, mHour, mMin, mSec);
	MainForm->SaveDialog->FileName = AnsiString(buf);
	if (MainForm->SaveDialog->Execute())
	{
		//fileName = LibCore::pathOfFilename(MainForm->SaveDialog->FileName) + "\\" + buf;
        // why is filename discarded and only path used?
        fileName = MainForm->SaveDialog->FileName;
		measurement.saveToFile(fileName, LibCore::CMeasurement::nevroEKG);
		Caption = (measurement.getMeasurementName().Length() > 0 ?
					measurement.getMeasurementName() : AnsiString("untitled"))
					+ " [" + fileName + "]";
		dirty = false;
	} else
		Application->MessageBox("Meritev NI shranjena.", "Opozorilo", MB_OK);
    */
	return true;
}

bool __fastcall TDocumentForm::CloseQuery()
{
	if (dirty)
	{
		switch (Application->MessageBox(
					"Ali �elite shraniti datoteko?", "VisECG",
					MB_ICONQUESTION | MB_YESNOCANCEL))
		{
		case IDNO:
			return true;
		case IDYES:
			try {
				SaveMeasurement(fileName);
			} catch (LibCore::EKGException *e) {
				Application->MessageBox(e->Message.c_str(), "Napaka pri shranjevanju", MB_OK);
				return false;
			}
			return true;
		default:
			return false;
		}
	} else
		return true;
}

void __fastcall TDocumentForm::FormClose(TObject *Sender,
	  TCloseAction &Action)
{
	Action = caFree;
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::FormShow(TObject *Sender)
{
	Chart->LeftAxis->Automatic = false;
    Chart->RightAxis->Automatic = false;
    Chart->TopAxis->Automatic = false;
    btnShowChannelTable->Down = true; 
    btnShowChannelTableClick(Sender);
	FormResize(NULL);
	refreshStatusLine();
}
//---------------------------------------------------------------------------

void TDocumentForm::fillGridChannelView()
{
	gridChannelView->RowCount = measurement.getNumChannels()+measurement.getNumEventChannels()+measurement.getNumAnnotationChannels()+1;
	for (int i = 0; i < measurement.getNumChannels(); i++)
	{
		gridChannelView->Cells[0][i+1] = measurement.getChannelDescription(i);
		gridChannelView->Cells[1][i+1] = measurement[i].viewProps.visible ? "D" : "N";
		gridChannelView->Cells[2][i+1] = AnsiString(measurement[i].viewProps.zoomY);
		gridChannelView->Cells[3][i+1] = AnsiString(measurement[i].viewProps.offset);
		gridChannelView->Cells[4][i+1] = (!annotationChannelSelected && !eventChannelSelected && numSelectedChannel == i) ? "D" : "N";
	}
	for (int i = 0; i < measurement.getNumEventChannels(); i++)
	{
		gridChannelView->Cells[0][i+1+measurement.getNumChannels()] = measurement.getEventChannelDescription(i);
		gridChannelView->Cells[1][i+1+measurement.getNumChannels()] = "D";
		gridChannelView->Cells[1][i+1+measurement.getNumChannels()]
			= measurement.getEventChannel(i).viewProps.visible ? "D" : "N";
		gridChannelView->Cells[2][i+1+measurement.getNumChannels()]
			= AnsiString(measurement.getEventChannel(i).viewProps.zoomY);
		gridChannelView->Cells[3][i+1+measurement.getNumChannels()]
			= AnsiString(measurement.getEventChannel(i).viewProps.offset);
		gridChannelView->Cells[4][i+1+measurement.getNumChannels()] = (eventChannelSelected && !annotationChannelSelected && numSelectedChannel == i) ? "D" : "N";
	}
    for (int i = 0; i < measurement.getNumAnnotationChannels(); i++)
	{
		gridChannelView->Cells[0][i+1+measurement.getNumChannels()+measurement.getNumEventChannels()]
            = measurement.getAnnotationChannelDescription(i);
		gridChannelView->Cells[1][i+1+measurement.getNumChannels()+measurement.getNumEventChannels()]
            = "A";
		gridChannelView->Cells[1][i+1+measurement.getNumChannels()+measurement.getNumEventChannels()]
			= measurement.getAnnotationChannel(i).viewProps.visible ? "D" : "N";
		gridChannelView->Cells[2][i+1+measurement.getNumChannels()+measurement.getNumEventChannels()]
			= AnsiString(measurement.getAnnotationChannel(i).viewProps.zoomY);
		gridChannelView->Cells[3][i+1+measurement.getNumChannels()+measurement.getNumEventChannels()]
			= AnsiString(measurement.getAnnotationChannel(i).viewProps.offset);
		gridChannelView->Cells[4][i+1+measurement.getNumChannels()+measurement.getNumEventChannels()] = (annotationChannelSelected && numSelectedChannel == i) ? "D" : "N";
	}
}

void __fastcall TDocumentForm::gridChannelViewDblClick(TObject *Sender)
{
	if (gridCellUnderMouse.Y-1 < 0 || gridCellUnderMouse.Y-1 >= measurement.getNumChannels()+measurement.getNumEventChannels()+measurement.getNumAnnotationChannels())
		return;
	bool eventChannelClicked = gridCellUnderMouse.Y-1 >= measurement.getNumChannels();
    bool annotationChannelClicked = gridCellUnderMouse.Y-1 >= measurement.getNumChannels() + measurement.getNumEventChannels();

	int channelNum = eventChannelClicked ? gridCellUnderMouse.Y-1-measurement.getNumChannels()
								: gridCellUnderMouse.Y-1;
    if(annotationChannelClicked)
        channelNum -= measurement.getNumEventChannels();


	switch (gridCellUnderMouse.X) {
	case 0: { //dbl-click on name column: display channelProps dialog for clicked channel
        if(annotationChannelClicked)
            dlgChannelProps->channel2Dialog(measurement.getAnnotationChannel(channelNum));
		else if (eventChannelClicked)
			dlgChannelProps->channel2Dialog(measurement.getEventChannel(channelNum));
		else
			dlgChannelProps->channel2Dialog(measurement[channelNum]);
		if (dlgChannelProps->ShowModal() == IDOK) {
            if(annotationChannelClicked)
                dlgChannelProps->dialog2Channel(measurement.getAnnotationChannel(channelNum));
			else if (eventChannelClicked)
				dlgChannelProps->dialog2Channel(measurement.getEventChannel(channelNum));
			else
				dlgChannelProps->dialog2Channel(measurement[channelNum]);
			dirty = true;
			fillGridChannelView();
			repaintGraph();
		}
		break;
	}
	case 1: { //dbl-click on visible column: toggle visible property
		if (gridChannelView->Cells[1][gridCellUnderMouse.Y] == "D")
		{
            if ( annotationChannelClicked && (!eventChannelSelected || numSelectedChannel != channelNum))
				measurement.getAnnotationChannel(channelNum).viewProps.visible = false;
			else if (eventChannelClicked && (!eventChannelSelected || numSelectedChannel != channelNum))
				measurement.getEventChannel(channelNum).viewProps.visible = false;
			else if (!eventChannelClicked && (eventChannelSelected || numSelectedChannel != channelNum))
				measurement[channelNum].viewProps.visible = false;
		} else {
            if (annotationChannelClicked)
                measurement.getAnnotationChannel(channelNum).viewProps.visible = true;
			else if (eventChannelClicked)
				measurement.getEventChannel(channelNum).viewProps.visible = true;
			else
				measurement[channelNum].viewProps.visible = true;
		}
		fillGridChannelView();
		repaintGraph();
		break;
	}
	case 4: { //dbl-click on selected column: select clicked channel
		selectChannel(channelNum, eventChannelClicked, annotationChannelClicked);
		gridChannelView->EditorMode = false;
		fillGridChannelView();
		refreshStatusLine();
		repaintGraph();
		break;
	}
	default:
		break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::gridChannelViewMouseMove(TObject *Sender,
	  TShiftState Shift, int X, int Y)
{
	gridCellUnderMouse = gridChannelView->MouseCoord(X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnShowAllChannelsClick(TObject *Sender)
{
	for (int i = 0; i < measurement.getNumChannels(); i++)
		measurement[i].viewProps.visible = true;
	for (int i = 0; i < measurement.getNumEventChannels(); i++)
		measurement.getEventChannel(i).viewProps.visible = true;
	repaintGraph();
	fillGridChannelView();
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnHideAllChannelsClick(TObject *Sender)
{
	for (int i = 0; i < measurement.getNumChannels(); i++)
		if (eventChannelSelected || numSelectedChannel != i)
			measurement[i].viewProps.visible = false;
	for (int i = 0; i < measurement.getNumEventChannels(); i++)
		if (!eventChannelSelected || numSelectedChannel != i)
			measurement.getEventChannel(i).viewProps.visible = false;
	repaintGraph();
	fillGridChannelView();
}
//---------------------------------------------------------------------------


void __fastcall TDocumentForm::gridChannelViewSetEditText(TObject *Sender,
	  int ACol, int ARow, const AnsiString Value)
{
	if (ARow-1 < 0 || ARow-1 >= measurement.getNumChannels()+measurement.getNumEventChannels())
		return;
	bool eventChannel = ARow-1 >= measurement.getNumChannels();
	int channelNum = eventChannel ? ARow-1-measurement.getNumChannels()	: ARow-1;
	double newValue = LibCore::myAtof(Value);
	if (newValue == 0 && Value != "0")
		return;
	if (ACol == 3)
	{
		//offset of a channel changed
		if (eventChannel)
			measurement.getEventChannel(channelNum).viewProps.offset = newValue;
		else
			measurement[channelNum].viewProps.offset = newValue;
		repaintGraph();
	}
	if (ACol == 2)
	{
		//y zoom of a channel changed
		if (newValue > 0)
		{
			if (eventChannel)
				measurement.getEventChannel(channelNum).viewProps.zoomY = newValue;
			else
				measurement[channelNum].viewProps.zoomY = newValue;
			repaintGraph();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnShowLegendClick(TObject *Sender)
{
	Chart->Legend->Visible = btnShowLegend->Down;
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnShowChannelTableClick(TObject *Sender)
{
	if (btnShowChannelTable->Down)
	{
		Chart->Width = ClientWidth - Chart->Left - 8;
		Chart->Left = gridChannelView->Width + 5;
		horzScroll->Width = Chart->Width;
		horzScroll->Left = Chart->Left;
		btnShowAllChannels->Visible = true;
		btnHideAllChannels->Visible = true;
		gridChannelView->Visible = true;
		groupCursors->Visible = true;
	} else {
		Chart->SetFocus();
		btnShowAllChannels->Visible = false;
		btnHideAllChannels->Visible = false;
		gridChannelView->Visible = false;
		groupCursors->Visible = false;
		Chart->Left = 5;
		Chart->Width = ClientWidth - 8;
		horzScroll->Left = Chart->Left;
		horzScroll->Width = Chart->Width;
	}
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::gridChannelViewMouseWheelDown(
	  TObject *Sender, TShiftState Shift, TPoint &MousePos, bool &Handled)
{
	FormMouseWheelDown(Sender, Shift, MousePos, Handled);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::gridChannelViewMouseWheelUp(TObject *Sender,
	  TShiftState Shift, TPoint &MousePos, bool &Handled)
{
	FormMouseWheelUp(Sender, Shift, MousePos, Handled);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::FormMouseWheelDown(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
	if (Shift.Contains(ssShift)) {
		if (yScaleMode == LibCore::CMeasurement::yScaleModeManual)
		{
			double yRange = Chart->LeftAxis->Maximum - Chart->LeftAxis->Minimum;
			if (Shift.Contains(ssCtrl))
			{
				Chart->LeftAxis->Minimum -= yRange/20;
				Chart->LeftAxis->Maximum += yRange/20;
			} else {
				Chart->LeftAxis->Minimum -= yRange/10;
				Chart->LeftAxis->Maximum -= yRange/10;
			}
			cursors.drawCursors(Chart, measurement, eventChannelSelected, annotationChannelSelected, numSelectedChannel);
		}
	} else if (Shift.Contains(ssCtrl))
		zoomOutX(1.25, Chart->Series[0]->XScreenToValue(Chart->GetCursorPos().x));
	else
		horzScroll->Position = horzScroll->Position + horzScroll->SmallChange;
	Handled = true;
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::FormMouseWheelUp(TObject *Sender,
	  TShiftState Shift, TPoint &MousePos, bool &Handled)
{
	if (Shift.Contains(ssShift)) {
		if (yScaleMode == LibCore::CMeasurement::yScaleModeManual)
		{
			double yRange = Chart->LeftAxis->Maximum - Chart->LeftAxis->Minimum;
			if (Shift.Contains(ssCtrl))
			{
				Chart->LeftAxis->Minimum += yRange/20;
				Chart->LeftAxis->Maximum -= yRange/20;
			} else {
				Chart->LeftAxis->Minimum += yRange/10;
				Chart->LeftAxis->Maximum += yRange/10;
			}
			cursors.drawCursors(Chart, measurement, eventChannelSelected, annotationChannelSelected, numSelectedChannel);
		}
	} else if (Shift.Contains(ssCtrl))
		zoomOutX(0.8, Chart->Series[0]->XScreenToValue(Chart->GetCursorPos().x));
	else
		horzScroll->Position = horzScroll->Position - horzScroll->SmallChange;
	Handled = true;
}
//---------------------------------------------------------------------------

void TDocumentForm::zoomOutX(double factor, double newCentre)
{
	double newRange = max(min((xRight-xLeft)*factor, measurement.getDuration()), 100/measurement.getSamplingRate());
	if (newCentre-newRange/2 >= 0 && newCentre+newRange/2 <= measurement.getDuration())
		xLeft = max(0.0, newCentre - newRange/2);
	else if (newCentre-newRange/2 >= 0)
		xLeft = measurement.getDuration() - newRange;
	else
		xLeft = 0;
	xRight = min(measurement.getDuration(), xLeft+newRange);
	repaintGraph();
}

void __fastcall TDocumentForm::ChartMouseWheel(TObject *Sender,
      TShiftState Shift, int WheelDelta, TPoint &MousePos, bool &Handled)
{
	if (WheelDelta > 0)
		for (; WheelDelta > 0; WheelDelta -= 120)
			FormMouseWheelUp(Sender, Shift, MousePos, Handled);
	else
		for (; WheelDelta < 0; WheelDelta += 120)
			FormMouseWheelDown(Sender, Shift, MousePos, Handled);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::ChartMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
	if (Chart->SeriesCount() == 0)
		return;
	mousePosOnChartX = Chart->Series[0]->XScreenToValue(X);
	mousePosOnChartY = Chart->Series[0]->YScreenToValue(Y);
	refreshStatusLine();
    if(mousePosOnChartX >= xLeft && mousePosOnChartX <= xRight)
        cursors.drawPopup(Chart, measurement, X, Y);
    else
        cursors.deletePopup();
}
//---------------------------------------------------------------------------


void __fastcall TDocumentForm::ChartMouseUp(TObject *Sender,
	  TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if (Chart->SeriesCount() == 0)
		return;
	double clickedX = Chart->Series[0]->XScreenToValue(X);
	if (clickedX < xLeft || clickedX > xRight)
		return;

	//add new event if in event adding mode
	if (nextClickDefinesEventTime && Button == mbLeft)
	{
		LibCore::Event newEvent = LibCore::Event(clickedX, valueOfEventToAdd);
		valueOfEventToAdd = 0;
		nextClickDefinesEventTime = false;

		if (!eventChannelSelected)
		{
			Application->MessageBox("Izbran mora biti dogodkovni ali opisni kanal.", "Napaka!", MB_OK);
			return;
		}
        if(!annotationChannelSelected)
        {
		    //find first existing event AFTER the clicked time
    		LibCore::CEventChannel &eventChannel = measurement.getEventChannel(numSelectedChannel);
    		int nextEventIndex = eventChannel.getIndexClosestToTime(clickedX);
    		if (fabs(eventChannel[nextEventIndex].time - clickedX) < 0.001)
    		{
    			Application->MessageBox("Dogodka morata biti vsaj 1 ms narazen.", "Napaka!", MB_OK);
    			return;
    		}
    		//find out type of channel
    		LibCore::CEventChannel::EventType eventType = eventChannel.getEventType(nextEventIndex);
	    	//add new event
    		if (eventChannel[nextEventIndex].time < clickedX)
        			nextEventIndex++;
    		eventChannel.insertElem(nextEventIndex, newEvent);
    		//correct values of newly inserted and next event
    		if (nextEventIndex+1 < eventChannel.getNumElems())
    			eventChannel.correctEventValue(nextEventIndex+1, eventType);
    		eventChannel.correctEventValue(nextEventIndex, eventType);
            eventChannel.appendComment("Dodan dogodek ob " + AnsiString(clickedX));
        		//memorize the action in the undo stack
	    	undoStack.push(CUndoStack::UndoableAction(CUndoStack::UndoableAction::actionTypeAddEvent,
    	    	newEvent, numSelectedChannel));
        }else
        {
             //find first existing event AFTER the clicked time
    		LibCore::CAnnotationChannel &annotationChannel = measurement.getAnnotationChannel(numSelectedChannel);

            AnsiString annotationString;
		    InstantDialog::queryOKCancel("Vpi�ite opis dogodka.", "Vpi�ite opis dogodka:", "Dogodek", annotationString);
            AnsiString lengthString;
		    InstantDialog::queryOKCancel("Vpi�ite trajanje dogodka.", "Vpi�ite trajanje dogodka:", "1", lengthString);
            AnsiString valueString;
		    InstantDialog::queryOKCancel("Vpi�ite vrednost dogodka.", "Vpi�ite vrednost dogodka:", "2", valueString);

            LibCore::Annotation newAnnotation = LibCore::Annotation(clickedX, annotationString, lengthString.ToDouble(), valueString.ToDouble());

            annotationChannel.appendComment("Dodan dogodek ob " + AnsiString(clickedX));
    		annotationChannel.insertElem(annotationChannel.getNumElems(), newAnnotation);
        }
		dirty = true;
		refreshWholeWindow();
		return;
	}

	//set cursor otherwise
	int cursorNum = Button == mbLeft ? comboCursorColor->ItemIndex : Button == mbMiddle ? 1 : 2;

	if (fabs(clickedX - cursors.c[cursorNum]) < (xRight-xLeft)/200)
		cursors.c[cursorNum] = -1;
	else
		cursors.c[cursorNum] = clickedX;
    cursors.drawCursors(Chart, measurement, eventChannelSelected, annotationChannelSelected, numSelectedChannel);
	refreshStatusLine();
}
//---------------------------------------------------------------------------


void TDocumentForm::filterSignal()
{
    if(annotationChannelSelected || eventChannelSelected)
        return;
	if (filterParamsDlg->ShowModal() == IDOK)
	{
		measurement.lowPassFilterBeatDetection(
		filterParamsDlg->radioRectangularWindow->Checked
				? LibCore::CMeasurement::windowRectangular : LibCore::CMeasurement::windowTriangular,
			numSelectedChannel,
        LibCore::myAtof(filterParamsDlg->editFrequency->Text),
        measurement.getMissingSamplesValue(eventChannelSelected,numSelectedChannel)
        );

    	selectChannel(numSelectedChannel, false, false);
		dirty = true;
		refreshWholeWindow();
	}
}

void TDocumentForm::setBaseline()
{
	char buff[1000];
	int signalChannel, existingBeatChannel;
	if (measurement.getNumEventChannels() == 0)
	{
		Application->MessageBox("Noben dogodkovni kanal ne obstaja.", "Napaka!", MB_OK);
		return;
	}
        if (annotationChannelSelected)
    	{
			Application->MessageBox("Izberite signalni ali dogodkovni kanal!", "Opozorilo!" , MB_OK);
            return;
        }
        else if (eventChannelSelected)
    	{
		    existingBeatChannel = numSelectedChannel;
	    	sprintf(buff, "Vnesi �tevilko signalnega kanala (%d-%d):\r\n", 1, measurement.getNumChannels());
    		AnsiString signalChannelStr;
		    if (InstantDialog::queryOKCancel("Poravnava bazne linije.", buff, "1", signalChannelStr))
	    		signalChannel = signalChannelStr.ToInt()-1;
    		else
		    	return;
   	    	if (signalChannel < 0 || signalChannel > measurement.getNumChannels()-1)
    		{
			    Application->MessageBox("Ta kanal ne obstaja.", "Izravnava bazne linije - Opozorilo!" , MB_OK);
		    	return;
	    	}

    	} else {
		    signalChannel = numSelectedChannel;
	    	sprintf(buff, "Vnesi �tevilko dogodkovnega kanala (%d-%d):\r\n", 1, measurement.getNumEventChannels());
    		AnsiString existingBeatChannelStr;
		    if (InstantDialog::queryOKCancel("Poravnava bazne linije.", buff, "1", existingBeatChannelStr))
	    		existingBeatChannel = existingBeatChannelStr.ToInt()-1;
    		else
		    	return;
	    	if (existingBeatChannel < 0 || existingBeatChannel > measurement.getNumEventChannels()-1)
    		{
			    Application->MessageBox("Ta kanal ne obstaja.", "Izravnava bazne linije - Opozorilo!" , MB_OK);
		    	return;
	    	}
       	}
    
	measurement.correctBaseline(signalChannel, existingBeatChannel, &cursors);
	selectChannel(measurement.getNumChannels()-1, false, false);
	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::invertChannel()
{
	double avg;
    if(annotationChannelSelected)
        avg =   measurement.getAnnotationChannel(numSelectedChannel).getAvgValue();
    else if (eventChannelSelected)
        measurement.getEventChannel(numSelectedChannel).getAvgValue();
    else
        measurement[numSelectedChannel].getAvgValue();

	double invertionCentre = LibCore::myAtof(InstantDialog::query("Zrcaljenje kanala.",
			"Signal zrcali okrog:", AnsiString(avg)));

    if (annotationChannelSelected){
    	measurement.getAnnotationChannel(numSelectedChannel).invertChannel(invertionCentre);
	}
    else if (eventChannelSelected){
		measurement.getEventChannel(numSelectedChannel).invertChannel(invertionCentre);
	}else{
		measurement[numSelectedChannel].invertChannel(invertionCentre);
        measurement[numSelectedChannel].tenBitSaving = false;
    }

	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::channelGain() {
    double avg;
    if(annotationChannelSelected)
        avg =   measurement.getAnnotationChannel(numSelectedChannel).getAvgValue();
    else if (eventChannelSelected)
        measurement.getEventChannel(numSelectedChannel).getAvgValue();
    else
        measurement[numSelectedChannel].getAvgValue();

	double gain = LibCore::myAtof(InstantDialog::query("Mno�enje kanala s konstanto.",
			"Pomno�i z:", AnsiString(avg)));

    if (annotationChannelSelected)
        measurement.getAnnotationChannel(numSelectedChannel).channelGain(gain);
	else if (eventChannelSelected)
		measurement.getEventChannel(numSelectedChannel).channelGain(gain);
	else
		measurement[numSelectedChannel].channelGain(gain);
	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::channelOffset() {
    double avg;
    if(annotationChannelSelected)
        avg =   measurement.getAnnotationChannel(numSelectedChannel).getAvgValue();
    else if (eventChannelSelected)
        measurement.getEventChannel(numSelectedChannel).getAvgValue();
    else
        measurement[numSelectedChannel].getAvgValue();
        
	double offset = LibCore::myAtof(InstantDialog::query("Zamikanje kanala s konstanto.",
			"Zamakni za:", AnsiString(-avg)));
    if (annotationChannelSelected)
		measurement.getAnnotationChannel(numSelectedChannel).channelSetOffset(offset);
	else if (eventChannelSelected)
		measurement.getEventChannel(numSelectedChannel).channelSetOffset(offset);
   	else
        measurement[numSelectedChannel].channelSetOffset(offset);
	dirty = true;
	refreshWholeWindow();
}



void TDocumentForm::assignInterEventTimeAsEventValues()
{
    if(eventChannelSelected && !annotationChannelSelected){
    	measurement.assignInterEventTimeAsEventValues(numSelectedChannel);
    	selectChannel(measurement.getNumEventChannels()-1, true,false);
    	dirty = true;
    	refreshWholeWindow();
    }
}

void TDocumentForm::assignInterEventTimeAsEventValuesSameChannel()
{
    if(eventChannelSelected && !annotationChannelSelected){
    	measurement.assignInterEventTimeAsEventValuesSameChannel(numSelectedChannel);
    	selectChannel(measurement.getNumEventChannels()-1, true, false);
    	dirty = true;
    	refreshWholeWindow();
    }
}

void TDocumentForm::assignEventFreqAsEventValues()
{
    if(eventChannelSelected && !annotationChannelSelected){
    	measurement.assignEventFreqAsEventValues(numSelectedChannel);
    	selectChannel(measurement.getNumEventChannels()-1, true, false);
    	dirty = true;
    	refreshWholeWindow();
    }
}

void TDocumentForm::assignEventFreqAsEventValuesFromRRI()
{
    if(eventChannelSelected && !annotationChannelSelected){
    	measurement.assignEventFreqAsEventValuesFromRRI(numSelectedChannel);
    	selectChannel(measurement.getNumEventChannels()-1, true, false);
    }
}


void TDocumentForm::refineBRwithInterpolation()
{
	char buff[1000];
	int signalChannel, existingBeatChannel;
	if (measurement.getNumEventChannels() == 0)
	{
		Application->MessageBox("Noben dogodkovni kanal ne obstaja.", "Napaka!", MB_OK);
		return;
	}
	if (eventChannelSelected)
	{
		existingBeatChannel = numSelectedChannel;
		sprintf(buff, "Vnesi �tevilko signalnega kanala, s pomo�jo katerega �eli� interpolirati(%d-%d):\r\n(OPOZORILO: Dogodkovne kanale dobljene z AM,AF,FZ ali FX interpoliramo z izmerjenim kanalom.\r\nDogodkovne kanale dobljene s FCOREL ali FRMS interpoliramo s korelacijo oz. RMSe med izmerjenim kanalom in vzorcem.)", 1, measurement.getNumChannels());
		AnsiString signalChannelStr;
		if (InstantDialog::queryOKCancel("Interpolacija dogodkovnega kanala.", buff, "1", signalChannelStr))
			signalChannel = signalChannelStr.ToInt()-1;
		else
			return;
   		if (signalChannel < 0 || signalChannel > measurement.getNumChannels()-1)
		{
			Application->MessageBox("Ta kanal ne obstaja.", "Interpolacija - Opozorilo!" , MB_OK);
			return;
		}

	} else {
		signalChannel = numSelectedChannel;
		sprintf(buff, "Vnesi �tevilko dogodkovnega kanala, ki ga �eli� interpolirati(%d-%d):\r\n(OPOZORILO: Dogodkovne kanale dobljene z AM,AF,FZ ali FX interpoliramo z izmerjenim kanalom.\r\nDogodkovne kanale dobljene s FCOREL ali FRMS interpoliramo s korelacijo oz. RMSe med izmerjenim kanalom in vzorcem.)", 1, measurement.getNumEventChannels());
		AnsiString existingBeatChannelStr;
		if (InstantDialog::queryOKCancel("Interpolacija dogodkovnega kanala.", buff, "1", existingBeatChannelStr))
			existingBeatChannel= existingBeatChannelStr.ToInt()-1;
		else
			return;
		if (existingBeatChannel < 0 || existingBeatChannel > measurement.getNumEventChannels()-1)
		{
			Application->MessageBox("Ta kanal ne obstaja.", "Interpolacija - Opozorilo!" , MB_OK);
			return;
		}
	}
	measurement.refineBeatRateWithInterpolation(signalChannel, existingBeatChannel);
	selectChannel(measurement.getNumEventChannels()-1, true, false);
	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::calcBeatTimes(LibCore::CMeasurement::BeatAlgorithm algorithm, double AMparam, bool automatic, double AMmaxValue)
{
	LibCore::CMeasurement::BeatTimesParams		params(algorithm, numSelectedChannel, &cursors);

	char buff[1000];
	switch (algorithm) {
	case LibCore::CMeasurement::beatAlgorithmFCOREL:
		if (cursors.c[0] < 0 || cursors.c[1] < 0) {
			Application->MessageBox("Za FCOREL algoritem morata biti postavljena modri in zeleni kurzor.", "Postopek FCOREL - Napaka!", MB_OK); //izpis sporocila na ekran
			return;
		}
		sprintf(buff, "Vnesi �tevilko dogodkovnega kanala, ki ga bo� uporabil za dolo�itev utripov po FCOREL (%d-%d)\r\nVnesi 0, �e dolo�a� brez predhodno dolo�enih utripov:", 0, measurement.getNumEventChannels()-1);
		params.existBeatNum = InstantDialog::query("Postopek FCOREL.", buff, "0").ToInt()-1;
		if (params.existBeatNum > measurement.getNumEventChannels()-1)   {
			Application->MessageBox("Ta kanal ne obstaja.", "Postopek FCOREL - Opozorilo!" , MB_OK);
			return;
		}
        {
            AnsiString def(0.9);
    		params.minCorel = LibCore::myAtof(InstantDialog::query("Postopek FCOREL.", "Vnesi zahtevano minimalno korelacijo (najve� 1!)", def));
        }

	default:
		break;
	}

    if (eventChannelSelected && (algorithm == LibCore::CMeasurement::beatAlgorithmAM))
        measurement.beatTimesAmForEventChan(params.sourceChannelNum, AMparam, automatic, AMmaxValue);
    else
    	measurement.calculateBeatTimes(params, AMparam, automatic, AMmaxValue);

	dirty = true;
	refreshWholeWindow();
}
void TDocumentForm::calcBeatTimesBeatDetection(LibCore::CMeasurement::BeatAlgorithm algorithm, double AMparam)
{
	LibCore::CMeasurement::BeatTimesParams		params(algorithm, numSelectedChannel, &cursors);
    if (eventChannelSelected && (algorithm == LibCore::CMeasurement::beatAlgorithmAM))
        measurement.beatTimesAmForEventChanBeatDetection(params.sourceChannelNum, AMparam);
    else
    	measurement.calculateBeatTimesBeatDetection(params, AMparam);
}


void TDocumentForm::calcStatistics()
{
    if(annotationChannelSelected){
        LibCore::CAnnotationChannel &annoChann = measurement.getAnnotationChannel(numSelectedChannel);
        LibCore::CAnnotationChannel::Statistics stat = annoChann.calcStatistics(0, measurement.getDuration());

		char buff[1000];
		AnsiString unit = annoChann.getMeasurementUnit();
		sprintf(buff, "\r\n\r\nOsnovna statistika dogodkovnega kanala.\r\n�tevilo najdenih dogodkov: %i\r\nNajve�ja vrednost: %.4f \r\nNajmanj�a vrednost: %.4f \r\nPovpre�na vrednost: %.4f \r\nPovpre�no trajanje: %.4f \r\n",stat.numOfEvents, stat.maxValue, stat.minValue, stat.avgValue, stat.avgDuration);
		Application->MessageBox(buff, "Osnovna statistika opisnega kanala." , MB_OK);
        annoChann.appendComment(buff);
    }else if (eventChannelSelected)
	{
		LibCore::CEventChannel &eventChann = measurement.getEventChannel(numSelectedChannel);
		LibCore::CEventChannel::Statistics stat = eventChann.calcStatistics();

		char buff[1000];
		AnsiString unit = eventChann.getMeasurementUnit();
		sprintf(buff, "\r\n\r\nOsnovna statistika dogodkovnega kanala.\r\n�tevilo najdenih dogodkov: %i\r\nSrednja vrednost: %.4f [%s], Sr.vr. �asov %.2f [ms]\r\nNajve�ja vrednost/�as: %.4f [%s] /  %.2f [ms]\r\nNajmanj�a vrednost/�as: %.4f [%s], %.2f [ms]\r\nStandardni odklon od sr.vred./sr.�asa: %.4f [%s] / %.2f [ms]\r\nRMS razlik med vrednostmi sosednjih dogodkov: %.4f [%s]",
			stat.numOfEvents, stat.avgValue, unit, stat.avgTime*1000, stat.maxValue, unit, stat.maxTime*1000, stat.minValue, unit, stat.minTime*1000, stat.stDevValue, unit, stat.stDevTime*1000, stat.RMSSD, unit);
		if (autoCopyToClipboard) {
			char shortBuff[1000];
			double	mult = 1.0;
			if (unit == "s")
				mult = 1000.0;

			sprintf(shortBuff, "%.4f \t %.4f \t %.4f \t %.4f \t %.4f \r\n",
				stat.avgValue*mult, stat.maxValue*mult, stat.minValue*mult, stat.stDevValue*mult, stat.RMSSD*mult);
			Clipboard()->SetTextBuf(shortBuff);
		}
		Application->MessageBox(buff, "Osnovna statistika dogodkovnega kanala." , MB_OK);
		eventChann.appendComment(buff);
	}
	else

		measurement.calcStatisticsSignal(numSelectedChannel);
}


void TDocumentForm::calcStatisticsBetweenCursors()
{

    double start,  stop;

	if (cursors.getNumSetCursors() >= 2)
	{
		start = cursors.getLeftmostCursor();
		stop = cursors.getRightmostCursor();
	}
	else
    {
        Application->MessageBox("Potrebna sta vsaj dva kurzorja.", "Napaka!", MB_OK);
        return;
    }

    if(stop<=start)
    {
        Application->MessageBox("Napa�na postavitev kurzorjev.", "Napaka!", MB_OK);
        return;
    }

    if(annotationChannelSelected){
        LibCore::CAnnotationChannel &annoChann = measurement.getAnnotationChannel(numSelectedChannel);
        LibCore::CAnnotationChannel::Statistics stat = annoChann.calcStatistics(start, stop);

		char buff[1000];
		AnsiString unit = annoChann.getMeasurementUnit();
		sprintf(buff, "\r\n\r\nOsnovna statistika dogodkovnega kanala.\r\n�tevilo najdenih dogodkov: %i\r\nNajve�ja vrednost: %.4f \r\nNajmanj�a vrednost: %.4f \r\nPovpre�na vrednost: %.4f \r\nPovpre�no trajanje: %.4f \r\n",stat.numOfEvents, stat.maxValue, stat.minValue, stat.avgValue, stat.avgDuration);
		Application->MessageBox(buff, "Osnovna statistika opisnega kanala." , MB_OK);
        annoChann.appendComment(buff);
    }else if (eventChannelSelected)
	{
		LibCore::CEventChannel &eventChann = measurement.getEventChannel(numSelectedChannel);
		LibCore::CEventChannel::Statistics stat = eventChann.calcStatistics(start, stop);

		char buff[1000];
		AnsiString unit = eventChann.getMeasurementUnit();
		sprintf(buff, "\r\n\r\nOsnovna statistika dogodkovnega kanala.\r\n�tevilo najdenih dogodkov: %i\r\nSrednja vrednost: %.4f [%s], Sr.vr. �asov %.2f [ms]\r\nNajve�ja vrednost/�as: %.4f [%s] /  %.2f [ms]\r\nNajmanj�a vrednost/�as: %.4f [%s], %.2f [ms]\r\nStandardni odklon od sr.vred./sr.�asa: %.4f [%s] / %.2f [ms]\r\nRMS razlik med vrednostmi sosednjih dogodkov: %.4f [%s]",
			stat.numOfEvents, stat.avgValue, unit, stat.avgTime*1000, stat.maxValue, unit, stat.maxTime*1000, stat.minValue, unit, stat.minTime*1000, stat.stDevValue, unit, stat.stDevTime*1000, stat.RMSSD, unit);
		if (autoCopyToClipboard) {
			char shortBuff[1000];
			double	mult = 1.0;
			if (unit == "s")
				mult = 1000.0;

			sprintf(shortBuff, "%.4f \t %.4f \t %.4f \t %.4f \t %.4f \r\n",
				stat.avgValue*mult, stat.maxValue*mult, stat.minValue*mult, stat.stDevValue*mult, stat.RMSSD*mult);
			Clipboard()->SetTextBuf(shortBuff);
		}
		Application->MessageBox(buff, "Osnovna statistika dogodkovnega kanala." , MB_OK);
		eventChann.appendComment(buff);
	}
	else
    {
		measurement.calcStatisticsSignalBetweenCursors(numSelectedChannel, start, stop);
    }
}

void TDocumentForm::maxANDmin()
{
	if (eventChannelSelected)
		measurement.MaxMin(numSelectedChannel, &cursors);
	else
        Application->MessageBox("Izberi dogodkovni kanal.", "Max/Min - Opozorilo!" , MB_OK);
}

void __fastcall TDocumentForm::gridChannelViewDrawCell(TObject *Sender,
      int ACol, int ARow, TRect &Rect, TGridDrawState State)
{
	if (ACol == 4 && gridChannelView->Cells[ACol][ARow] == "D")
	{
		TColor oldColor = gridChannelView->Canvas->Brush->Color;
		gridChannelView->Canvas->Brush->Color = clRed;
		gridChannelView->Canvas->Rectangle(Rect);
		gridChannelView->Canvas->Brush->Color = oldColor;
	}
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnZoomCursorClick(TObject *Sender)
{
	if (cursors.getNumSetCursors() >= 2)
	{
		xLeft = cursors.getLeftmostCursor();
		xRight = cursors.getRightmostCursor();
		repaintGraph();
	} else if (cursors.getNumSetCursors() >= 1)
		zoomOutX(0.5, cursors.getLeftmostCursor());
	else
		Application->MessageBox("Napaka!", "Potreben je vsaj en kurzor.", MB_OK);
}
//---------------------------------------------------------------------------

void TDocumentForm::fourierTransform()
{
    if(annotationChannelSelected)
        return;
    double freq;
    AnsiString resultStr;
	if (InstantDialog::queryOKCancel("Fourierjeva transformacija.", "Izberite najvijo frekvenco za prikaz", "0.5", resultStr))
        freq = resultStr.ToDouble();
    else
		return;
	LibCore::CMeasurement::DFTParams params;
	params = measurement.fourierTransform(numSelectedChannel, freq);

	measurement.getEventChannel(measurement.getNumEventChannels()-1).refreshMinMaxValue();
	FFTform->Caption = "DFT kanala D" + AnsiString(numSelectedChannel+1);
	FFTform->coherence = false;
	FFTform->pMeasurement = &measurement;
	FFTform->pDFTParams = &params;
	FFTform->autoCopyToClipboard = autoCopyToClipboard;
	FFTform->ShowModal();
	measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
}

void TDocumentForm::cutFromTo(double from, double to)
{
	measurement.cut(from, to);
	if (xLeft > from)
		xLeft = (xLeft < to) ? from : xLeft - (to-from);
	if (xRight > from)
		xRight = (xRight < to) ? from : xRight - (to-from);
	for (int i = 0; i < cursors.numCursors; i++)
		if (cursors.c[i] > from)
			cursors.c[i] = (cursors.c[i] < to) ? from : cursors.c[i] - (to-from);
	dirty = true;
	undoStack.resetStack();
	refreshWholeWindow();
}

void __fastcall TDocumentForm::btnTrueYscaleClick(TObject *Sender)
{
	dialogViewY->radioUntrueYscale->Checked = !trueYscale;
	dialogViewY->radioTrueYscale->Checked = trueYscale;
	switch (yScaleMode) {
	case LibCore::CMeasurement::yScaleModeAll:
		dialogViewY->radioViewAll->Checked = true;
		break;
	case LibCore::CMeasurement::yScaleModeAuto:
		dialogViewY->radioViewCurrent->Checked = true;
		break;
	case LibCore::CMeasurement::yScaleModeManual:
		dialogViewY->radioViewManual->Checked = true;
		break;
	}
	dialogViewY->Show();
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::btnRemoveAllCursorsClick(TObject *Sender)
{
	for (int i = 0; i < LibCore::CursorPositions::numCursors; i++)
		cursors.c[i] = -1;
    cursors.drawCursors(Chart, measurement, eventChannelSelected, annotationChannelSelected, numSelectedChannel);
	refreshStatusLine();
}
//---------------------------------------------------------------------------

void TDocumentForm::removeEventAtCursor(int cursorIndex)
{
	assert(eventChannelSelected);
	assert(cursorIndex >= 0 && cursors.c[cursorIndex] != -1);
    if(annotationChannelSelected){
    	LibCore::CAnnotationChannel &annChannel = measurement.getAnnotationChannel(numSelectedChannel);
        double time = cursors.c[cursorIndex];
        annChannel.deleteElems(time,time);
    }
    if(!annotationChannelSelected){
    	LibCore::CEventChannel &eventChannel = measurement.getEventChannel(numSelectedChannel);

    	if (eventChannel.getNumElems() == 0)
    	{
    		Application->MessageBox("Edinega dogodka ni mo�no zbrisati. Zbri�ite celoten kanal.", "Napaka!", MB_OK);
    		return;
    	}
       	//memorize action for undo
    	int eventIndex = eventChannel.getIndexClosestToTime(cursors.c[cursorIndex]);
    	undoStack.push(CUndoStack::UndoableAction(CUndoStack::UndoableAction::actionTypeDeleteEvent,
	    					eventChannel[eventIndex], numSelectedChannel));
    	//find out channel type
    	LibCore::CEventChannel::EventType eventType = eventChannel.getEventType(eventIndex);   //find out what type if event channel it is
    	//delete event
    	eventChannel.deleteElems(eventIndex, eventIndex);

        eventChannel.appendComment("Zbrisan dogodek ob " + AnsiString(cursors.c[cursorIndex]));

    	if (eventChannel.getNumElems() > 0)
    	{
    		//correct next event's value
    		if (eventIndex < eventChannel.getNumElems()) //correct value of event after the deleted one
    			eventChannel.correctEventValue(eventIndex, eventType);
    		eventChannel.refreshMinMaxValue();
    	} else {
    		//delete channel if it is now empty
    		measurement.deleteEventChannel(numSelectedChannel);
    		selectChannel(0, false, false);
    	}
    }else{
       	LibCore::CAnnotationChannel &eventChannel = measurement.getAnnotationChannel(numSelectedChannel);
      	if (eventChannel.getNumElems() == 0)
       	{
       		Application->MessageBox("Edinega dogodka ni mo�no zbrisati. Zbri�ite celoten kanal.", "Napaka!", MB_OK);
       		return;
       	}
       	int eventIndex = eventChannel.getIndexClosestToTime(cursors.c[cursorIndex]);
       	eventChannel.deleteElems(eventIndex, eventIndex);
        eventChannel.appendComment("Zbrisan dogodek ob " + AnsiString(cursors.c[cursorIndex]));
        if(eventChannel.getNumElems() == 0){
    		measurement.deleteAnnotationChannel(numSelectedChannel);
    		selectChannel(0, false, false);
        }
    }
	dirty = true;
    refreshWholeWindow();
//	repaintGraph();
}

// removes events that lay between (and including) the leftmost and the rightmost cursor
void TDocumentForm::removeEventsAtCursors() {
	assert(eventChannelSelected); 

	double					firstEventInRange = cursors.c[0];
	double					lastEventInRange = cursors.c[0];
	
	if (cursors.c[1] > -1) {
		if ((cursors.c[1] < firstEventInRange) || (firstEventInRange == -1))
			firstEventInRange = cursors.c[1];
		if (cursors.c[1] > lastEventInRange)
			lastEventInRange = cursors.c[1];
	}
	if (cursors.c[2] > -1) {
		if ((cursors.c[2] < firstEventInRange) || (firstEventInRange == -1))
			firstEventInRange = cursors.c[2];
		if (cursors.c[2] > lastEventInRange)
			lastEventInRange = cursors.c[2];
	}

	// no cursor was set
	if (firstEventInRange == -1)
		return;

	// cursors.c[cursorIndex] != -1);
	if(annotationChannelSelected){
        LibCore::CAnnotationChannel &eventChannel = measurement.getAnnotationChannel(numSelectedChannel);
    	if (eventChannel.getNumElems() == 0)
    	{
    		Application->MessageBox("Edinega dogodka ni mo�no zbrisati. Zbri�ite celoten kanal.", "Napaka!", MB_OK);
    		return;
    	}

    	double eventIndex1 = firstEventInRange;
       	double eventIndex2 = lastEventInRange;

    	eventChannel.deleteElems(eventIndex1, eventIndex2);

    	if (eventChannel.getNumElems() > 0)
    	{
    		// no value correction here
    	} else {
    	    	//delete channel if it is now empty
    		measurement.deleteAnnotationChannel(numSelectedChannel);
    		selectChannel(0, false, false);
    	}
    }
	else{
        LibCore::CEventChannel &eventChannel = measurement.getEventChannel(numSelectedChannel);
    	if (eventChannel.getNumElems() == 0)
    	{
    		Application->MessageBox("Edinega dogodka ni mo�no zbrisati. Zbri�ite celoten kanal.", "Napaka!", MB_OK);
    		return;
    	}

    	int 	  				eventIndex1 = eventChannel.getIndexClosestToTime(firstEventInRange);
       	int 	  				eventIndex2 = eventChannel.getIndexClosestToTime(lastEventInRange);

    	// memorize undo actions
    	for (int eIndex = eventIndex1; eIndex <= eventIndex2; ++eIndex) {
    		undoStack.push(CUndoStack::UndoableAction(CUndoStack::UndoableAction::actionTypeDeleteEvent,
    			eventChannel[eIndex], numSelectedChannel));
    	}
    	eventChannel.deleteElems(eventIndex1, eventIndex2);
    	eventChannel.refreshMinMaxValue();

    	if (eventChannel.getNumElems() > 0)
    	{
    		// no value correction here
    	} else {
    	    	//delete channel if it is now empty
    		measurement.deleteEventChannel(numSelectedChannel);
    		selectChannel(0, false, false);
    	}
    }
	dirty = true;
	repaintGraph();
}

void TDocumentForm::addEvent()
{
	nextClickDefinesEventTime = true;
	refreshStatusLine();
}

void TDocumentForm::addEventByValue() {
    assert(eventChannelSelected);
    if(annotationChannelSelected){
        Application->MessageBox("Za dodajanje opisnih dogodkov uporabite mo�nost \"Vstavi dogodek\".", "Napaka!", MB_OK);
        return;
    }

    AnsiString queryReturn("");
	while (InstantDialog::queryOKCancel("Dodaj dogodek.", AnsiString(
		"Vpi�i dogodek kot \"�as\" \"presledek\" \"vrednost\""), queryReturn, queryReturn)) {
        double time, value;
        bool hasSucceded = true;

        try {    
            int delIndex = 0;
            for (; (delIndex < queryReturn.Length()) && (!queryReturn.IsDelimiter(" ", delIndex)); ++delIndex) ;
            time = queryReturn.SubString(1, delIndex-1).ToDouble();
            value = queryReturn.SubString(delIndex+1, queryReturn.Length()).ToDouble();
        } catch(...) {
            hasSucceded = false;
        }
        
        if (hasSucceded) {
            LibCore::CEventChannel& ec = measurement.getEventChannel(numSelectedChannel);
            int nearIndex = ec.getIndexClosestToTime(time);
            int indexOneAfter = (ec[nearIndex].time > time ? nearIndex : nearIndex+1);
            ec.insertElem(indexOneAfter, LibCore::Event(time, value));
    
            ec.refreshMinMaxValue();
        	dirty = true;
        	repaintGraph();

            break;
        } else {
            Application->MessageBox("Napaka v podanih podatkih, poskusi znova.", "Napaka!", MB_OK);
        }
    }
}

void TDocumentForm::moveEventAtFirstCursor()
{
	assert(eventChannelSelected);
    if(annotationChannelSelected){
         Application->MessageBox(
         "Premikanje opisnih dogodkov ni mo�no!",
         "Opozorilo", MB_OK);
        return;
    }
	assert(cursors.c[0] != -1);
	valueOfEventToAdd = measurement.accessEventByTime(numSelectedChannel, cursors.c[0]).value;
	removeEventAtCursor(0);
    measurement.getEventChannel(numSelectedChannel).appendComment("Odstranjen dogodek ob "+ AnsiString(cursors.c[0]));
	addEvent();
}

void TDocumentForm::replaceWithEquidistantEvents(int numReplacementEvents)
{
	assert(eventChannelSelected);
    if(annotationChannelSelected){
         Application->MessageBox(
         "Prosimo izberite dogodkovni, in ne opisni kanal!",
         "Opozorilo", MB_OK);
        return;
    }
	assert(cursors.c[0] != -1 && cursors.c[1] != -1);

	LibCore::CEventChannel &eventChannel = measurement.getEventChannel(numSelectedChannel);
	double start = cursors.c[0], end = cursors.c[1];
	int startIndex = eventChannel.getIndexClosestToTime(start);
	int endIndex = eventChannel.getIndexClosestToTime(end);
	LibCore::CEventChannel::EventType eventType = eventChannel.getEventType(endIndex);

	//remove any existing events in the interval, memorizing actions for undo)
	if (endIndex > startIndex+1)
	{
		for (int i = startIndex+1; i < endIndex; i++)
			undoStack.push(CUndoStack::UndoableAction(CUndoStack::UndoableAction::actionTypeDeleteEvent,
							eventChannel[i], numSelectedChannel));
		eventChannel.deleteElems(startIndex+1, endIndex-1);
	}

	//add new events & correct their values, memorizing actions for undo)
	for (int i = 0; i < numReplacementEvents; i++)
	{
		eventChannel.insertElem(startIndex+i+1,
				LibCore::Event( start + (i+1)*(end-start)/(numReplacementEvents+1), 0) );
		eventChannel.correctEventValue(startIndex+i+1, eventType);
		undoStack.push(CUndoStack::UndoableAction(CUndoStack::UndoableAction::actionTypeAddEvent,
							eventChannel[startIndex+i+1], numSelectedChannel));
	}
	//correct the value at first (if necessary) & last cursor
	if (startIndex == 0)
		eventChannel.correctEventValue(startIndex, eventType);
	eventChannel.correctEventValue(startIndex+numReplacementEvents+1, eventType);

	eventChannel.refreshMinMaxValue();
	dirty = true;
	repaintGraph();
}

void TDocumentForm::removeExtremeEvents()
{
	assert(eventChannelSelected);
    if(annotationChannelSelected)
        return;
	AnsiString currentIntervalStr;
	LibCore::CEventChannel &eventChannel = measurement.getEventChannel(numSelectedChannel);
	currentIntervalStr.sprintf("Trenutno obmo�je: %lf - %lf. ", 
        eventChannel.getMinValue(), eventChannel.getMaxValue());

    AnsiString userInput;
    if (InstantDialog::queryOKCancel(
        "Odstranjevanje ekstremnih dogodkov.", 
        currentIntervalStr + "Novi minimum:", 
        AnsiString(floor(eventChannel.getMinValue())),
        userInput)) 
    {
    	double minAllowedValue = LibCore::myAtof(userInput);
        if (InstantDialog::queryOKCancel(
            "Odstranjevanje ekstremnih dogodkov.", 
            currentIntervalStr + "Novi maximum:", 
            AnsiString(ceil(eventChannel.getMaxValue())),
            userInput)) 
        {
        	double maxAllowedValue = LibCore::myAtof(userInput);
        	eventChannel.removeExtremeEvents(minAllowedValue, maxAllowedValue);
    	    if (eventChannel.getNumElems() == 0)
        	{
	    	    measurement.deleteEventChannel(numSelectedChannel);
	        	selectChannel(0, false, false);
        	}
	        dirty = true;
        	repaintGraph();
        }
    }
}

void __fastcall TDocumentForm::FormKeyDown(TObject *Sender, WORD &Key,
	  TShiftState Shift)
{
	bool moveRight;
	int cursorIndex;

	if (Key == VK_F2 && eventChannelSelected)
		addEvent();
	if (Key >= VK_F3 && Key <= VK_F8)
	{
		//F3..F8: cursor operations
		cursorIndex = (Key - VK_F3) / 2;
		if (Shift.Contains(ssShift))
		{
			//shift+Fi: delete event
			if (eventChannelSelected && cursors.c[cursorIndex] != -1)
				removeEventAtCursor(cursorIndex);
		} else {
			//Fi alone: move cursor to next/previous sample/event
			moveRight = (Key - VK_F3) % 2;
			if (cursors.c[cursorIndex] != -1)
			{
                if (annotationChannelSelected)
				{
					int currentEventIndex = measurement.getAnnotationChannel(numSelectedChannel).getIndexClosestToTime(cursors.c[cursorIndex]);
					if (!moveRight && currentEventIndex > 0)
						cursors.c[cursorIndex] = measurement.getAnnotationChannel(numSelectedChannel)[currentEventIndex-1].time;
					else if (moveRight && currentEventIndex < measurement.getAnnotationChannel(numSelectedChannel).getNumElems()-1)
						cursors.c[cursorIndex] = measurement.getAnnotationChannel(numSelectedChannel)[currentEventIndex+1].time;
				}
				else if (eventChannelSelected)
				{
					int currentEventIndex = measurement.getEventChannel(numSelectedChannel).getIndexClosestToTime(cursors.c[cursorIndex]);
					if (!moveRight && currentEventIndex > 0)
						cursors.c[cursorIndex] = measurement.getEventChannel(numSelectedChannel)[currentEventIndex-1].time;
					else if (moveRight && currentEventIndex < measurement.getEventChannel(numSelectedChannel).getNumElems()-1)
						cursors.c[cursorIndex] = measurement.getEventChannel(numSelectedChannel)[currentEventIndex+1].time;
				} else {
					int sampleIndex = measurement.time2SampleIndex(cursors.c[cursorIndex]);
					if (!moveRight && sampleIndex > 0)
						cursors.c[cursorIndex] = measurement.sampleIndex2Time(sampleIndex-1);
					else if (moveRight && sampleIndex < measurement[0].getNumElems()-1)
						cursors.c[cursorIndex] = measurement.sampleIndex2Time(sampleIndex+1);
				}
    			cursors.drawCursors(Chart, measurement, eventChannelSelected, annotationChannelSelected, numSelectedChannel);
			}
			if (cursors.c[cursorIndex] < xLeft || cursors.c[cursorIndex] > xRight)
			{
				double newLeft = cursors.c[cursorIndex] - (xRight - xLeft)/2;
				horzScroll->Position = max(0, min(int(newLeft*double(horzScroll->Max)/measurement.getDuration()), horzScroll->Max));
			}
		}
		refreshStatusLine();
	}
}
//---------------------------------------------------------------------------

void TDocumentForm::refreshStatusLine()
{
	char buf[1000];
	MainForm->StatusLine->SimpleText = "";

	if (nextClickDefinesEventTime)
		MainForm->StatusLine->SimpleText = "DODAJANJE DOGODKA NA MESTO KLIKA! ";

	//status line: x, y at mouse position
	if (numSelectedChannel != -1 && mousePosOnChartX >= xLeft && mousePosOnChartX <= xRight)
	{
		double x, y;
		if (annotationChannelSelected)
		{
			if (nextClickDefinesEventTime)
			{
				//actual mouse position is shown
				x = mousePosOnChartX;
				y = mousePosOnChartY;
			} else {
				//mouse position of the closest event is shown
				int index = measurement.getAnnotationChannel(numSelectedChannel).getIndexClosestToTime(mousePosOnChartX);
				x = measurement.getAnnotationChannel(numSelectedChannel)[index].time;
				y = measurement.getAnnotationChannel(numSelectedChannel)[index].value;
			}
		}else if (eventChannelSelected)
		{
			if (nextClickDefinesEventTime)
			{
				//actual mouse position is shown
				x = mousePosOnChartX;
				y = mousePosOnChartY;
			} else {
				//mouse position of the closest event is shown
				int index = measurement.getEventChannel(numSelectedChannel).getIndexClosestToTime(mousePosOnChartX);
				x = measurement.getEventChannel(numSelectedChannel)[index].time;
				y = measurement.getEventChannel(numSelectedChannel)[index].value;
			}
		} else {
			int index = measurement.time2SampleIndex(mousePosOnChartX);
			x = measurement.sampleIndex2Time(index);
			y = measurement[numSelectedChannel][index];
		}
		sprintf(buf, "Mi�ka: (x= %.3lf, y(x)= %.3lf ) ", x, y);
		MainForm->StatusLine->SimpleText = MainForm->StatusLine->SimpleText + AnsiString(buf);
	}

	//cursor labels
	double x[cursors.numCursors], y[cursors.numCursors];
	for (int i = 0; i < cursors.numCursors; i++)
		if (cursors.c[i] != -1)
   		{
            if (annotationChannelSelected)
			{
				int index = measurement.getAnnotationChannel(numSelectedChannel).getIndexClosestToTime(cursors.c[i]);
				x[i] = measurement.getAnnotationChannel(numSelectedChannel)[index].time;
				y[i] = measurement.getAnnotationChannel(numSelectedChannel)[index].value;
			}
			else if (eventChannelSelected)
			{
				int index = measurement.getEventChannel(numSelectedChannel).getIndexClosestToTime(cursors.c[i]);
				x[i] = measurement.getEventChannel(numSelectedChannel)[index].time;
				y[i] = measurement.getEventChannel(numSelectedChannel)[index].value;
			} else {
				int index = measurement.time2SampleIndex(cursors.c[i]);
				x[i] = measurement.sampleIndex2Time(index);
				y[i] = measurement[numSelectedChannel][index];
			}
		}
    
    sprintf(buf,"%s",measurement.getDate().FormatString("c"));
    lblStartDateTimeVar->Caption = buf;

    unsigned short mHour, mMinute, mSecond, mmSecond; //measurement start date and time
    TDateTime cursorTime;
    double measurementStartTime = double(measurement.getDate());
    
	if (cursors.c[0] < 0)
		sprintf(buf, "");
	else{
        //add x[0]/(num of sec/day) and add to start time
        // EXPLANATION:TDateTime day = 24h, therefore x/86400

        TDateTime cursorTime = (x[0]/86400)+measurementStartTime;
        try{
            cursorTime.DecodeTime(&mHour, &mMinute, &mSecond, &mmSecond);
            sprintf(buf, "%02u:%02u:%02u.%03u, y = %.3lf",mHour,mMinute,mSecond,mmSecond,y[0]);
        }catch(Exception *e){
            //annotationChannelWeirdBehaviour
        }
        }
	lblBlueCursor->Caption = buf;

	if (cursors.c[1] < 0)
		sprintf(buf, "");
	else{
        TDateTime cursorTime = (x[1]/86400)+measurementStartTime;
        try{
        cursorTime.DecodeTime(&mHour, &mMinute, &mSecond, &mmSecond);
        sprintf(buf, "%02u:%02u:%02u.%03u, y = %.3lf",mHour,mMinute,mSecond,mmSecond,y[1]);
        }catch(Exception *e){
            //annotationChannelWeirdBehaviour
        }
        }
	lblGreenCursor->Caption = buf;

	if (cursors.c[2] < 0)
		sprintf(buf, "");
	else{
        TDateTime cursorTime = (x[2]/86400)+measurementStartTime;
        try{
        cursorTime.DecodeTime(&mHour, &mMinute, &mSecond, &mmSecond);
        sprintf(buf, "%02u:%02u:%02u.%03u, y = %.3lf",mHour,mMinute,mSecond,mmSecond,y[2]);
        }catch(Exception *e){
            //annotationChannelWeirdBehaviour
        }
        }
	lblRedCursor->Caption = buf;

	if (cursors.c[0] == 0 || cursors.c[1] < 0)
		sprintf(buf, "");
	else
		sprintf(buf, "dx= %.3lf, dy= %.3lf", x[1]-x[0], y[1]-y[0]);
	lblBlueGreenDiff->Caption = buf;

	if (cursors.c[0] == 0 || cursors.c[2] < 0)
		sprintf(buf, "");
	else
		sprintf(buf, "dx= %.3lf, dy= %.3lf", x[2]-x[0], y[2]-y[0]);
	lblBlueRedDiff->Caption = buf;

	if (cursors.c[1] == 0 || cursors.c[2] < 0)
		sprintf(buf, "");
	else
		sprintf(buf, "dx= %.3lf, dy= %.3lf", x[2]-x[1], y[2]-y[1]);
	lblGreenRedDiff->Caption = buf;
}

void TDocumentForm::calcCoherence()
{
    // selected must be an event channel
    assert(eventChannelSelected && !annotationChannelSelected);

    // propose a number of second event channel
    int secondChannel = numSelectedChannel + 1;
    if (secondChannel >= measurement.getNumEventChannels())
        secondChannel = measurement.getNumEventChannels()-1;

    // show coherence dialog
    CoherenceParamsForm->initDialog(numSelectedChannel, secondChannel, measurement.getNumEventChannels());
    CoherenceParamsForm->ShowModal();

    // continue coherence calculations only if OK has been pressed
    if (CoherenceParamsForm->ModalResult == mrOk) {
        // extract data from coherence dialog (all returned numbers are ok, since they are
        // checked inside the dialog, and "OK" button cannot be pressed if they are not)
        int firstChannel = CoherenceParamsForm->getFirstChannelNum();
        secondChannel = CoherenceParamsForm->getSecondChannelNum();
        int numberOfWins = CoherenceParamsForm->getNumOfWindows();
        double overlapping = CoherenceParamsForm->getOverlap();
        double maxFreq = CoherenceParamsForm->getMaxFreq();

        LibCore::CMeasurement::DFTParams params;
        params = measurement.spectrumCoherence(firstChannel, secondChannel, numberOfWins, overlapping, maxFreq);
        if (params.end < 0){
            measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
            measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
            return;
        }
        measurement.getEventChannel(measurement.getNumEventChannels()-1).refreshMinMaxValue();
        FFTform->Caption = "Koherenca spektrov kanalov D"
            + AnsiString(numSelectedChannel+1) + " in D" + AnsiString(secondChannel+1);
        FFTform->coherence = true;
        FFTform->pMeasurement = &measurement;
        FFTform->pDFTParams = &params;
        FFTform->ShowModal();
        measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
        measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
        measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
        measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
        measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
    }
}

void TDocumentForm::sequentialBRS()
{
	assert(eventChannelSelected);
	char buf[1000];
	sprintf(buf, "Preveri, �e je izbran dogodkovni kanal z RR �asi!\r\n\r\nVnesi �tevilko dogodkovnega kanala za sistoli�ni tlak SBP (%d-%d):\r\n", 1, measurement.getNumEventChannels());
	int pressureChannel = InstantDialog::query("Sekven�na BRS analiza.", buf, "1").ToInt()-1;
	if (pressureChannel < 0 || pressureChannel > measurement.getNumEventChannels()-1)
		{
			Application->MessageBox("Ta kanal ne obstaja.", "sBRS - Opozorilo!" , MB_OK);
			return;
		}

	double dRR = 0.001*InstantDialog::query("sBRS analiza.", "Vnesi druga�no vrednost za dRR[ms]: ", "5").ToDouble();
	double dBP = InstantDialog::query("sBRS analiza.", "Vnesi druga�no vrednost za dBP[mmHg]: ", "1").ToDouble();

	int oldNumEventChannels = measurement.getNumEventChannels();
	LibCore::CMeasurement::BRSReturn brs = measurement.sequentialBRS(numSelectedChannel, pressureChannel, dRR, dBP);
	Application->MessageBox(brs.comment.c_str(), "Rezultati sBRS-Up!" , MB_OK);

	if (measurement.getNumEventChannels() > oldNumEventChannels)
	{
		selectChannel(measurement.getNumEventChannels()-1, true, false);
		dirty = true;
		refreshWholeWindow();
	}
}
void TDocumentForm::correlationxBRS()
{
	assert(eventChannelSelected);
	char buf[1000];
	sprintf(buf, "Preveri, �e je izbran dogodkovni kanal z RR �asi!\r\n\r\nVnesi �tevilko dogodkovnega kanala za sistoli�ni tlak SBP (%d-%d):\r\n", 1, measurement.getNumEventChannels());
	int pressureChannel = InstantDialog::query("Korelacijska BRS analiza.", buf, "1").ToInt()-1;
    if (pressureChannel < 0 || pressureChannel > measurement.getNumEventChannels()-1)
		{
			Application->MessageBox("Ta kanal ne obstaja.", "xBRS - Opozorilo!" , MB_OK);
			return;
		}
	int oldNumEventChannels = measurement.getNumEventChannels();
	LibCore::CMeasurement::BRSReturn brs = measurement.correlationxBRS(numSelectedChannel, pressureChannel, 0.75, 0, 11);
	Application->MessageBox(brs.comment.c_str(), "Rezultati xBRS!" , MB_OK);
	if (measurement.getNumEventChannels() > oldNumEventChannels)
	{
		selectChannel(measurement.getNumEventChannels()-1, true, false);
		dirty = true;
		refreshWholeWindow();
	}
}
void TDocumentForm::sinhronizedrBRS()
{
	assert(eventChannelSelected);
	char buf[1000];

	//systolic pressure
	sprintf(buf, "Preveri, �e je izbran dogodkovni kanal z RR �asi in, �e sta postavljena modri in zeleni kurzor!\r\n\r\nVnesi �tevilko dogodkovnega kanala za sistoli�ni tlak SBP (%d-%d):\r\n", 1, measurement.getNumEventChannels());
	int pressureChannel = InstantDialog::query("Sinhronizirana BRS analiza.", buf, "1").ToInt()-1;
	if (pressureChannel < 0 || pressureChannel > measurement.getNumEventChannels()-1)
		{
			Application->MessageBox("Ta kanal ne obstaja.", "rBRS - Opozorilo!" , MB_OK);
			return;
		}

	// respiration
	sprintf(buf, "Vnesi �tevilko dogodkovnega kanala s �asi izdiha ali vdiha (%d-%d):\r\n", 1, measurement.getNumEventChannels());
	int respirationChannel = InstantDialog::query("Sinhronizirana BRS analiza.", buf, "1").ToInt()-1;
	if (respirationChannel < 0 || respirationChannel > measurement.getNumEventChannels()-1)
	{
		Application->MessageBox("Ta kanal ne obstaja.", "rBRS - Opozorilo!" , MB_OK);
		return;
	}

	int oldNumEventChannels = measurement.getNumEventChannels();
	LibCore::CMeasurement::BRSReturn brs = measurement.sinhronizedrBRS(numSelectedChannel, pressureChannel, respirationChannel, &cursors);
	Application->MessageBox(brs.comment.c_str(), "Rezultati rBRS!" , MB_OK);

	if (measurement.getNumEventChannels() > oldNumEventChannels)
	{
		selectChannel(measurement.getNumEventChannels()-1, true, false);
		dirty = true;
		refreshWholeWindow();
	}
}
void TDocumentForm::frequencyfBRS()
{
	assert(eventChannelSelected);
	char buf[1000];
	sprintf(buf, "Preveri, �e je izbran dogodkovni kanal z RR �asi!\r\n\r\nVnesi �tevilko dogodkovnega kanala za sistoli�ni tlak SBP (%d-%d):\r\n", 1, measurement.getNumEventChannels());
	int pressureChannel = InstantDialog::query("Frekven�na f-BRS analiza.", buf, "1").ToInt()-1;
    if (pressureChannel < 0 || pressureChannel > measurement.getNumEventChannels()-1)
	{
		Application->MessageBox("Ta kanal ne obstaja.", "fBRS - Opozorilo!" , MB_OK);
		return;
	}
	int oldNumEventChannels = measurement.getNumEventChannels(), oldSelectedEventCh = numSelectedChannel;
	LibCore::CMeasurement::BRSReturn brs = measurement.frequencyfBRS(numSelectedChannel, pressureChannel);
	if (measurement.getNumEventChannels() > oldNumEventChannels)
	{
		selectChannel(measurement.getNumEventChannels()-1, true, false);
		dirty = true;
		refreshWholeWindow();
	}

	char buff[1000];
	sprintf(buff, "\r\nFrekven�na f-BRS analiza.\r\nalfaLF[0,04-0,15Hz]: %0.2f[ms/mmHg]\r\nalfaHF[0,15-0,4Hz]: %0.2f[ms/mmHg]",
		brs.alphaLF, brs.alphaHF);
	Application->MessageBox(buff, "Frekven�na f-BRS analiza." , MB_OK);
	measurement.getEventChannel(numSelectedChannel).appendComment(buff);

	// delete DFT channels
	selectChannel(oldSelectedEventCh, true, false);
  	//measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
	//measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
	refreshWholeWindow();
}

void TDocumentForm::everyKnownBRS() {
	// selected must be an event channel
	assert(eventChannelSelected);

	// remember current state of measurement
	int oldNumEventChannels = measurement.getNumEventChannels();
	
	// propose a number for the second event channel
	int secondChannel = numSelectedChannel + 1;
	if (secondChannel >= measurement.getNumEventChannels())
	secondChannel = measurement.getNumEventChannels()-1;

	BRSForm->initDialog(numSelectedChannel, secondChannel, measurement, cursors);
	BRSForm->autoCopyToClipboard = autoCopyToClipboard;
	BRSForm->ShowModal();

	// measurement state changed?
	if (measurement.getNumEventChannels() > oldNumEventChannels)
	{
		selectChannel(measurement.getNumEventChannels()-1, true, false);
		dirty = true;
		refreshWholeWindow();
	}
}

void TDocumentForm::differentialChannel(double errorValue, bool absolute) {
    if (annotationChannelSelected)
        return;
    if (eventChannelSelected) {
        if(!absolute)
            measurement.eventDifferential(numSelectedChannel);
        else
            measurement.eventDifferentialAbsolute(numSelectedChannel);
    } else {
        if(!absolute)
        	measurement.signalDifferential(numSelectedChannel, errorValue);
        else
           	measurement.signalDifferentialAbsolute(numSelectedChannel, errorValue);
    }
    selectChannel(measurement.getNumEventChannels()-1, true, false);
	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::resampleSignalChannel()
{
	assert(!eventChannelSelected && !annotationChannelSelected);
    AnsiString oldVal(measurement.getSamplingRate());
	double freq = LibCore::myAtof(InstantDialog::query("Prevzor�enje signalnega kanala", "Vnesi novo frekvenco v Hz: ", oldVal));
	measurement.resampleSignalChannel(numSelectedChannel, freq);
    selectChannel(measurement.getNumEventChannels()-1, true, false);
	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::resampleEventChannel()
{
	assert(eventChannelSelected);
	double freq = LibCore::myAtof(InstantDialog::query("Prevzor�enje", "Vnesi novo frekvenco v Hz: ", "1"));
	measurement.resampleEventChannel(numSelectedChannel, freq);
    selectChannel(measurement.getNumEventChannels()-1, true, false);
	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::undo()
{
	assert(!undoStack.isEmpty());
	CUndoStack::UndoableAction action = undoStack.pop();

	switch (action.actionType) {
	case CUndoStack::UndoableAction::actionTypeDeleteEvent:
	{
		//find first existing event AFTER the clicked time
		LibCore::CEventChannel &eventChannel = measurement.getEventChannel(action.channelNum);
		int nextEventIndex = eventChannel.getIndexClosestToTime(action.event.time);
		LibCore::CEventChannel::EventType eventType = eventChannel.getEventType(nextEventIndex);
		if (eventChannel[nextEventIndex].time < action.event.time)
			nextEventIndex++;

		//add event; set its & next's value
		eventChannel.insertElem(nextEventIndex, action.event);
		if (nextEventIndex+1 < eventChannel.getNumElems())
			eventChannel.correctEventValue(nextEventIndex+1, eventType);
		eventChannel.correctEventValue(nextEventIndex, eventType);
		eventChannel.refreshMinMaxValue();
		break;
	}
	case CUndoStack::UndoableAction::actionTypeAddEvent:
	{
		LibCore::CEventChannel &eventChannel = measurement.getEventChannel(action.channelNum);
		int eventIndex = eventChannel.getIndexClosestToTime(action.event.time);
		LibCore::CEventChannel::EventType eventType = eventChannel.getEventType(eventIndex);   //find out what type if event channel it is
		eventChannel.deleteElems(eventIndex, eventIndex);  //delete event
		if (eventIndex < eventChannel.getNumElems()) //correct value of event after the deleted one
			eventChannel.correctEventValue(eventIndex, eventType);
		eventChannel.refreshMinMaxValue();
		break;
	}
	default:
		throw new LibCore::EKGException(__LINE__, "TDocumentForm::undo: cannot undo this action");
	}

	dirty = true;
	repaintGraph();
	refreshStatusLine();
}

void TDocumentForm::createBPEventChannel()
{
	char buff[1000];
	int signalChannel, existingBeatChannel;
	if (measurement.getNumEventChannels() == 0)
	{
		Application->MessageBox("Noben dogodkovni kanal ne obstaja.", "Napaka!", MB_OK);
		return;
	}
	if (eventChannelSelected)
	{
		existingBeatChannel = numSelectedChannel;
		sprintf(buff, "Vnesi �tevilko signalnega kanala, ki prikazuje izmerjeni tlak (%d-%d)\r\n(Izbran mora biti dogodkovni kanal z RRI.):", 1, measurement.getNumChannels());
		signalChannel = InstantDialog::query("Sistoli�ni tlak", buff, "1").ToInt()-1;
   		if (signalChannel < 0 || signalChannel > measurement.getNumChannels()-1)
		{
			Application->MessageBox("Ta kanal ne obstaja.", "Dolo�anje tlaka - Opozorilo!" , MB_OK);
			return;
		}

	} else {
		signalChannel = numSelectedChannel;
		sprintf(buff, "Vnesi �tevilko dogodkovnega kanala z RRI (%d-%d)\r\n(Izbran mora biti signalni kanal, ki prikazuje izmerjeni tlak.):", 1, measurement.getNumEventChannels());
		existingBeatChannel = InstantDialog::query("Sistoli�ni tlak", buff, "1").ToInt()-1;
		if (existingBeatChannel < 0 || existingBeatChannel > measurement.getNumEventChannels()-1)
		{
			Application->MessageBox("Ta kanal ne obstaja.", "Dolo�anje tlaka - Opozorilo!" , MB_OK);
			return;
		}

	}
	measurement.createBPEventChannel(signalChannel, existingBeatChannel);
	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::refreshWholeWindow(bool automatic)
{
	for (int i = measurement.getNumChannels() - 1; i >= 0; --i) {
		if (measurement[i].getNumElems() <= 0) {
			measurement.deleteChannel(i);
			if(!automatic)
                Application->MessageBox("Kanal z 0 vrednostmi je bil izbrisan", "Opozorilo!", MB_OK);
		}
	}
	for (int i = measurement.getNumEventChannels() - 1; i >= 0; --i) {
		if (measurement.getEventChannel(i).getNumElems() <= 0) {
			measurement.deleteEventChannel(i);
            if(!automatic)
			    Application->MessageBox("Kanal z 0 dogodki je bil izbrisan", "Opozorilo!", MB_OK);
		}
	}

    if (annotationChannelSelected){
        if (numSelectedChannel >= measurement.getNumAnnotationChannels())
			selectChannel(measurement.getNumChannels()-1, false, false);
    }
	else if (eventChannelSelected) {
		if (numSelectedChannel >= measurement.getNumEventChannels())
			selectChannel(measurement.getNumChannels()-1, false, false);
	} else {
        selectChannel(min(numSelectedChannel, measurement.getNumChannels()-1), false, false);
	}


	fillGridChannelView();
	measurement.refreshAllMinMaxValues();
	repaintGraph();
	refreshStatusLine();
}

void TDocumentForm::deleteChannel()
{
    if (annotationChannelSelected)
	{
		measurement.deleteAnnotationChannel(numSelectedChannel);
		int newSelectedChannel = min(numSelectedChannel, measurement.getNumAnnotationChannels()-1);
		if (newSelectedChannel == -1)
			selectChannel(measurement.getNumChannels()-1, false, false);
		else
			selectChannel(0, true, true);
	}
	else if (eventChannelSelected && !annotationChannelSelected)
	{
		measurement.deleteEventChannel(numSelectedChannel);
		int newSelectedChannel = min(numSelectedChannel, measurement.getNumEventChannels()-1);
		if (newSelectedChannel == -1)
			selectChannel(measurement.getNumChannels()-1, false, false);
		else
			selectChannel(newSelectedChannel, true, false);
	} else {
		if (measurement.getNumChannels() == 1)
		{
			Application->MessageBox("Brisanje edinega signalnega kanala ni mo�no.", "VisECG", MB_OK);
			return;
		}
		measurement.deleteChannel(numSelectedChannel);
		selectChannel(min(numSelectedChannel, measurement.getNumChannels()-1), false, false);
	}
	refreshWholeWindow();
	dirty = true;
}

void TDocumentForm::duplicateChannel()
{
	if (annotationChannelSelected) {
		measurement.duplicateAnnotationChannel(numSelectedChannel);
        selectChannel(measurement.getNumAnnotationChannels()-1, true, true);
	}else if (eventChannelSelected) {
		measurement.duplicateEventChannel(numSelectedChannel);
        selectChannel(measurement.getNumEventChannels()-1, true, false);
	}else {
		measurement.duplicateChannel(numSelectedChannel);
        selectChannel(measurement.getNumChannels()-1, false, false);
    }
    refreshWholeWindow();
	dirty = true;
}

void TDocumentForm::shiftChannel()
{
	assert(eventChannelSelected && !annotationChannelSelected);
	double offset = LibCore::myAtof(InstantDialog::query("Zamik dogodkovnega kanala",
			"Vnesi �as v [s], za katerega naj se zamakne kanal\r\n(pozitivne vrednosti za zamik v desno, negativne v levo)", "0"));
	measurement.getEventChannel(numSelectedChannel).shiftChannel(offset, measurement.getDuration());
	for (int i = 0; i < 3; i++)
	{
		if (cursors.c[i] != -1)
			cursors.c[i] += offset;
		if (cursors.c[i] < 0 || cursors.c[i] > measurement.getDuration())
			cursors.c[i] = -1;
	}
	refreshWholeWindow();
	dirty = true;
}

void TDocumentForm::offsetChannel()
{
	assert(eventChannelSelected && !annotationChannelSelected);  
	double average = 0;
	for (int i = 0; i < measurement.getEventChannel(numSelectedChannel).getNumElems(); i++)
		average += measurement.getEventChannel(numSelectedChannel)[i].value;
	average /= measurement.getEventChannel(numSelectedChannel).getNumElems();
	measurement.offsetEventChannel(numSelectedChannel, -average);
	refreshWholeWindow();
	dirty = true;
}

void TDocumentForm::detrendChannel()
{
	assert(eventChannelSelected && !annotationChannelSelected);
	measurement.detrendEventChannel(numSelectedChannel);
	refreshWholeWindow();
	dirty = true;
}

void TDocumentForm::createRTEventChannel()
{
	assert(eventChannelSelected);
	char buf[1000];
	sprintf(buf, "Vnesi �tevilko dogodkovnega kanala s PP intervali (%d-%d)", 1, measurement.getNumEventChannels());
	int TTchannel = InstantDialog::query("Izra�un RT intervalov", buf, "1").ToInt()-1;
	if (TTchannel < 0 || TTchannel > measurement.getNumEventChannels()-1)
		{
			Application->MessageBox("Ta kanal ne obstaja.", "RT intervali - Opozorilo!" , MB_OK);
			return;
		}

	int oldNumEventChannels = measurement.getNumEventChannels();
	measurement.createRTEventChannel(numSelectedChannel, TTchannel);
	if (measurement.getNumEventChannels() > oldNumEventChannels)
	{
		selectChannel(measurement.getNumEventChannels()-1, true, false);
		dirty = true;
		refreshWholeWindow();
	}
}

void TDocumentForm::selectChannel(int numChannel, bool eventChannel, bool annotationChannel)
{
	assert(numChannel >= 0);
    if(annotationChannel)
    {
    	assert(numChannel < measurement.getNumAnnotationChannels());
		numSelectedChannel = numChannel;
		eventChannelSelected = true; // we force this, 
        annotationChannelSelected = annotationChannel;
		measurement.getAnnotationChannel(numChannel).viewProps.visible = true;
    }
	else if (eventChannel)
	{
		assert(numChannel < measurement.getNumEventChannels());
		numSelectedChannel = numChannel;
        annotationChannelSelected = annotationChannel;
		eventChannelSelected = eventChannel;
		measurement.getEventChannel(numChannel).viewProps.visible = true;
	} else {
		assert(numChannel < measurement.getNumChannels());
		numSelectedChannel = numChannel;
        annotationChannelSelected = annotationChannel;
		eventChannelSelected = eventChannel;
		measurement[numChannel].viewProps.visible = true;
	}
}


void TDocumentForm::showAveragerWindow() {
    AveragerForm->initDialog(numSelectedChannel, eventChannelSelected, measurement, cursors);
    AveragerForm->setFilename(measurement.getMeasurementName());
    AveragerForm->ShowModal();
}


/// za viktorja +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void TDocumentForm::averageBeatAndsubtract() {
    if (eventChannelSelected || annotationChannelSelected)
        return;

    double startSampleTime, endSampleTime;
    if ((cursors.c[0] < 0) || ((cursors.c[1] < 0) && (cursors.c[2] < 0))) {
        Application->MessageBox("Ne najdem enega ali ve�ih kurzorjev.", "Napaka", MB_OK);
        return;
    } else {
        startSampleTime = cursors.c[0], endSampleTime = (cursors.c[1] < 0 ? cursors.c[2]: cursors.c[1]);
    }

    int  					rriNum;
	AnsiString				rriName[] = {"RRI iz M"};
	findChannelByNameStart(rriName, 1, rriNum, true); 
    AnsiString queryTitle("Vpis parametrov");
    do {
        AnsiString answer;
        if (!InstantDialog::queryOKCancel(queryTitle.c_str(), 
            "Vpi�i �tevilko dogodkovnega kanala, ki ozna�uje utripe (npr RRI kanal)", rriNum+1, answer))
            return;
        rriNum = answer.ToInt()-1;
        queryTitle = "Napaka, ponoven vpis parametrov";
    } while ((rriNum < 0) || (rriNum > measurement.getNumEventChannels()));
    LibCore::CEventChannel& rriChan = measurement.getEventChannel(rriNum);
    LibCore::CChannel& channel = measurement[numSelectedChannel];

    // locate beat closest to the cursors, which will serve as a base for the selection area
    int startSample = measurement.time2SampleIndex(startSampleTime);
    int endSample = measurement.time2SampleIndex(endSampleTime);
    int sampleSize = endSample - startSample + 1;
    int sampleBeat = rriChan.getIndexClosestToTime(
        (startSampleTime + endSampleTime) * 0.5);
    double timeDif = rriChan[sampleBeat].time - startSampleTime;
    
    // calculate average for the selected sample
    std::vector<double> averageSample(sampleSize, 0.0);
    std::vector<int> averageSampleCnt(sampleSize, 0);
    for (int i = 0; i < rriChan.getNumElems(); ++i) {
        if (rriChan[i].time < timeDif)
            continue;
        int beatIndex = measurement.time2SampleIndex(
            rriChan[i].time - timeDif);
        for (int j = beatIndex, cnt = 0; cnt < sampleSize; ++j, ++cnt) {
            if ((j >= 0) && (j < channel.getNumElems())) {
                ++averageSampleCnt[cnt];
                averageSample[cnt] += channel[j];
            }
        }
    }    
    for (int cnt = 0; cnt < sampleSize; ++cnt)
        averageSample[cnt] /= averageSampleCnt[cnt];
    
    // detrend the average sample and pin it to zero
    double k = (averageSample.back() - averageSample.front()) / sampleSize;
    double n = averageSample.front();
    
    for (int cnt = 0; cnt < sampleSize; ++cnt)
        averageSample[cnt] -= cnt*k + n;

    // subtract the average sample from the signal
    for (int i = 0; i < rriChan.getNumElems(); ++i) {
        if (rriChan[i].time < timeDif)
            continue;
        
        int beatIndex = measurement.time2SampleIndex(
            rriChan[i].time - timeDif);
        for (int j = beatIndex, cnt = 0; cnt < sampleSize; ++j, ++cnt) {
            if ((j >= 0) && (j < channel.getNumElems())) {
                channel[j] -= averageSample[cnt];
            }
        }
    }

    channel.tenBitSaving = false;
    dirty = true;
    channel.refreshMinMaxValue();
    refreshWholeWindow();
}


void TDocumentForm::removeBeatLinear() {
    if (eventChannelSelected || annotationChannelSelected)
        return;

    double startSampleTime, endSampleTime;
    if ((cursors.c[0] < 0) || ((cursors.c[1] < 0) && (cursors.c[2] < 0))) {
        Application->MessageBox("Ne najdem enega ali ve�ih kurzorjev.", "Napaka", MB_OK);
        return;
    } else {
        startSampleTime = cursors.c[0], endSampleTime = (cursors.c[1] < 0 ? cursors.c[2]: cursors.c[1]);
    }

    int  					rriNum;
	AnsiString				rriName[] = {"RRI iz M"};
	findChannelByNameStart(rriName, 1, rriNum, true); 
    AnsiString queryTitle("Vpis parametrov");
    do {
        AnsiString answer;
        if (!InstantDialog::queryOKCancel(queryTitle.c_str(), 
            "Vpi�i �tevilko dogodkovnega kanala, ki ozna�uje utripe (npr RRI kanal)", rriNum+1, answer))
            return;
        rriNum = answer.ToIntDef(0)-1;
        queryTitle = "Napaka, ponoven vpis parametrov";
    } while ((rriNum < 0) || (rriNum > measurement.getNumEventChannels()));
    LibCore::CEventChannel& rriChan = measurement.getEventChannel(rriNum);
    LibCore::CChannel& channel = measurement[numSelectedChannel];

    // locate beat closest to the cursors, which will serve as a base for the selection area
    int startSample = measurement.time2SampleIndex(startSampleTime);
    int endSample = measurement.time2SampleIndex(endSampleTime);
    int sampleSize = endSample - startSample + 1;
    int sampleBeat = rriChan.getIndexClosestToTime(
        (startSampleTime + endSampleTime) * 0.5);
    double timeDif = rriChan[sampleBeat].time - startSampleTime;
    
    // replace the signal with linear function (in every beat)
    for (int i = 0; i < rriChan.getNumElems(); ++i) {
        if (rriChan[i].time < timeDif)
            continue;

        int beatIndex = measurement.time2SampleIndex(rriChan[i].time - timeDif);
        if (beatIndex + sampleSize >= channel.getNumElems()) 
            continue;
        
        // determine the linear function
        double k = (channel[beatIndex + sampleSize] - channel[beatIndex]) / sampleSize;
        double n = channel[beatIndex];
        
        for (int j = beatIndex, cnt = 0; cnt < sampleSize; ++j, ++cnt) {
            if ((j >= 0) && (j < channel.getNumElems())) {
                channel[j] = n + k*cnt;
            }
        }
    }
    channel.tenBitSaving = false;
    dirty = true;
    channel.refreshMinMaxValue();
    refreshWholeWindow();
}


void TDocumentForm::addMeasurementChannel() {
    if (eventChannelSelected || annotationChannelSelected)
        return;
    
    AnsiString queryTitle("Vpis parametrov");
    int addChannelNum = measurement.getNumChannels();
    do {
        AnsiString answer;
        if (!InstantDialog::queryOKCancel(queryTitle.c_str(), 
            "Vpi�i �tevilko merilnega kanala, ki ga �eli� pri�teti", addChannelNum+1, answer) )
            return;
        addChannelNum = answer.ToIntDef(0)-1;
        queryTitle = "Napaka, ponoven vpis parametrov!";
    } while ((addChannelNum < 0) || (addChannelNum > measurement.getNumChannels()));
    LibCore::CChannel& target = measurement[numSelectedChannel];
    LibCore::CChannel& source = measurement[addChannelNum];

    for (int i = 0; i < target.getNumElems(); ++i) {
        target[i] += source[i];
    }
    target.appendComment("\r\nPri�tet kanal M"+AnsiString(numSelectedChannel));
    target.tenBitSaving = false;

    dirty = true;
    target.refreshMinMaxValue();
    refreshWholeWindow();
}


/// predefined scripts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void TDocumentForm::generateEventChannels(int ecgChan, int bpChan, int respChan) {
	// script description:
	//   preconditions:
	//	   ECG in measurement channel ecgChan
	//	   blood pressure in measurement channel bpChan
	//     [optional] respiration in measurement channel
	//   produces:
	//     filtered ECG measurement channel (-2 or -3)
	//     filtered blood pressure channel (-1 or -2)
	//	   if respiration measurement channel exists
	//       filtered respiration channel (-1)
	//     RRI event channel (-5 or -3), with detrend and removal of extremes
	//     SBP event channel (-4 or -2)
	//     DBP event channel (-3 or -1)
	//     if respiration measurement channel does not exist
	//       respiration SBP event channel (-2)
	//	     respiration DBP event channel (-1)
	//

	assert( (ecgChan < measurement.getNumChannels()) && (ecgChan >= 0) );
	assert( (bpChan < measurement.getNumChannels()) && (bpChan >= 0) );
	assert( respChan < measurement.getNumChannels() );

	if ((respChan >= 0) && ((cursors.c[0] < 0) || (cursors.c[1] < 0)) ) {
		Application->MessageBox("Modri in zeleni kurzor morata biti nastavljena tako, da ozna�ujeta en val v meritvi dihanja", "Napaka", MB_OK);
		return;
	}

	// measurement will be changed
	dirty = true;

	// filter signals (rectangular, 50Hz); alternative for triangular window: LibCore::CMeasurement::windowTriangular
	measurement.lowPassFilter(LibCore::CMeasurement::windowRectangular, ecgChan, 50);
	measurement.lowPassFilter(LibCore::CMeasurement::windowRectangular, bpChan, 50);
	ecgChan = measurement.getNumChannels() - 2;
	bpChan = measurement.getNumChannels() - 1;
	if (respChan >= 0) {
		measurement.lowPassFilter(LibCore::CMeasurement::windowRectangular, respChan, 20);
		respChan = measurement.getNumChannels() - 1;
	}

	// create RRI, SBP and DBP channels from filtered inputs
	measurement.calculateBeatTimes(LibCore::CMeasurement::beatAlgorithmFZ, ecgChan, &cursors);
	const int  			  	rriChan = measurement.getNumEventChannels() - 1;
	measurement.createBPEventChannel(bpChan, rriChan);
	const int				sbpChan = measurement.getNumEventChannels() - 2;
	// dbpchan = measurement.getNumEventChannels() - 1

	// detrend and filter out extreme events
	// detrend improves statistics in frequency space but messes them up in time space
	// measurement.detrendEventChannel(rriChan);

	if (respChan < 0) {
		// create >respiration< channels
		measurement.createBPEventChannel(ecgChan, sbpChan);
		const int				respChan1 = measurement.getNumEventChannels() - 2, respChan2 = measurement.getNumEventChannels() - 1;

		measurement.getEventChannel(respChan1).setChannelName(AnsiString("AR"));
		measurement.getEventChannel(respChan2).setChannelName(AnsiString("AS"));
	} else {
		LibCore::CMeasurement::BeatTimesParams		params(LibCore::CMeasurement::beatAlgorithmFCOREL,
			respChan, &cursors);
		params.existBeatNum = -1;
		params.minCorel = 0.8;

		measurement.calculateBeatTimes(params);
		int corelChan = measurement.findChannelByName("Korelacija", respChan, true);
		if (corelChan != -1) {
			measurement.deleteChannel(corelChan);
		}
		int ppiRespChan = measurement.getNumEventChannels() - 1;
		measurement.getEventChannel(ppiRespChan).setChannelName(AnsiString("dihanje ") +
		    measurement.getEventChannel(ppiRespChan).getChannelName());
	}

	selectChannel(sbpChan, true, false);
	refreshWholeWindow();
}


void TDocumentForm::standardAnalysis() {
	// script description:
	//   preconditions:
	//	   generateEventChannels was run or equivalent calculations were done manually
	//     events were checked and eventual noise in data was removed manually
	//     if respiration channel is available, cursors are set to mark one respiratory wave
	//   produces:
	//     a row of data in the clipboard:
	//        RESavg	RESmax	RESmin	RESSD	RESRMSN	RRIavg	RRImax	RRImin	RRISD	RRIRMSN	RRILF	RRIHF	RRILFn	RRIHFn	RRILF/HF	SBPavg	SBPmax	SBPmin	SBPSD	SBPRMSN	SBPLF	SBPHF	SBPLFn	SBPHFn	SBPLF/HF	KOHLF	KOHLF1stPeakValue	KOHLF1stPeakFreq	KOHLF2ndPeakValue	KOHLF2ndPeakFreq	KOHLFFAZA	KOHHF	KOHHF1stPeakValue	KOHHF1stPeakFreq	KOHHF2ndPeakValue	KOHHF2ndPeakFreq	KOHHFFAZA	sBRSUP	sBRSUPCOUNT	sBRSDOWN	sBRSDOWNCOUNT	xBRS	xBRSCOUNT	AlfaLF	AlfaHF	rBRS	rBRSDelay	rBRSRespCount
	//     statistics for respiration (RES variables are set to '?' when respiration not available), RRI and SBP channels
	//     frequency analysis for RRI and SBP channels
	//	   coherence between RRI and SBP
	// 	   BRS results (from all methods, with rBRS disabled when respiration is not available)
	//	   event channels for all forms of BRS that require them
	//
	//

	// keep an eye on shifty events (for now only in BRS calculus)
	bool						errorsDetected = false;

	dirty = true;

	// do an automatic scan for respiratory, RRI and SBP channels with standard names as a
	// matching criteria; first channel matching the criteria is used if multiple are found;
	// an exception is respiratory channel which is not used if multiple are found because
	// 2 are ususlly made from ECG measurement when 'true' respiration measurement is missing
	// and these two are of a very poor quality
	int  					rriChan, sbpChan, respChan;
	AnsiString				rriName[] = {"RRI"};
	AnsiString				sbpName[] = {"SBP"};
	AnsiString				respName[] = {"dihanje"};
	findChannelByNameStart(rriName, 1, rriChan, true);
	findChannelByNameStart(sbpName, 1, sbpChan, true);
	bool singleRespChan = findChannelByNameStart(respName, 1, respChan, true);
      
	/*
	// debug - which event channels were found by name?
	char buf[10000];
	sprintf(buf, "%i %i %i %s rsp.ch. found", rriChan+1, sbpChan+1, respChan+1,
		(singleRespChan ? "one" : (respChan == -1 ? "zero" : "two or more")));
	Application->MessageBox(buf, "!!!", MB_OK);
	*/

	// Warn user if not all channels are found or cursors are not set when they should be
	if ((rriChan < 0) || (sbpChan < 0)) {
		Application->MessageBox("Ne najdem dogodkovnih kanalov RRI in/ali SBP", "Napaka", MB_OK);
		return;
	}

	if ((singleRespChan) && ((cursors.c[0] < 0) || (cursors.c[1] < 0))) {
		Application->MessageBox("Kurzorja morata biti nastavljena tako, da ozna�ujeta en val v meritvi dihanja", "Napaka", MB_OK);
		return;
	}

	// coherence params
	const int				numberOfWins = 8;
	const double 			overlapping = 0.5;

	// clipboard helper variables
//	char 					clipBuf[10000];
//	clipBuf[0] = 0;
//	using namespace std;
//	stringstream			clipText;
    AnsiString              clipTextAnsi;

	if (singleRespChan) {
		// respiratory statistics
		LibCore::CEventChannel::Statistics stat = measurement.getEventChannel(respChan).calcStatistics();
//		clipText << setw(4) <<
// 			stat.avgValue << "\t" << stat.maxValue << "\t" <<
//			stat.minValue << "\t" << stat.stDevValue << "\t" <<
//			stat.RMSSD << "\t";
        clipTextAnsi = AnsiString::FloatToStrF(stat.avgValue, AnsiString::sffFixed, 4, 3) + "\t"
            + AnsiString::FloatToStrF(stat.maxValue, AnsiString::sffFixed, 4, 3) + "\t"
            + AnsiString::FloatToStrF(stat.minValue, AnsiString::sffFixed, 4, 3) + "\t"
            + AnsiString::FloatToStrF(stat.stDevValue, AnsiString::sffFixed, 4, 3) + "\t"
            + AnsiString::FloatToStrF(stat.RMSSD, AnsiString::sffFixed, 4, 3) + "\t";

			/*
		LibCore::CMeasurement::DFTParams params = measurement.fourierTransform(respChan);
		FFTform->coherence = false;
		FFTform->pMeasurement = &measurement;
		FFTform->pDFTParams = &params;
		FFTform->autoCopyToClipboard = false;
		FFTform->initForm();
		TStringGrid*	   		gridStats = FFTform->getGridStats();
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		clipText << gridStats->Cells[1][9].c_str() << "\t" <<
			gridStats->Cells[1][10].c_str() << "\t" << gridStats->Cells[1][11].c_str() << "\t" <<
			gridStats->Cells[1][12].c_str() << "\t" << gridStats->Cells[1][13].c_str() << "\t";
			*/
	} else {
//		clipText << "?\t?\t?\t?\t?\t";
        clipTextAnsi += "?\t?\t?\t?\t?\t";
	}

	// rri statistics and DFT (DFT channels discarded afterwards)
	LibCore::CEventChannel::Statistics statRri = measurement.getEventChannel(rriChan).calcStatistics();
//	clipText << setw(4) <<
//		statRri.avgValue*1000.0 << "\t" << statRri.maxValue*1000.0 << "\t" <<
//		statRri.minValue*1000.0 << "\t" << statRri.stDevValue*1000.0 << "\t" <<
//		statRri.RMSSD*1000.0 << "\t";

    clipTextAnsi += AnsiString::FloatToStrF(statRri.avgValue*1000.0, AnsiString::sffFixed, 4, 3) + "\t"
        + AnsiString::FloatToStrF(statRri.maxValue*1000.0, AnsiString::sffFixed, 4, 3) + "\t"
        + AnsiString::FloatToStrF(statRri.minValue*1000.0, AnsiString::sffFixed, 4, 3) + "\t"
        + AnsiString::FloatToStrF(statRri.stDevValue*1000.0, AnsiString::sffFixed, 4, 3) + "\t"
        + AnsiString::FloatToStrF(statRri.RMSSD*1000.0, AnsiString::sffFixed, 4, 3) + "\t";

	// DFT is retrieved through a minor FFTform hack - let FFTform compute everything but without making it visible
	LibCore::CMeasurement::DFTParams rriParams = measurement.fourierTransform(rriChan);
	FFTform->coherence = false;
	FFTform->pMeasurement = &measurement;
	FFTform->pDFTParams = &rriParams;
	FFTform->autoCopyToClipboard = false;
	FFTform->initForm();
	TStringGrid*				gridStats = FFTform->getGridStats();
	assert(gridStats);
	measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
//	clipText << gridStats->Cells[1][9].c_str() << "\t" <<
//		gridStats->Cells[1][10].c_str() << "\t" << gridStats->Cells[1][11].c_str() << "\t" <<
//		gridStats->Cells[1][12].c_str() << "\t" << gridStats->Cells[1][13].c_str() << "\t";

    clipTextAnsi += gridStats->Cells[1][9] + "\t"
        + gridStats->Cells[1][10] + "\t" + gridStats->Cells[1][11] + "\t"
		+ gridStats->Cells[1][12] + "\t" + gridStats->Cells[1][13] + "\t";

	// sbp statistics and DFT
	LibCore::CEventChannel::Statistics statSbp = measurement.getEventChannel(sbpChan).calcStatistics();
//	clipText << setw(4) <<
//		statSbp.avgValue << "\t" << statSbp.maxValue << "\t" <<
//		statSbp.minValue << "\t" << statSbp.stDevValue << "\t" <<
//		statSbp.RMSSD << "\t";

    clipTextAnsi += AnsiString::FloatToStrF(statSbp.avgValue, AnsiString::sffFixed, 4, 3) + "\t"
        + AnsiString::FloatToStrF(statSbp.maxValue, AnsiString::sffFixed, 4, 3) + "\t"
        + AnsiString::FloatToStrF(statSbp.minValue, AnsiString::sffFixed, 4, 3) + "\t"
        + AnsiString::FloatToStrF(statSbp.stDevValue, AnsiString::sffFixed, 4, 3) + "\t"
        + AnsiString::FloatToStrF(statSbp.RMSSD, AnsiString::sffFixed, 4, 3) + "\t";

	LibCore::CMeasurement::DFTParams sbpParams = measurement.fourierTransform(sbpChan);
	FFTform->coherence = false;
	FFTform->pMeasurement = &measurement;
	FFTform->pDFTParams = &sbpParams;
	FFTform->autoCopyToClipboard = false;
	FFTform->initForm();
	measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
//	clipText << gridStats->Cells[1][9].c_str() << "\t" <<
//		gridStats->Cells[1][10].c_str() << "\t" << gridStats->Cells[1][11].c_str() << "\t" <<
//		gridStats->Cells[1][12].c_str() << "\t" << gridStats->Cells[1][13].c_str() << "\t";

    clipTextAnsi += gridStats->Cells[1][9] + "\t" 
        + gridStats->Cells[1][10] + "\t" + gridStats->Cells[1][11] + "\t"
		+ gridStats->Cells[1][12] + "\t" + gridStats->Cells[1][13] + "\t";

	// coherence
	LibCore::CMeasurement::DFTParams cohParams = measurement.spectrumCoherence(rriChan, sbpChan, numberOfWins, overlapping, false);
	if (cohParams.end < 0){
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
	} else {
		FFTform->coherence = true;
		FFTform->pMeasurement = &measurement;
		FFTform->pDFTParams = &cohParams;
		FFTform->autoCopyToClipboard = false;
		FFTform->initForm();
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
	}
//	clipText <<
//		gridStats->Cells[1][12].c_str() << "\t" << gridStats->Cells[1][14].c_str() << "\t" <<
//		gridStats->Cells[1][15].c_str() << "\t" << gridStats->Cells[1][16].c_str() << "\t" <<
//		gridStats->Cells[1][17].c_str() << "\t" << gridStats->Cells[1][18].c_str() << "\t" <<
//		gridStats->Cells[1][13].c_str() << "\t" << gridStats->Cells[1][19].c_str() << "\t" <<
//		gridStats->Cells[1][20].c_str() << "\t" << gridStats->Cells[1][21].c_str() << "\t" <<
//		gridStats->Cells[1][22].c_str() << "\t" << gridStats->Cells[1][23].c_str() << "\t";

    clipTextAnsi += 
		gridStats->Cells[1][12] + "\t" + gridStats->Cells[1][14] + "\t" +
		gridStats->Cells[1][15] + "\t" + gridStats->Cells[1][16] + "\t" +
		gridStats->Cells[1][17] + "\t" + gridStats->Cells[1][18] + "\t" +
		gridStats->Cells[1][13] + "\t" + gridStats->Cells[1][19] + "\t" +
		gridStats->Cells[1][20] + "\t" + gridStats->Cells[1][21] + "\t" +
		gridStats->Cells[1][22] + "\t" + gridStats->Cells[1][23] + "\t";

	// BRS
	BRSForm->autoCopyToClipboard = false;
	BRSForm->initDialog(rriChan, sbpChan, measurement, cursors);
	if (singleRespChan) {
		BRSForm->enableBRS(true, true, true, true);
		BRSForm->setChannelIndices(rriChan, sbpChan, respChan);
	} else {
		BRSForm->enableBRS(true, true, false, true);
	}
	LibCore::CMeasurement::BRSReturn& brsRetVal = BRSForm->calculate();
	errorsDetected |= brsRetVal.error;
//	clipText << setw(4) <<
//		brsRetVal.sBRSUp << "\t" << brsRetVal.sBRSUpCount << "\t" << brsRetVal.sBRSDown << "\t" <<
//		brsRetVal.sBRSDownCount << "\t" << brsRetVal.xBRS << "\t" << brsRetVal.xBRSCount << "\t" <<
//		brsRetVal.alphaLF << "\t" << brsRetVal.alphaHF << "\t" << brsRetVal.rBRS << "\t" <<
//		brsRetVal.rBRSDelay << "\t" << brsRetVal.rBRSCount;

    clipTextAnsi += AnsiString::FloatToStrF(brsRetVal.sBRSUp, AnsiString::sffFixed, 4, 3) + "\t"         
        + AnsiString::FloatToStrF(brsRetVal.sBRSUpCount, AnsiString::sffFixed, 4, 3) + "\t" 
        + AnsiString::FloatToStrF(brsRetVal.sBRSDown, AnsiString::sffFixed, 4, 3) + "\t" 
        + AnsiString::FloatToStrF(brsRetVal.sBRSDownCount, AnsiString::sffFixed, 4, 3) + "\t"  
        + AnsiString::FloatToStrF(brsRetVal.xBRS, AnsiString::sffFixed, 4, 3) + "\t" 
        + AnsiString::FloatToStrF(brsRetVal.xBRSCount, AnsiString::sffFixed, 4, 3) + "\t" 
        + AnsiString::FloatToStrF(brsRetVal.alphaLF, AnsiString::sffFixed, 4, 3) + "\t"        
        + AnsiString::FloatToStrF(brsRetVal.alphaHF, AnsiString::sffFixed, 4, 3) + "\t" 
        + AnsiString::FloatToStrF(brsRetVal.rBRS, AnsiString::sffFixed, 4, 3) + "\t" 
        + AnsiString::FloatToStrF(brsRetVal.rBRSDelay, AnsiString::sffFixed, 4, 3) + "\t"      
        + AnsiString::FloatToStrF(brsRetVal.rBRSCount, AnsiString::sffFixed, 4, 3);

    Clipboard()->SetTextBuf(const_cast<char*>(clipTextAnsi.c_str()));
	refreshWholeWindow();

	if (errorsDetected) {
		Application->MessageBox("Med ra�unanjem BRS vrednosti je pri�lo do napake -\r\npreveri BRS rezultate.", "Opozorilo", MB_OK);
		return;
	}
}


// searches for a channel with a name starting with one of specified possibilities
// returns true if exactly one matching channel is found and saves it's index in chan variable
// returns false if no matching channels (chan is set to -1) or more than one (chan is set to
// of one of the possible indices) are found.
bool TDocumentForm::findChannelByNameStart(const AnsiString* names, int numOfNames, int& chan, bool eventChan) {
	assert(names != 0);
	assert(numOfNames > 0);

	chan = -1;
	int 						i = 0;

	// search for channel matching the names
	while ((chan < 0) && (i < numOfNames)) {
		chan = measurement.findChannelByName(names[i], 0, true, eventChan);
		++i;
	}

	if (chan == -1) {
		// did not find a matching channel name
		return false;
	} else {
		// found a match, search for more channels matching the same name
		if (measurement.findChannelByName(names[i-1], chan+1, true, eventChan) >= 0) {
			return false;
		} else {
			// search for more channels matching other names
			for (; i < numOfNames; ++i) {
				int c2 = measurement.findChannelByName(names[i], 0, true, eventChan);
				if ((c2 >= 0) && (c2 != chan))
					return false;
			}
		}
	}

	return true;
}


void TDocumentForm::interactiveGenerateEventChannels(bool respirationEnabled) {
	int				 			ecgChan, bpChan, respChan = -1;

	// don't expect 3 measurement channels when only two (ECG and BP) are available
	if (measurement.getNumChannels() == 2)
		respirationEnabled = false;

	// try finding channels by name first
	AnsiString					ecgChanNames[] = {"ECG", "EKG", "ecg", "ekg"};
	bool found1Ecg = findChannelByNameStart(ecgChanNames, 4, ecgChan);

	AnsiString					bpChanNames[] = {"tlak", "krvni tlak", "blood pressure", "pressure"};
	bool found1Bp = findChannelByNameStart(bpChanNames, 4, bpChan);

	bool 						found1Resp = false;
	if (respirationEnabled) {
		AnsiString				respChanNames[] = {"dihanje", "respiracija", "respiration", "ventilation"};
		found1Resp = findChannelByNameStart(respChanNames, 4, respChan);
	}

	AnsiString					dialogCaption("Parametri skripte");

	// if one or more unique (exactly one ECG, exactly one BP, ...) channels were not found,
	// ask user for their input
	while (!found1Ecg || !found1Bp || (respirationEnabled && !found1Resp)) {
		std::ostringstream		oss;
		AnsiString 			   	str;

		oss << (ecgChan + 1) << " " << (bpChan + 1);
		if (respirationEnabled) {
			// ask for respiration channel since respiration is enabled (entering respiration channel
			// index remains optional - if invalid then simply don't do respiration processing)
			oss << " " << (respChan + 1);
			if (!InstantDialog::queryOKCancel(dialogCaption,
				"Vpi�i s presledki lo�ene �tevilke signalnih kanalov za ekg, krvni tlak in \r\n(opcijsko) dihanje",
				oss.str().c_str(), str))
				return;
		} else {
			// don't ask for respiration channel since respiration is disabled
			if (!InstantDialog::queryOKCancel(dialogCaption,
				"Vpi�i s presledki lo�eni �tevilki signalnih kanalov za ekg in krvni tlak",
				oss.str().c_str(), str))
				return;
		}

		std::istringstream 		iss(str.c_str());

		iss >> ecgChan;
		iss >> bpChan;
		if (respirationEnabled) {
			iss >> respChan;
			if (iss.fail())
				respChan = 0;
		}

		if (iss.fail() || (ecgChan < 1) || (bpChan < 1) || (bpChan == ecgChan) ||
			(ecgChan > measurement.getNumChannels()) ||
			(bpChan > measurement.getNumChannels()) ||
			// a bit softer conditions for respiration channel
			(respChan == ecgChan) || (bpChan == respChan) ||
			(respChan > measurement.getNumChannels())
			)
		{
			dialogCaption = "Napaka v parametrih skripte!";
		} else {
			--ecgChan;
			--bpChan;
			if (respChan >= 0)
				--respChan;
			break;
		}
	}

	// run pocessing
	generateEventChannels(ecgChan, bpChan, respChan);
}


AnsiString getFileExtension(const AnsiString& fileName) {
	// find dot
	int							dotPos = fileName.Pos(".");
	if (dotPos == 0) {
		return "";
	} else {
		// find last dot
		int newDotPos;
		while ((newDotPos = fileName.SubString(dotPos + 1, fileName.Length() - dotPos).Pos(".")) != 0)
			dotPos = newDotPos;
		// return extension (without the dot)
		return fileName.SubString(dotPos + 1, fileName.Length() - dotPos);
	}
}


void TDocumentForm::saveCoherenceGraph(AnsiString _fileName) {
	if (_fileName == "") {
		_fileName = fileName + "-coherence.wmf";
	}

	int  					rriChan, sbpChan;
	AnsiString				rriName[] = {"RRI"};
	AnsiString				sbpName[] = {"SBP"};
	findChannelByNameStart(rriName, 1, rriChan, true);
	findChannelByNameStart(sbpName, 1, sbpChan, true);

	if ((rriChan < 0) || (sbpChan < 0)) {
		if (getFileExtension(fileName) == "nekg")
			generateEventChannels(2, 0, -1);
		else if (getFileExtension(fileName) == "afd")
			generateEventChannels();
		else
			interactiveGenerateEventChannels();

		// search for RRI and SBP again
		findChannelByNameStart(rriName, 1, rriChan, true);
		findChannelByNameStart(sbpName, 1, sbpChan, true);

		// if still not found (error must have happened)
		if ((rriChan < 0) || (sbpChan < 0))
			return;
	}

	const int 				numberOfWins = 8;
	const double 		  	overlapping = 0.5;
	LibCore::CMeasurement::DFTParams cohParams = measurement.spectrumCoherence(rriChan, sbpChan, numberOfWins, overlapping, false);
	if (cohParams.end < 0){
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
	} else {
		FFTform->coherence = true;
		FFTform->pMeasurement = &measurement;
		FFTform->pDFTParams = &cohParams;
		FFTform->autoCopyToClipboard = false;
		FFTform->trackAveragingColumns->Position = 1; // window of size 3
		FFTform->createAndSaveGraph(_fileName);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
	}
}


void TDocumentForm::saveDFTGraph(int channel, AnsiString _fileName) {
	if (_fileName == "") {
		_fileName = fileName + "-dft-" + (channel+1) + ".wmf";
	}

	if ((channel < 0) || (channel >= measurement.getNumEventChannels())) {
		return;
	}

	LibCore::CMeasurement::DFTParams params = measurement.fourierTransform(channel);
	measurement.getEventChannel(measurement.getNumEventChannels()-1).refreshMinMaxValue();
	FFTform->coherence = false;
	FFTform->pMeasurement = &measurement;
	FFTform->pDFTParams = &params;
	FFTform->autoCopyToClipboard = false;
	FFTform->trackAveragingColumns->Position = 1; // window of size 3
	FFTform->createAndSaveGraph(_fileName);

	measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
}


void TDocumentForm::saveCoherenceData(AnsiString _fileName) {
	if (_fileName == "") {
		_fileName = fileName + "-coherence.txt";
	}

	int  					rriChan, sbpChan;
	AnsiString				rriName[] = {"RRI"};
	AnsiString				sbpName[] = {"SBP"};
	findChannelByNameStart(rriName, 1, rriChan, true);
	findChannelByNameStart(sbpName, 1, sbpChan, true);

	if ((rriChan < 0) || (sbpChan < 0)) {
		if (getFileExtension(fileName) == "nekg")
			generateEventChannels(2, 0, -1);
		else if (getFileExtension(fileName) == "afd")
			generateEventChannels();
		else
			interactiveGenerateEventChannels();

		// search for RRI and SBP again
		findChannelByNameStart(rriName, 1, rriChan, true);
		findChannelByNameStart(sbpName, 1, sbpChan, true);

		// if still not found (this is a definite error)
		if ((rriChan < 0) || (sbpChan < 0))
			return;
	}

	const int 				numberOfWins = 8;
	const double 		  	overlapping = 0.5;
	LibCore::CMeasurement::DFTParams cohParams = measurement.spectrumCoherence(rriChan, sbpChan, numberOfWins, overlapping, false);
	if (cohParams.end < 0){
		// something went wrong
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
	} else {
		FFTform->coherence = true;
		FFTform->pMeasurement = &measurement;
		FFTform->pDFTParams = &cohParams;
		FFTform->autoCopyToClipboard = false;
		FFTform->trackAveragingColumns->Position = 1; // window of size 3
		FFTform->initForm();
		FFTform->saveDFTResults(_fileName);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
	}
}


void TDocumentForm::exportChannel(int channel, AnsiString channelName) {
    if (channelName == "") {
        channelName = measurement.getEventChannel(channel).getChannelName();
    } 
        
	AnsiString _fileName = fileName + "." + channelName + ".txt";
    selectChannel(channel, true, false);
    
    exportText(_fileName, false);
}


void TDocumentForm::resampleAndExportChannel(float frequency, int channel, AnsiString channelName) {
    if (channelName == "") {
        channelName = measurement.getEventChannel(channel).getChannelName();
    } 
        
	AnsiString _fileName = fileName + "." + channelName + ".txt";
	measurement.resampleEventChannel(channel, frequency);
    selectChannel(measurement.getNumEventChannels()-1, true, false);    
    exportText(_fileName, false);
    measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
}


void TDocumentForm::functionPovprecniTlak() {
    using namespace LibCore;
    float interval, length = -1;      

    // got 2 cursors?
   	float mirovanjeStart = cursors.getLeftmostCursor();
	float mirovanjeEnd = cursors.getRightmostCursor();
    if ((mirovanjeEnd == -1) || (mirovanjeEnd == mirovanjeStart)) {
        Application->MessageBox("Napaka - ne najdem dveh kurzorjev!", "Opozorilo", MB_OK);
        return;
    }

    // got channels?
    int rriChannelNum, systChannelNum, diastChannelNum;
    const AnsiString cRRIChannelName[] = {"RRI"};
    const AnsiString cSystChannelName[] = {"SBP"};
    const AnsiString cDiastChannelName[] = {"DBP"};

   	bool channelError = !(findChannelByNameStart(cRRIChannelName, 1, rriChannelNum, true) &
       	findChannelByNameStart(cSystChannelName, 1, systChannelNum, true) &
   	    findChannelByNameStart(cDiastChannelName, 1, diastChannelNum, true));

    if ((rriChannelNum < 0) || (systChannelNum < 0) || (diastChannelNum < 0)) {
        Application->MessageBox("Napaka - ne najdem kanalov!\r\nPodani morajo biti kanali RRI, SBP in DBP.", "Opozorilo", MB_OK);
        return;
    } else {   
    	AnsiString str("");
        while (channelError) {
            // found multiple channels with similar name or error from previous iteration

		    str += AnsiString(rriChannelNum + 1) + " " + AnsiString(systChannelNum + 1) + " " + AnsiString(diastChannelNum + 1);
            // ask for manual specification of channels to use
	    	if (!InstantDialog::queryOKCancel("Opozorilo",
		    	"Vpi�i s presledki lo�ene �tevilke dogodkovnih kanalov za RRI, SBP in DBP:", str, str))
			    return;
                
            std::istringstream iss(str.c_str());
            iss >> rriChannelNum >> systChannelNum >> diastChannelNum;
            --rriChannelNum;
            --systChannelNum;
            --diastChannelNum;

            if (iss.fail() || (rriChannelNum < 0) || (rriChannelNum >= measurement.getNumEventChannels()) || 
                (systChannelNum < 0) || (systChannelNum >= measurement.getNumEventChannels()) || 
                (diastChannelNum < 0) || (diastChannelNum >= measurement.getNumEventChannels())) 
            {
                str = "Napaka pri vpisanih podatkih!\r\n";
            } else
                channelError = false;
        }
    }
    
    // ask for input of parameters
    {
        AnsiString str;
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
            "Povpre�enje bo izvedeno na delu meritve omejenem z najbolj levo in \r\n"
            "najbolj desno postavljenima kurzorjema.\r\n"
            "Vpi�i periodo povpre�enja in dol�ino intervala povpre�enja v sekundah,\r\n"
            "lo�ena s presledkom.",
            "", str))
        {
	    	return;
        }

        try {    
            int delIndex = 0;
            for (; (delIndex < str.Length()) && (!str.IsDelimiter(" ", delIndex)); ++delIndex) ;
            interval = str.SubString(1, delIndex-1).ToDouble();
            length = str.SubString(delIndex+1, str.Length()).ToDouble();
        } catch(...) {
            interval = -1;
        }

        if ((interval < 1) || (length < 1) || (interval > (mirovanjeEnd-mirovanjeStart)) || (length > (mirovanjeEnd-mirovanjeStart))) {
            AnsiString msg(AnsiString("Napaka - napa�en vnos podatkov:\n") + str);
            Application->MessageBox(msg.c_str(), "Opozorilo", MB_OK);
            return;
        }
    }

	CEventChannel &systChannel = measurement.getEventChannel(systChannelNum);
	CEventChannel &diastChannel = measurement.getEventChannel(diastChannelNum);
	CEventChannel &rrChannel = measurement.getEventChannel(rriChannelNum);

	// calculations of RR values for each minute & graph 
    int maxTime = (mirovanjeEnd-mirovanjeStart-((length+1)/2)) / interval;
    std::vector<float> rrVals;
    std::vector<float> sbpVals;
    std::vector<float> dbpVals;
    std::vector<float> timeVals;
    for (int time = 1; time <= maxTime; ++time) {
        if (time*interval >= length/2) {
            timeVals.push_back(mirovanjeStart + time*interval);
	    	rrVals.push_back(rrChannel.calcStatistics(mirovanjeStart + time*interval - length/2,
		    	mirovanjeStart + time*interval + (length+1)/2).avgValue*1000);
            sbpVals.push_back(systChannel.calcStatistics(mirovanjeStart + time*interval - length/2,
	    		mirovanjeStart + time*interval + (length+1)/2).avgValue);
		    dbpVals.push_back(diastChannel.calcStatistics(mirovanjeStart + time*interval - length/2,
			    mirovanjeStart + time*interval + (length+1)/2).avgValue);
        }
    }
    
    // write results
    AnsiString outString("");
    AnsiString displayString;
    
    displayString = "Podatki za �as od " + AnsiString::FloatToStrF(mirovanjeStart, AnsiString::sffFixed, 10, 1) 
        + " do " + AnsiString::FloatToStrF(mirovanjeEnd, AnsiString::sffFixed, 10, 1) + " se nahajajo tudi v odlo�i��u.\r\n\r\n"; 
    outString = "cas[s]\trri[ms]\tsbp\tmbp\tdbp\n";

    for (size_t i = 0; i < rrVals.size(); ++i)
        outString = outString + AnsiString::FloatToStrF(timeVals[i], AnsiString::sffFixed, 10, 1) + "\t" 
            + AnsiString::FloatToStrF(rrVals[i], AnsiString::sffFixed, 10, 1) + "\t"
            + AnsiString::FloatToStrF(sbpVals[i], AnsiString::sffFixed, 10, 1) + "\t"
            + AnsiString::FloatToStrF((sbpVals[i] + dbpVals[i]*2) / 3.0, AnsiString::sffFixed, 10, 1) + "\t"
            + AnsiString::FloatToStrF(dbpVals[i], AnsiString::sffFixed, 10, 1) + "\n";
    Clipboard()->SetTextBuf(const_cast<char*>(outString.c_str()));
    displayString = displayString + outString;
    Application->MessageBox(displayString.c_str(), "Rezultat", MB_OK);
}


void TDocumentForm::calculateMDP() {
    int dbpChanNum = -1;
    int sbpChanNum = numSelectedChannel;

    if (eventChannelSelected) {
        AnsiString str;
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
            "Vpi�i �tevilko kanala, ki vsebje dogodkovni DBP kanal.\r\nPred tem naj bo izbran dogodkovni kanal SBP.", AnsiString(numSelectedChannel+2), str))
        {
	    	return;
        }

        std::istringstream iss(str.c_str());
        iss >> dbpChanNum;
        --dbpChanNum;
        
        if (dbpChanNum < 0) {
            AnsiString msg(AnsiString("Kanal " + str + " ne obstaja.\n"));
            Application->MessageBox(msg.c_str(), "Opozorilo", MB_OK);
            return;
        }
    } else {
        Application->MessageBox("Izbran mora biti dogodkovni kanal.", "Opozorilo", MB_OK);
        return;
    }

    selectChannel(measurement.getNumEventChannels()-1, true, false);
    measurement.createMBPEventChannel(sbpChanNum, dbpChanNum);
    refreshWholeWindow();
	dirty = true;
}


// export v comvis v naprej dolocenem formatu: ekg, dihanje, rri, eei
void TDocumentForm::comVisExport() {
    AnsiString fname = fileName + ".comvis";

    // find relevant channels
	int  					ekgChan, dihanjeChan, rriChan, eeiChan;
    
	AnsiString				ekgName[] = {"EKG"};
	AnsiString				dihanjeName[] = {"dihanje"};
    AnsiString				rriName[] = {"RRI iz M"};
    
	findChannelByNameStart(ekgName, 1, ekgChan, false);
	findChannelByNameStart(dihanjeName, 1, dihanjeChan, false);
    findChannelByNameStart(rriName, 1, rriChan, true);

    int                     temp;                                   
    AnsiString				odvodName[] = {"Odvod", "odvod"};
    findChannelByNameStart(odvodName, 2, temp, true);
    if (temp > 0) {
        AnsiString			eeiName[] = {AnsiString("RRI iz D") + AnsiString(temp + 1)};
        findChannelByNameStart(eeiName, 1, eeiChan, true);
    } else {
        eeiChan = temp;
    }

	if (ekgChan < 0) {
        AnsiString answer;
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
            "Ne najdem EKG kanala.\r\nProsim vpi�i �tevilko EKG merilnega kanala:", 
            "", answer))
	    	return;
        ekgChan = answer.ToIntDef(0) - 1;
	}

    if (dihanjeChan < 0) {
        AnsiString answer;
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
            "Ne najdem kanala z dihanjem.\r\nProsim vpi�i �tevilko merilnega kanala z dihanjem:", 
            "", answer))
	    	return;
        dihanjeChan = answer.ToIntDef(0) - 1;
	}

    if (rriChan < 0) {
        AnsiString answer;
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
            "Ne najdem RRI kanala.\r\nProsim vpi�i �tevilko RRI dogodkovnega kanala:", 
            "", answer))
	    	return;
        rriChan = answer.ToIntDef(0) - 1;
	}

    if (eeiChan < 0) {
        AnsiString answer;
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
            "Ne najdem EEI kanala.\r\nProsim vpi�i �tevilko EEI dogodkovnega kanala:", 
            "", answer))
	    	return;
        eeiChan = answer.ToIntDef(0) - 1;
	}

    // if channels found start the export
    if ((ekgChan >= 0) && (dihanjeChan >= 0) && (rriChan >= 0) && (eeiChan >= 0) &&
        (ekgChan < measurement.getNumChannels()) && 
        (dihanjeChan < measurement.getNumChannels()) && 
        (rriChan < measurement.getNumEventChannels()) && 
        (eeiChan < measurement.getNumEventChannels()))
    {
        using namespace LibCore;
        
        // ask for the starting number for eyported time series (usefull when combining multiple
        // files after the export - no need to fix time series' indices then)
        AnsiString answer;
        AnsiString msg(AnsiString("Izbrani kanali: EKG=M") + AnsiString(ekgChan+1) + AnsiString(", dihanje=M") +
            AnsiString(dihanjeChan+1) + AnsiString(", RRI=D") + AnsiString(rriChan+1) + 
            AnsiString(", EEI=D") + AnsiString(eeiChan+1) + AnsiString("\n") +
            AnsiString("Vpi�i za�etni indeks za izvo�ene �asovne vrste:"));
        if (!InstantDialog::queryOKCancel("Vpis parametrov", msg.c_str(), "1", answer))
	    	return;
        int timeSeriesIndex = answer.ToIntDef(1);
        
        std::ofstream file(fname.c_str());

        double referenceTime = (cursors.getNumSetCursors() > 0 ? 
            cursors.getLeftmostCursor() : 
            (measurement.getEventChannel(rriChan).getNumElems() > 300) ? 
                referenceTime = measurement.getEventChannel(rriChan)[99].time : 0.0);


        // ECG time series
        {
            int filteredEcg = measurement.getNumChannels();
            measurement.lowPassFilter(LibCore::CMeasurement::windowTriangular, ekgChan, 50);
            int resampledEcg = measurement.getNumEventChannels();
            measurement.resampleSignalChannel(filteredEcg, 150);
            CEventChannel &channel = measurement.getEventChannel(resampledEcg);
        
            size_t startIndex = channel.getIndexClosestToTime(referenceTime);
            size_t endIndex = startIndex + 3*150;
            if (endIndex > (size_t)channel.getNumElems())
                endIndex = (size_t)channel.getNumElems();

            file << timeSeriesIndex << "\n" << (endIndex - startIndex) << "\n";
            ++timeSeriesIndex;
            for (size_t i = startIndex; i < endIndex; ++i) {
                file << (channel[i].time - channel[startIndex].time) << ";\t" << channel[i].value << "\n";
            }
        
            measurement.deleteChannel(filteredEcg);
            measurement.deleteEventChannel(resampledEcg);
        }

        // dihanje time series
        {
            int filtered = measurement.getNumChannels();
            measurement.lowPassFilter(LibCore::CMeasurement::windowTriangular, dihanjeChan, 10);
            int resampled = measurement.getNumEventChannels();
            measurement.resampleSignalChannel(filtered, 10);
            CEventChannel &channel = measurement.getEventChannel(resampled);
        
            size_t startIndex = channel.getIndexClosestToTime(referenceTime);
            size_t endIndex = startIndex + 20*10;
            if (endIndex > (size_t)channel.getNumElems())
                endIndex = (size_t)channel.getNumElems();

            file << timeSeriesIndex << "\n" << (endIndex - startIndex) << "\n";
            ++timeSeriesIndex;
            for (size_t i = startIndex; i < endIndex; ++i) {
                file << (channel[i].time - channel[startIndex].time) << ";\t" << channel[i].value << "\n";
            }
        
            measurement.deleteChannel(filtered);
            measurement.deleteEventChannel(resampled);
        }  

        // RRI time series
        {
            CEventChannel &channel = measurement.getEventChannel(rriChan);
            size_t startIndex = channel.getIndexClosestToTime(referenceTime);
            size_t endIndex = channel.getIndexClosestToTime(referenceTime + 100);
        
            file << timeSeriesIndex << "\n" << (endIndex - startIndex) << "\n";
            ++timeSeriesIndex;
            for (size_t i = startIndex; i < endIndex; ++i) {
                file << (channel[i].time - channel[startIndex].time) << ";\t" << channel[i].value << "\n";
            }
        }  

        // EEI time series
        {
            CEventChannel &channel = measurement.getEventChannel(eeiChan);
            size_t startIndex = channel.getIndexClosestToTime(referenceTime);
            size_t endIndex = channel.getIndexClosestToTime(referenceTime + 100);
        
            file << timeSeriesIndex << "\n" << (endIndex - startIndex) << "\n";
            ++timeSeriesIndex;
            for (size_t i = startIndex; i < endIndex; ++i) {
                file << (channel[i].time - channel[startIndex].time) << ";\t" << channel[i].value << "\n";
            }
        }

        // RR-EE time series
        {                           
            int chartMs = 4000;
            
            AveragerForm->initDialog(rriChan, true, measurement, cursors, eeiChan);
            AveragerForm->setChartRange(chartMs);
            AveragerForm->redrawGraph();
            
            int i = 0;
            std::vector<double> times;
            std::vector<double> values;
            for (std::vector<double>::const_iterator it = AveragerForm->getAverage().begin(), it1 = it+1;
                it1 != AveragerForm->getAverage().end(); 
                ++it, ++it1) 
            {
                double time = (-chartMs + i * chartMs * 2.0 / (AveragerForm->getAverage().size()-1));
                double time1 = time + chartMs * 2.0 / (AveragerForm->getAverage().size()-1);

                if ((floor((chartMs + time) * 0.008) < floor((chartMs + time1) * 0.008)) ||
                    ((time == -chartMs) && (floor(0.008 * time)*125 == time)))
                {
                    // if step over 0.1s accours, calculate and output value at 0.1s barrier
                    double timeMid = floor(0.008 * time1) * 125;
                    double value = (*it * (time1 - timeMid) + *it1 * (timeMid - time)) / (time1 - time);

                    times.push_back(timeMid);
                    values.push_back(value);
                }
                ++i;
            }

            file << timeSeriesIndex << "\n" << values.size() << "\n";
            for (size_t i = 0; i < values.size(); ++i) {
                file << (times[i]*0.001) << ";\t" << (1000.0 * values[i]) << "\n";
            }
        }
    } else {
        Application->MessageBox("Napa�na izbira kanalov.", "Napaka", MB_OK);
    }    
}


void TDocumentForm::TWaveAnalysis() {
    using namespace LibCore;

    // find relevant channels
    if (eventChannelSelected) {
        Application->MessageBox("Izbran mora biti EKG merilni kanal.", "Napaka", MB_OK);
        return;
    }
	int  					ekgChan, ekgBaselineChan, filteredEkgChan, rriChan;
	AnsiString				rriName[] = {"RRI iz M"};
	findChannelByNameStart(rriName, 1, rriChan, true); 

    if ((cursors.c[0] < 0) || ((cursors.c[1] < 0) && (cursors.c[2] < 0))) {
        Application->MessageBox("Ne najdem enega ali ve�ih kurzorjev.", "Napaka", MB_OK);
        return;
    }

    // ask for parameters    
    double skipTimeAfterR = 0.1;
    double allowMaxTimeAfterR = 0.45;
    double fittingParamOffset = 0.02;
    int fittingParamMeanCntHalf = 3;
    double derivativeParam = 0.01;

    AnsiString settingsFname("nastavitve.ini");
    for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
        if (Application->ExeName[i] == '\\') {
            settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
            break;
        }
    }
    Ini::File ini(settingsFname.c_str());
    std::string defaultParams("");
    static const char privzetiKanaliCharStr[] = "T val parametri";
    ini.loadVar(defaultParams, privzetiKanaliCharStr);
        
    int diffDist = measurement.time2SampleIndex(derivativeParam);
    if (measurement.sampleIndex2Time(diffDist) > derivativeParam)
        --diffDist;
    if (diffDist < 1)
        diffDist = 1;

    {
        AnsiString answer;
        AnsiString firstWord;
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
                "Vpi�i parametre v naslednjem zaporedju (�asi so v [s])\r\n"
                "  min �as med R in T vrhovoma, max �as med R in T vrhovoma, \r\n"
                "  odmik pri dolo�anju to�k za kvadratno interpolacijo, \r\n"
                "  �t povpre�enih to�k pri kvadratni interpolaciji, dol�ina desne strmine T vala", 
            defaultParams.c_str(), answer))
            return;
            
        defaultParams = answer.c_str();
        while (answer.Pos(' ') == 1)
            answer = answer.SubString(2, answer.Length() - 1);
        firstWord = answer.SubString(1, answer.Pos(' '));
        answer = answer.SubString(answer.Pos(' ')+1,  answer.Length() - firstWord.Length());
        skipTimeAfterR = firstWord.ToDouble();

        while (answer.Pos(' ') == 1)
            answer = answer.SubString(2, answer.Length() - 1);
        firstWord = answer.SubString(1, answer.Pos(' '));
        answer = answer.SubString(answer.Pos(' ')+1,  answer.Length() - firstWord.Length());
        allowMaxTimeAfterR = firstWord.ToDouble();

        while (answer.Pos(' ') == 1)
            answer = answer.SubString(2, answer.Length() - 1);
        firstWord = answer.SubString(1, answer.Pos(' '));
        answer = answer.SubString(answer.Pos(' ')+1,  answer.Length() - firstWord.Length());
        fittingParamOffset = firstWord.ToDouble();
                                                      
        while (answer.Pos(' ') == 1)
            answer = answer.SubString(2, answer.Length() - 1);
        firstWord = answer.SubString(1, answer.Pos(' ')-1);
        answer = answer.SubString(answer.Pos(' ')+1,  answer.Length() - firstWord.Length());
        fittingParamMeanCntHalf = firstWord.ToInt() / 2;

        while (answer.Pos(' ') == 1)
            answer = answer.SubString(2, answer.Length() - 1);
        derivativeParam = (answer.ToDouble() - 1);

        if (!ini.storeVar(defaultParams, privzetiKanaliCharStr) || !ini.save())
            Application->MessageBox("Napaka pri shranjevanju nastavitev. Izbrane nastavitve ne bodo shranjene!", "Opozorilo", MB_OK);
    }
    
    if (rriChan < 0) {
        Application->MessageBox("Ne najdem RRI kanala.", "Napaka", MB_OK);
    } else {
        ekgChan = numSelectedChannel;
        
        // temporary channel
        ekgBaselineChan = measurement.getNumChannels();
        measurement.correctBaseline(ekgChan, rriChan, &cursors);
        
        if (measurement[ekgChan].getChannelName().Pos("LPF") == 1) {
            filteredEkgChan = ekgBaselineChan;
        } else {  
            // temporary channel
            filteredEkgChan = measurement.getNumChannels();
            measurement.lowPassFilter(LibCore::CMeasurement::windowTriangular, ekgBaselineChan, 50);
        }
    
        CChannel &ekgChannel = measurement[filteredEkgChan];
        CEventChannel &rriChannel = measurement.getEventChannel(rriChan);

        double baseLineValue = ekgChannel[measurement.time2SampleIndex(cursors.c[0])];
        double baseLineTime = rriChannel[rriChannel.getIndexClosestToTime(cursors.c[0])].time -
            cursors.c[0];     
        double beatStartTime = (cursors.c[1] > 0) ? rriChannel[rriChannel.getIndexClosestToTime(cursors.c[1])].time - cursors.c[1] :
            rriChannel[rriChannel.getIndexClosestToTime(cursors.c[2])].time - cursors.c[2];

//        Application->MessageBox("approx", "dbg", MB_OK);
        // find approximate T peak positions
        std::vector<size_t> approxTPeak(rriChannel.getNumElems() - 1);
        for (int i = 1; i < rriChannel.getNumElems(); ++i) {
            double startTime = rriChannel[i-1].time + skipTimeAfterR;
            double endTime = std::min(rriChannel[i-1].time + allowMaxTimeAfterR, 
                rriChannel[i].time - beatStartTime);
            size_t startIndex = measurement.time2SampleIndex(startTime);
            size_t endIndex = measurement.time2SampleIndex(endTime);
                                   
            size_t maxI = endIndex;
            double maxVal = ekgChannel[endIndex];
            for (size_t s = startIndex; s < endIndex; ++s) {
                if (ekgChannel[s] > maxVal) {
                    maxVal = ekgChannel[s];
                    maxI = s;
                }
            }
            approxTPeak[i-1] = maxI;
        }
     
//        Application->MessageBox("fit", "dbg", MB_OK);
        // perform polynomial fitting to improve T peak position
        std::vector<double> TPeak(approxTPeak.size());
        std::vector<double> TPeakValue(approxTPeak.size());
        int offs = measurement.time2SampleIndex(fittingParamOffset);
        if (offs < fittingParamMeanCntHalf)
            offs = fittingParamMeanCntHalf;
        double mean1 = 0, mean2 = 0, mean3 = 0;
        for (size_t i = 0; i < approxTPeak.size(); ++i) {
            int j1 = approxTPeak[i] - offs;
            for (int j = j1 - fittingParamMeanCntHalf; j < j1 + fittingParamMeanCntHalf; ++j)
                mean1 += ekgChannel[j];
            mean1 /= (2 * fittingParamMeanCntHalf + 1);

            int j2 = approxTPeak[i];
            for (int j = j2 - fittingParamMeanCntHalf; j < j2 + fittingParamMeanCntHalf; ++j)
                mean2 += ekgChannel[j];
            mean2 /= (2 * fittingParamMeanCntHalf + 1);
        
            int j3 = approxTPeak[i] + offs;
            for (int j = j3 - fittingParamMeanCntHalf; j < j3 + fittingParamMeanCntHalf; ++j)
                mean3 += ekgChannel[j];
            mean3 /= (2 * fittingParamMeanCntHalf + 1);

            // quadratic interpolation
            double b = (mean2 - mean3 - (mean1 - mean2)*(j2*(double)j2 - j3*(double)j3) / 
                (j1*(double)j1 - j2*(double)j2)) / (j2 - j3 - (j1 - j2)*(j2*(double)j2 - j3*(double)j3) / 
                (double)(j1*(double)j1 - j2*(double)j2));
            double a = ((mean1 - mean2) - b*(j1 - j2)) / (double)(j1*(double)j1 - j2*(double)j2);
            if (a == 0)
                TPeak[i] = j2;
            else
                TPeak[i] = b / (-2.0*a);

            TPeakValue[i] = (a*TPeak[i] + b) * TPeak[i] + mean2 - (a*j2 + b) * j2;
        }
                                    
//        Application->MessageBox("derive", "dbg", MB_OK);
        // find smallest (max negative) derivative of ecg, following T peak
        std::vector<int> derivatives(approxTPeak.size());
        std::vector<double> TEnd(approxTPeak.size());
        for (size_t i = 0; i < approxTPeak.size(); ++i) {
            double startTime = measurement.sampleIndex2Time((int)approxTPeak[i]);
            double endTime = std::min(rriChannel[i].time + allowMaxTimeAfterR, rriChannel[i+1].time - beatStartTime);
            int startIndex = measurement.time2SampleIndex(startTime);
            int endIndex = measurement.time2SampleIndex(endTime);
            double normalizationParam = measurement.sampleIndex2Time(2* diffDist + 1);

            assert(startIndex >= 0);
            assert(endIndex < ekgChannel.getNumElems());
            if ((startIndex + diffDist) > (endIndex - diffDist)) {
                TEnd[i] = 0;
                derivatives[i] = 0;
                continue;
            }
            
            int maxJ = endIndex - diffDist;
            double maxDif = ekgChannel[maxJ-diffDist] - ekgChannel[maxJ+diffDist];
            for (int j = startIndex + diffDist; j < (endIndex - diffDist); ++j) {
                assert((j+diffDist) < ekgChannel.getNumElems());
                assert((j-diffDist) >= 0);
                double temp = ekgChannel[j-diffDist] - ekgChannel[j+diffDist];
                if (temp > maxDif) {
                    maxDif = temp;
                    maxJ = j;
                }
            }
                  
            if (maxDif > 0) {
                TEnd[i] = measurement.sampleIndex2Time(maxJ) + (ekgChannel[maxJ] - baseLineValue) / maxDif * normalizationParam;

                if (TEnd[i] < endTime)
                    derivatives[i] = maxJ;
                else {
                    derivatives[i] = 0;
                }
            } else {
                derivatives[i] = 0;
            }
        }
                                       
//        Application->MessageBox("sym", "dbg", MB_OK);
        // find T wave symmetricity
        std::vector<double> TSym(approxTPeak.size());
        for (size_t i = 0; i < TSym.size(); ++i) {
            TSym[i] = -1.0;

            if (derivatives[i] == 0)
                continue;
            
            for (int j = approxTPeak[i]; 
                j > measurement.time2SampleIndex(rriChannel[i].time + skipTimeAfterR); --j) 
            {
                if (ekgChannel[j] < ekgChannel[derivatives[i]]) {
                    TSym[i] = ( (ekgChannel[derivatives[i]] - ekgChannel[j]) * 
                        measurement.sampleIndex2Time(j+1) +
                        (ekgChannel[j+1] - ekgChannel[derivatives[i]]) * 
                        measurement.sampleIndex2Time(j) ) /
                        (ekgChannel[j+1] - ekgChannel[j]);
                    break;
                }
            }

            if (TSym[i] < 0.0)
                derivatives[i] = 0;
        }
    
//        Application->MessageBox("chans", "dbg", MB_OK);
        // create channels for display
        CEventChannel &peakChannel = measurement.getEventChannel(
            measurement.addEventChannel("T val vrh", "s"));
        peakChannel.setContiguous(false);
        
        CEventChannel &derMinChannel = measurement.getEventChannel(
            measurement.addEventChannel("T val min odvod", "s"));
        derMinChannel.setContiguous(false);
        
        CEventChannel &derSymChannel = measurement.getEventChannel(
            measurement.addEventChannel("T val simetri�nost", "s"));
        derSymChannel.setContiguous(false);
            
        CEventChannel &lenTChannel = measurement.getEventChannel(
            measurement.addEventChannel("T val dolzina", "s"));
        lenTChannel.setContiguous(false);
        
        int j = 0;
        for (size_t i = 0; i < TPeak.size(); ++i) {
            if ((derivatives[i] == 0) || (derivatives[i] >= ekgChannel.getNumElems()))
                continue;         
                
            peakChannel[j].time = measurement.sampleIndex2Time(TPeak[i]);
            peakChannel[j].value = peakChannel[j].time - rriChannel[i].time + beatStartTime;
    
            derMinChannel[j].time = measurement.sampleIndex2Time(derivatives[i]);
            derMinChannel[j].value = derMinChannel[j].time - rriChannel[i].time + beatStartTime;

            derSymChannel[j].time = TSym[i];
            derSymChannel[j].value = TSym[i] - rriChannel[i].time + beatStartTime;
            
            lenTChannel[j].time = TEnd[i];
            lenTChannel[j].value = lenTChannel[j].time - rriChannel[i].time + beatStartTime;

            ++j;
        }  

//        Application->MessageBox("file", "dbg", MB_OK);
        // save channels to file
        AnsiString fname = fileName + ".Tval";
        std::ofstream file(fname.c_str());
        file << "# each line contains all the times, followed by all the values for the points of interest:\n";
        file << "# baseLine Time, beat start, R peak, position on left-hand side of T, where "
            "amplitude is the same as on the point of max negative derivative, T peak, position on "
            "T with max negative derivative, T end\n";
        for (size_t i = 0; i < TPeak.size(); ++i) {
            if (derivatives[i] == 0)
                continue;
            
            double beatStartVal = ekgChannel[measurement.time2SampleIndex(rriChannel[i].time - beatStartTime)];
            
            file << (rriChannel[i].time - baseLineTime) << " \t" << 
                // times
                (rriChannel[i].time - beatStartTime) << " \t" << 
                rriChannel[i].time << " \t" <<                     
                TSym[i] << " \t" << 
                measurement.sampleIndex2Time(TPeak[i]) << " \t" << 
                measurement.sampleIndex2Time(derivatives[i]) << " \t" << 
                TEnd[i] << " \t" <<
                // values
                "0" << " \t" << 
                (beatStartVal - baseLineValue) << " \t" << 
                (ekgChannel[measurement.time2SampleIndex(rriChannel[i].time)] - baseLineValue) << " \t" <<                     
                (ekgChannel[derivatives[i]] - baseLineValue) << " \t" << 
                (TPeakValue[i] - baseLineValue) << " \t" << 
                (ekgChannel[derivatives[i]] - baseLineValue) << " \t" << 
                (measurement.signalValue(filteredEkgChan, TEnd[i]) - baseLineValue) << "\n";
        }
     
        if (filteredEkgChan != ekgBaselineChan) 
            measurement.deleteChannel(filteredEkgChan);
        measurement.deleteChannel(ekgBaselineChan);
        refreshWholeWindow();
    	dirty = true;
    }
}


void TDocumentForm::addRandomChannel() {
    using namespace LibCore;
    
    double frequency;
    double normalDeviation;
    {
        AnsiString answer;
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
                "povpre�na frekvenca kanala [Hz]", "1", answer))
            return;
        frequency = answer.ToDouble();
    }
    {
        AnsiString answer;
        AnsiString pointOne(0.1); // use conversion to proper locale
        if (!InstantDialog::queryOKCancel("Vpis parametrov",
                "random deviacija frekvence [Hz]", pointOne.c_str(), answer))
            return;
        normalDeviation = answer.ToDouble();
    }

    double prevTime = 0.0;
    CEventChannel &channel = measurement.getEventChannel(measurement.addEventChannel("random", "s"));
    for (int i = 0;; ++i) {
        double interval = 1.0 / (frequency + normalDeviation * (1.0 - (2.0 * rand() / double(RAND_MAX))));
        if (prevTime + interval < measurement.sampleIndex2Time(measurement[0].getNumElems()-1)) {
            channel[i].value = interval;          
            prevTime += interval;
            channel[i].time = prevTime;
        } else
            break;
    }
    
    refreshWholeWindow();
   	dirty = true;
}


bool TDocumentForm::runNamedScript(const AnsiString& name, const AnsiString& params) {
// Script parameters that are floating point numbers are in format 123.456 (decimal dot!)
	bool						autoExit = false;
	if (params.Pos("auto_exit") > 0) {
		autoExit = true;
	}

	if (name == "save_coherence_graph") {
		// script without parameters
		saveCoherenceGraph();
	} else if (name == "save_DFT_graph") {
		// script requires one parameter - 1-based index(es) of event channel(s) to apply DFT to
		std::stringstream		ss(params.c_str());
		int						chan;
		do {
			ss >> chan;
			if (!ss.fail()) {
				// run script on channel (convert channel index from 1-based to 0-based)
				saveDFTGraph(chan-1);
			} else {
				std::string		name;
				ss.clear();
				ss >> name;
				AnsiString		ansiName(name.c_str());
				// check if channel name is provided instead of channel number
				if (findChannelByNameStart(&ansiName, 1, chan, true))
					saveDFTGraph(chan);
			}
		} while (!ss.fail());
	} else if (name == "save_coherence_data") {
		saveCoherenceData();
	} else if (name == "export_channel") {
        std::stringstream		ss(params.c_str());
        while (!ss.fail()) {
            int	chanNum;
            ss >> chanNum;
			if (!ss.fail()) {
				// run script on channel (convert channel index from 1-based to 0-based)
				exportChannel(chanNum - 1);
			} else {
				std::string chanName;
				ss.clear();
				ss >> chanName;
				AnsiString		ansiName(chanName.c_str());

				// check if channel name is provided instead of channel number
				if (findChannelByNameStart(&ansiName, 1, chanNum, true))
					exportChannel(chanNum, ansiName);
			}
        }
        autoExit = true;
    } else if (name == "export_resampled_channel") {
        std::stringstream		ss(params.c_str());
        float frequency;
        ss >> frequency;
        while (!ss.fail()) {
            int	chanNum;
            ss >> chanNum;
			if (!ss.fail()) {
				// run script on channel (convert channel index from 1-based to 0-based)
				resampleAndExportChannel(frequency, chanNum - 1);
			} else {
				std::string chanName;
				ss.clear();
				ss >> chanName;
				AnsiString		ansiName(chanName.c_str());

				// check if channel name is provided instead of channel number
				if (findChannelByNameStart(&ansiName, 1, chanNum, true))
					resampleAndExportChannel(frequency, chanNum, ansiName);
			}
        }
        autoExit = true;
    } else if (name == "mobECG_demo") {
        std::stringstream		ss(params.c_str());
        if (!ss.fail()) {
        }
        autoExit = false;
    }

	return (autoExit);
}

void TDocumentForm::mobEcgGetBeatEvents(bool automatic) {

    if(eventChannelSelected || annotationChannelSelected){
        Application->MessageBox("Prosimo izberite signalni kanal!", "Statistika utripov." , MB_OK);
        return;
    }

    double AMvalue;
    double AMmaxValue = 160; //AMmaxValue increased from 120 because regular beats with values ~125 found
    double extremeEventsMin = 0.24; //250Hz MAX,
    double extremeEventsMax = 2.5;  //24Hz Min
    double filterFreq = 40;

    double startMinAmValue = 6;
    double minAmValue = startMinAmValue;
    double maxAmValue = 42;
    double amValueIncrement = 6;

    HeartBeatParam->amMaxValue->Text = AMmaxValue;

    Screen->Cursor = crHourGlass;

//get minimum number of events found
    double minDetectedHeartBeat = measurement.getMinimumHeartBeatEvents(numSelectedChannel);

//low pass
    try{
        measurement.lowPassFilterBeatDetection(
            LibCore::CMeasurement::windowTriangular,
     		numSelectedChannel,
            filterFreq,
            measurement.getMissingSamplesValue(eventChannelSelected, numSelectedChannel)
            );
        }
        catch (LibCore::EKGException *e) {
			Application->MessageBox(e->Message.c_str(), "Napaka pri filtriranju:", MB_OK);
			Screen->Cursor = crDefault;
			return;
		}
    selectChannel(measurement.getNumChannels()-1, false, false);
    dirty = true;


//    measurement[numSelectedChannel].getNumElems();
//    signalChan.
//    double *temporarySamples = (double*)malloc(measurement[numSelectedChannel].getNumElems() * sizeof(double));
  
//differential of filtered signal
    differentialChannel(getMissingSamplesValue(),true); // differential ignoring err values, absolute value
    selectChannel(measurement.getNumChannels()-1, false, false); //select filtered channel
    deleteChannel(); //delete filtered channel
    selectChannel(measurement.getNumEventChannels()-1, true, false); // select differential channel

    Screen->Cursor = crDefault;
    if(measurement.getEventChannel(numSelectedChannel).getMaxValue() > 400){
        if(Application->MessageBox("Vrednosti odvoda so prevelike. Analiza tega posnetka lahko traja dolgo �asa. Nadaljujem?", "Dolgo izvajanje programa" , MB_OKCANCEL )
            != IDOK)
            return; //uporabnik ne �eli nadaljevati
    }
    Screen->Cursor = crHourGlass;

    AMvalue = minAmValue;
    double minBpmStd = 100000; // std is abnormally high...
    LibCore::CEventChannel::Statistics stat;
    HeartBeatParam->Series1->Clear();
    HeartBeatParam->Series2->Clear();

    while(AMvalue <= maxAmValue){
        calcBeatTimesBeatDetection(LibCore::CMeasurement::beatAlgorithmAM, AMvalue);
        if(measurement.getEventChannel(measurement.getNumEventChannels()-1).getNumElems() < min(5.0,minDetectedHeartBeat)){
            //maxAmValue = AMvalue -1;
            selectChannel(measurement.getNumEventChannels()-1, true, false);
            deleteChannel();
            break;
        }

        stat = measurement.getEventChannel(measurement.getNumEventChannels()-1).calcStatisticsBeatDetection(extremeEventsMin, extremeEventsMax);

        HeartBeatParam->Series1->AddXY(AMvalue, stat.stDevValue*100, FormatFloat("0.#",(stat.avgValue!=0)?(60/stat.avgValue):0), TColor(clRed));
        HeartBeatParam->Series2->AddXY(AMvalue, stat.numOfEvents, "", TColor(clBlue));

        if(stat.numOfEvents < minDetectedHeartBeat){
 //           maxAmValue = AMvalue -1;
            selectChannel(measurement.getNumEventChannels()-1, true, false);
            deleteChannel();
            break;
        }

        if(stat.stDevValue < minBpmStd){
            minAmValue = AMvalue;
            minBpmStd = stat.stDevValue;
        }

        selectChannel(measurement.getNumEventChannels()-1, true, false);
        deleteChannel();
        AMvalue += amValueIncrement;
    }
    HeartBeatParam->amValueInput->Text = AnsiString(minAmValue);
    //HeartBeatParam->Label4->Caption = "Minimalna vrednost: "+FormatFloat("#.#",startMinAmValue)+",  maksimalna vrednost "+FormatFloat("#.#",maxAmValue)+".";


    HeartBeatParam->Chart1->LeftAxis->Maximum = HeartBeatParam->Series1->MaxYValue()+10;
    HeartBeatParam->Chart1->LeftAxis->Minimum = 0;
    HeartBeatParam->Chart1->BottomAxis->Maximum = AMvalue - 1.5;
    HeartBeatParam->Chart1->BottomAxis->Minimum = startMinAmValue-1.5;

    HeartBeatParam->Chart2->LeftAxis->Maximum = HeartBeatParam->Series2->MaxYValue();
    HeartBeatParam->Chart2->LeftAxis->Minimum = 0;
    HeartBeatParam->Chart2->BottomAxis->Maximum = AMvalue - 1.5;
    HeartBeatParam->Chart2->BottomAxis->Minimum = startMinAmValue-1.5;


    HeartBeatParam->redLine1->ParentChart = HeartBeatParam->Chart1;
    HeartBeatParam->redLine1->XYStyle =  xysAxisOrigin;
    HeartBeatParam->redLine1->X0 = minAmValue;
    HeartBeatParam->redLine1->X1 = 5;
    HeartBeatParam->redLine1->Y0 = (int)(minBpmStd*100);
    HeartBeatParam->redLine1->Y1 = 500;

    HeartBeatParam->redLine2->ParentChart = HeartBeatParam->Chart2;
    HeartBeatParam->redLine2->XYStyle =  xysAxisOrigin;
    HeartBeatParam->redLine2->X0 = minAmValue;
    HeartBeatParam->redLine2->X1 = 5;
    HeartBeatParam->redLine2->Y0 = HeartBeatParam->Series2->MaxYValue();
    HeartBeatParam->redLine2->Y1 = 500;


    Screen->Cursor = crDefault;
    if(automatic || HeartBeatParam->ShowModal() == IDOK)
    {
        // AM beat detection
        Screen->Cursor = crHourGlass;
       	char buff[1000];

        AMvalue = HeartBeatParam->amValueInput->Text.ToDouble();
        //AMvalue = min(maxAmValue, max(startMinAmValue, AMvalue));
        AMmaxValue = HeartBeatParam->amMaxValue->Text.ToDouble();

        int numChannelsBefore = measurement.getNumEventChannels();
        calcBeatTimes(LibCore::CMeasurement::beatAlgorithmAM, AMvalue, true, AMmaxValue);
        if(measurement.getNumEventChannels() <= numChannelsBefore){
             // no new channel created!
            deleteChannel();
            selectChannel(0,false, false); //select first signal channel
            refreshWholeWindow();
            return;             
        }
        deleteChannel();
        selectChannel(measurement.getNumEventChannels()-1, true, false);
    	LibCore::CEventChannel &eventChan = measurement.getEventChannel(measurement.getNumEventChannels()-1);

        // removeExtremeEvents();
        try{
            eventChan.removeExtremeEvents(extremeEventsMin, extremeEventsMax);
        }catch (LibCore::EKGException *e){
            //channel has no events-> do not continue and leave state as is
            deleteChannel();
            selectChannel(0, false, false);
            Screen->Cursor = crDefault;
            return;
        }

        //fix y values
        assignInterEventTimeAsEventValuesSameChannel();
        //fix values that got set very wrong
//        eventChan.ceilExtremeEvents(extremeEventsMax);
        eventChan.removeExtremeEvents(extremeEventsMin, extremeEventsMax);

        if (eventChan.getNumElems() == 0) {
    	    measurement.deleteEventChannel(numSelectedChannel);
            if(measurement.getNumEventChannels()>0){
                selectChannel(measurement.getNumEventChannels()-1, true, false);
                dirty = true;
                repaintGraph();

                // calcStatistics
            	stat = eventChan.calcStatistics();

            	sprintf(buff, "Stevilo utripov: %i\r\nBPM: %0.1f +- %0.2f\%",
           	    		stat.numOfEvents, 60/stat.avgValue, stat.stDevValue*100);
                refreshWholeWindow();
            	if(!automatic) Application->MessageBox(buff, "Statistika utripov." , MB_OK);
            	eventChan.appendComment(buff);
            }
            else{
                selectChannel(measurement.getNumChannels()-1, false, false);
            }

        }
        else{
           	stat = measurement.getEventChannel(measurement.getNumEventChannels()-1).calcStatistics();
            selectChannel(measurement.getNumEventChannels()-1,true, false);
          	sprintf(buff, "Stevilo utripov: %i\r\nBPM: %0.1f +- %0.2f\%",
           	    		stat.numOfEvents, 60/stat.avgValue, stat.stDevValue*100);
            refreshWholeWindow();
        	if(!automatic) Application->MessageBox(buff, "Statistika utripov." , MB_OK);
        }
        Screen->Cursor = crDefault;
    }else{
        deleteChannel();
        selectChannel(0,false, false); //select first signal channel
    }
    refreshWholeWindow();
}

//IvanT
void TDocumentForm::generate12leadMeasurement(const TDocumentForm* source) {
    if (source) {
        source->measurement.calculateECG12(&measurement);
        ArrangeDisplay();
    } else {
        Application->MessageBox("Napaka", "Napaka pri generiranju meritve - izvorna meritev ni veljavna", MB_OK);
    }
}


class TGraphPainter12Lead : public TGraphPainter {
    int calibrationSamples, totalSamples;

public:
    TGraphPainter12Lead(int ps, int ts) {
        calibrationSamples = ps;
        totalSamples = ts;
    }
    
    void afterGraphDraw(TChart * const chart) {
        static AnsiString chanName[12];
        if (chanName[0].Length() < 1) {
            chanName[0] = "I";         
            chanName[1] = "II";
            chanName[2] = "III";
            chanName[3] = "aVR";
            chanName[4] = "aVL";
            chanName[5] = "aVF";
            chanName[6] = "V1";
            chanName[7] = "V2";
            chanName[8] = "V3";
            chanName[9] = "V4";
            chanName[10]= "V5";
            chanName[11]= "V6";
        }
        TRect rect = chart->ChartRect;
        
        for (int x = 0; x < 4; ++x) {
            for (int y = 0; y < 3; ++y) {
                double relX = (calibrationSamples / (double)totalSamples) + x * (0.25 * (totalSamples - calibrationSamples) / (double)totalSamples);
                double relY = y * 0.25;
                chart->Canvas->TextOut3D(
                    rect.Right * relX + rect.Left * (1.0 - relX), 
                    rect.Bottom * relY + rect.Top * (1.0 - relY), 
                    0, chanName[x * 3 + y]);
            }
        }
    }
};


void TDocumentForm::generate12leadGraph(const TDocumentForm* source) {
    if (source) {
        int nc = measurement.generate12leadGraph(&source->measurement);
        additionalPainter = new TGraphPainter12Lead(nc, measurement.getNumSamples());
        Chart->LeftAxis->Grid->Visible = true;
//        Chart->LeftAxis->MinorGrid->Visible = true;
        Chart->LeftAxis->MinorTickCount = 5;        
        Chart->LeftAxis->TickOnLabelsOnly = false;
        Chart->LeftAxis->Increment = 0.2;
        Chart->BottomAxis->Grid->Visible = true;
//        Chart->BottomAxis->MinorGrid->Visible = true;
        Chart->BottomAxis->MinorTickCount = 5;
        Chart->BottomAxis->TickOnLabelsOnly = false;
        Chart->BottomAxis->Increment = 0.2;
        
        ArrangeDisplay();
    } else {
        Application->MessageBox("Napaka", "Napaka pri generiranju meritve - izvorna meritev ni veljavna", MB_OK);
    }
}


void TDocumentForm::ArrangeDisplay(void)
{
        //load & set window components properties
	    Caption = measurement.getMeasurementName();
    	xLeft = 0;
	    xRight = measurement.getDuration();
    	numSelectedChannel = -1;
	    for (int i = 0; i < measurement.getNumChannels(); i++)
		    if (measurement[i].viewProps.visible)
    		{
	    		eventChannelSelected = false;
		    	numSelectedChannel = i;
			    break;
	    	}

    	if (numSelectedChannel == -1)
		    for (int i = 0; i < measurement.getNumEventChannels(); i++)
    		if (measurement.getEventChannel(i).viewProps.visible)
	    	{
		    	selectChannel(i, true, false);
			    break;
    		}

        if (numSelectedChannel == -1)
		    for (int i = 0; i < measurement.getNumAnnotationChannels(); i++)
    		if (measurement.getAnnotationChannel(i).viewProps.visible)
	    	{
		    	selectChannel(i, true, true);
			    break;
    		}

	    if (numSelectedChannel == -1)
		    selectChannel(0, false, false);

    	refreshWholeWindow();
	    undoStack.resetStack();
    	dirty = false;
}


void __fastcall TDocumentForm::ChartAfterDraw(TObject *Sender)
{
    if (additionalPainter) 
        additionalPainter->afterGraphDraw(Chart);
}


double TDocumentForm::getMissingSamplesValue(){
    return measurement.getMissingSamplesValue(eventChannelSelected, numSelectedChannel);
}

int TDocumentForm::getDocumentVersion()
{
    return measurement.getNekgFileVersion();
}

void TDocumentForm::setDocumentVersionAndBits(int ver, int bits){
    measurement.setNekgFileVersion(ver);
    measurement.setBitSaving(bits);
}


void TDocumentForm::absoluteValueOfChannel(){
    if (annotationChannelSelected)
		measurement.getAnnotationChannel(numSelectedChannel).channelAbsolute();
	else if (eventChannelSelected)
		measurement.getEventChannel(numSelectedChannel).channelAbsolute();
	else
		measurement[numSelectedChannel].channelAbsolute();
	dirty = true;
	refreshWholeWindow();
}

void TDocumentForm::correctToMiliVolts(){
    assert(!eventChannelSelected);
    if(measurement[numSelectedChannel].getMeasurementUnit().AnsiCompare("(null)") == 0){
        measurement[numSelectedChannel].channelSetOffset(-measurement[numSelectedChannel].getAvgValue());
        measurement[numSelectedChannel].channelGain(0.006);
        measurement[numSelectedChannel].setMeasurementUnit("mV");
    }
}

void TDocumentForm::centerCursorView(double duration){
    try{
    	int cursorNum = comboCursorColor->ItemIndex;
        double mid = cursors.c[cursorNum];
        if(mid == -1){
            Application->MessageBox("Postavite izbrani kurzor na meritev!", "Napaka", MB_OK);
            return;

        }
        xLeft = max(0.0,mid - (duration/2.0));
        xRight = min(xLeft + duration, measurement.getDuration());
        xLeft = max(0.0, xRight - duration);
        repaintGraph();
    }catch(Exception *e){
        //someting went wrong, is cursor selected?
    }
}

void __fastcall TDocumentForm::show10secBtnClick(TObject *Sender)
{

    centerCursorView(10);

}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::show30secBtnClick(TObject *Sender)
{
    centerCursorView(30);

}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::show60secBtnClick(TObject *Sender)
{
    centerCursorView(60);
}
//---------------------------------------------------------------------------
void TDocumentForm::windowFilterEventChannel(double windowSize, double step, bool strictTime, bool refresh){
    if(!eventChannelSelected || annotationChannelSelected){
        Application->MessageBox("Izberite veljaven dogodkovni kanal!", "Napaka", MB_OK);
        return;
    }
    int numnew = measurement.addEventChannel("temp",measurement.getEventChannel(numSelectedChannel).getMeasurementUnit());
    measurement.calcWindowAverage(numSelectedChannel ,numnew, windowSize,step,0,measurement.getDuration(), strictTime);
    if(refresh)
        refreshWholeWindow();
}

void TDocumentForm::setCurrentChannelVisible(bool visible){
    if(!eventChannelSelected && !annotationChannelSelected){
        measurement[numSelectedChannel].viewProps.visible = visible;
    }
    if(eventChannelSelected && !annotationChannelSelected){
        measurement.getEventChannel(numSelectedChannel).viewProps.visible = visible;
    }
    if(annotationChannelSelected){
        measurement.getAnnotationChannel(numSelectedChannel).viewProps.visible = visible;
    }
}
void __fastcall TDocumentForm::show10minbuttonClick(TObject *Sender)
{
    centerCursorView(600);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::show30minbuttonClick(TObject *Sender)
{
    centerCursorView(1800);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::Button3Click(TObject *Sender)
{
    centerCursorView(3600);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::show4hbuttonClick(TObject *Sender)
{
    centerCursorView(14400);
}
//---------------------------------------------------------------------------

void __fastcall TDocumentForm::openStaggered(double time, double duration)
{
    try{
        double mid = (time - measurement.getDate()) * 86400;
        xLeft = max(0.0,mid - (duration/2.0));
        xRight = min(xLeft + duration, measurement.getDuration());
        xLeft = max(0.0, xRight - duration);
        repaintGraph();
    }catch(Exception *e){
        //someting went wrong, is cursor selected?
    }
}


int TDocumentForm::getNumEventChannel(){
    return measurement.getNumEventChannels();
}

int TDocumentForm::saveGraphWMFShowAll(AnsiString location, double length, int quality){
    int numFilesGenerated = 0;
    double step = length;
    AnsiString currFile = location; 
    try{

        //HACK draw whatever is on screen, just so we resize the window
        Chart->Height = quality*250;
        Chart->Width = quality*1500;
        Chart->SaveToMetafile(currFile + "random.wmf");
        //HACK Repeat -> once is not enought as it looks like
        Chart->Height = quality*250;
        Chart->Width = quality*1500;
        Chart->SaveToMetafile(currFile + "random.wmf");
        Chart->Height = quality*250;
        Chart->Width = quality*1500;

        //hide all channels;
        for(int i=0;i<measurement.getNumChannels();i++){
            measurement[i].viewProps.visible = false;
        }
        for(int i=0;i<measurement.getNumEventChannels();i++){
            measurement.getEventChannel(i).viewProps.visible = false;
        }
        for(int i=0;i<measurement.getNumAnnotationChannels();i++){
            measurement.getAnnotationChannel(i).viewProps.visible = true;
        }
        //show last 2 event channels
        for(int i = measurement.getNumEventChannels()-2; i < measurement.getNumEventChannels();i++){
            if(i == measurement.getNumEventChannels()-2)
                measurement.getEventChannel(i).setSmallDots(true);
            if(i == measurement.getNumEventChannels()-1)
                measurement.getEventChannel(i).setContiguous(true);
            measurement.getEventChannel(i).viewProps.visible = true;
        }
        btnShowChannelTable->Down = false;
        trueYscale = true;
    	xLeft = 0;
	    xRight = min(measurement.getDuration(), step);

        Chart->Height = quality*250;
        Chart->Width = (int)(quality*1500.0 * ((xRight-xLeft) /length));
      	measurement.refreshGraphValuesForExport(Chart, xLeft, xRight, yScaleMode, trueYscale,
        	eventChannelSelected, annotationChannelSelected, numSelectedChannel, true, false, 0, quality);
        Chart->Height = quality*250;
        Chart->Width = (int)(quality*1500.0 * ((xRight-xLeft) /length));

        Chart->BorderWidth = 0;
        Chart->BevelWidth = 0;
        Chart->Frame->Visible = false;
        Chart->MarginLeft = 0;
        Chart->LeftAxis->LabelsSize = 40; 
        Chart->MarginRight = 0;
        Chart->MarginBottom= 0;
        Chart->MarginTop = 0;
        Chart->SaveToMetafile(currFile + AnsiString(numFilesGenerated+1)+".wmf");
        numFilesGenerated++;
       	xLeft += step;
   	    xRight = min(measurement.getDuration(), xRight + step);

        while(xLeft < measurement.getDuration()){
            Chart->Height = quality*250;
            Chart->Width = (int)(quality*1500.0 * ((xRight-xLeft) /  length));
        	measurement.refreshGraphValuesForExport(Chart, xLeft, xRight, yScaleMode, trueYscale,
		    	eventChannelSelected, annotationChannelSelected, numSelectedChannel, true, false, 0, quality);
            Chart->Height = quality*250;
            Chart->Width = (int)(quality*1500.0 * ((xRight-xLeft) /  length));

            Chart->SaveToMetafile(currFile + AnsiString(numFilesGenerated+1)+".wmf");
            numFilesGenerated++;
           	xLeft += step;
       	    xRight = min(measurement.getDuration(), xRight + step); //4hourt pices
        }

    }catch(Exception *e){
        AnsiString msg = "Problem z izvozom datoteke: " + location + "\n" + e->Message;
        Application->MessageBox(msg.c_str(), "Napaka", MB_OK);
        numFilesGenerated = -1;
    }
    return numFilesGenerated;
}


int TDocumentForm::saveGraphWMFShowAllAnnotations(AnsiString location, FILE *file, int fileNumber, int * numAnnotations, AnsiString *comStr, int * sequenceChar, int * sequenceNumbering, int quality){
    int numFilesGenerated = 0;
    AnsiString currFile = location; 
    try{

        //HACK draw whatever is on screen, just so we resize the window
        Chart->Height = quality*250;
        Chart->Width = quality*1500;
        Chart->SaveToMetafile(currFile + "random.wmf");
        //HACK Repeat -> once is not enought as it looks like
        Chart->Height = quality*250;
        Chart->Width = quality*1500;
        Chart->SaveToMetafile(currFile + "random.wmf");
        Chart->Height = quality*250;
        Chart->Width = quality*1500;


        //hide all channels //except singnal[0];
        for(int i=0;i<measurement.getNumChannels();i++){
            measurement[i].viewProps.visible = false;
            if(i==0)
                measurement[i].viewProps.visible = true;            
        }
        for(int i=0;i<measurement.getNumEventChannels();i++){
            measurement.getEventChannel(i).viewProps.visible = false;
        }
        for(int i=0;i<measurement.getNumAnnotationChannels();i++){
            measurement.getAnnotationChannel(i).viewProps.visible = true;
        }
        //show second to last  event channel
        for(int i = measurement.getNumEventChannels()-2; i < measurement.getNumEventChannels();i++){
            if(i == measurement.getNumEventChannels()-2){
                measurement.getEventChannel(i).setSmallDots(false);
                measurement.getEventChannel(i).viewProps.visible = true;
            }
        }
        btnShowChannelTable->Down = false;
        trueYscale = true;

        for(int i=0;i<measurement.getNumAnnotationChannels();i++){
            double previousEventTime = -1;
            //measurement.getAnnotationChannel(i).sortEventsByAscendingTime();
            for(int j=0;j<measurement.getAnnotationChannel(i).getNumElems();j++){

                double annNum = measurement.getAnnotationChannel(i)[j].value;
                int annInt = max(0,min(4,2*(int)annNum));
                numAnnotations[annInt]++;
                if(annNum <= 0)
                    continue;
                if(previousEventTime != -1 && measurement.getAnnotationChannel(i)[j].time - previousEventTime < 120)
                    continue;
                previousEventTime = measurement.getAnnotationChannel(i)[j].time;

                double mid = measurement.getAnnotationChannel(i)[j].time;
                xLeft = max(0.0, mid - 30);
        	    xRight = min(measurement.getDuration(), mid + 30);

                if(xLeft > measurement.getDuration() || xRight < 0)
                    continue;

                numAnnotations[annInt+1]++;

                if(annInt>3){
                    if(numAnnotations[6] < 5)
                        AppendStr(*comStr, measurement.getAnnotationChannel(i)[j].annotation + "\\n");
                    //if(numAnnotations[6] == 5)
                    //    AppendStr(*comStr, "...");
                    numAnnotations[6]++;
                }

                double number_of_mins = 8.0, number_of_mins_before = 5;
                double xLeftTemp = xLeft - (60.0*number_of_mins_before), xRightTemp;

                for(int ii = 0; ii < number_of_mins; ii++){

                    xLeftTemp =  max(0.0, min(measurement.getDuration(), xLeftTemp + 60.0));
                    xRightTemp = max(0.0, min(measurement.getDuration(), xLeftTemp + 60));
                    
                    if(xLeftTemp >= xRightTemp)
                        continue;
                    double startOffsetTime = xLeftTemp - measurement.getAnnotationChannel(i)[j].time;

                    //resize before and after drawing
                    Chart->Height = quality*250;
                    Chart->Width = (int) max(quality*750.0, quality*1500.0 * ( (xRightTemp-xLeftTemp) / 60.0));

          	        measurement.refreshGraphValuesForExport(Chart, xLeftTemp, xRightTemp, yScaleMode, trueYscale,
                    	eventChannelSelected, annotationChannelSelected, numSelectedChannel, true, true, startOffsetTime, quality);
                    Chart->Height = quality*250;
                    Chart->Width = (int) max(quality*750.0, quality*1500.0 * ( (xRightTemp-xLeftTemp) / 60.0));

                    Chart->BorderWidth = 0;
                    Chart->BevelWidth = 0;
                    Chart->Frame->Visible = false;
                    Chart->MarginLeft = 0;
                    Chart->MarginRight = 0;
                    Chart->MarginBottom= 0;
                    Chart->MarginTop = 0;
                    Chart->BottomAxis->Increment = 0;
                    if(1 < (int)Chart->BottomAxis->CalcIncrement()){
                        Chart->BottomAxis->MinorGrid->Visible= true;
                        Chart->BottomAxis->MinorGrid->Width = 1;
                        Chart->BottomAxis->MinorTickCount = ((int)Chart->BottomAxis->CalcIncrement()) - 1;
                    }
                    else{
                        Chart->BottomAxis->MinorGrid->Visible=false;
                    }
                    numFilesGenerated++;
                    Chart->SaveToMetafile(currFile + AnsiString(numFilesGenerated)+".wmf");

                    fprintf(file,"%dann_%d.wmf\n",fileNumber ,numFilesGenerated);
                    double percent = 100.0 *( (xRightTemp-xLeftTemp) / 60);
                    fprintf(file,"%lf\n", max(50.0,percent));
                    if(ii==0){
                        TDateTime curFileDate = measurement.getDate() + (xLeftTemp/86400);
                        //fprintf(file, "[ ");
                        //fprintf(file, curFileDate.DateTimeString().c_str() );
                        //fprintf(file, " ]    ");
                        if( measurement.getAnnotationChannel(i)[j].value == 1){
                            fprintf(file, "%i:",fileNumber);
                            fprintf(file, AnsiString(measurement.getAnnotationChannel(i)[j].annotationNum).c_str());
                            fprintf(file, ") ");
                        }
                        else if( measurement.getAnnotationChannel(i)[j].value > 1){
                            fprintf(file, "%i:%c) ",fileNumber, measurement.getAnnotationChannel(i)[j].annotationNum);
                        }

                        fprintf(file, "Dogodek \"");
                        fprintf(file, measurement.getAnnotationChannel(i)[j].annotation.c_str());
                        fprintf(file, "\" ob " );
                        curFileDate = measurement.getDate() + (mid/86400);
                        fprintf(file, curFileDate.TimeString().c_str() );
                        //fprintf(file, "    [ " );
                        //curFileDate = measurement.getDate() + (xRightTemp/86400);
                        //fprintf(file, curFileDate.DateTimeString().c_str());
                        //fprintf(file," ]");
                    }
                    fprintf(file, "\n");


                    if(xRightTemp == measurement.getDuration()){
                        break; 
                    }
                }
                fprintf(file,"<line>\n");
            }
        }

    }catch(Exception *e){
        AnsiString msg = "Problem z izvozom datoteke: " + location + "\n" + e->Message;
        Application->MessageBox(msg.c_str(), "Napaka", MB_OK);
        numFilesGenerated = -1;
    }
    return numFilesGenerated;
}



void TDocumentForm::findBeatNumAndDev(int numSamples, double threshold, double limit, double errorValue, double* signal, double* derivative, double* number, double* deviation, double* avgValue, bool createChannel, bool automatic){

    double MaxHR = 250;
    double MinHR = 24;

    double currDeviation = 0;
    double currAvg = 0;
    std::vector<double> eventValues;

    double frequency = measurement.getSamplingRate();

    //create Channel if so desired
    int createdChannel = -1;

    int nextEventIndex = 0;
    if(createChannel){
        createdChannel = measurement.addEventChannel("RRI iz M", "\min");
        measurement.getEventChannel(createdChannel).reallocTable(10000);
        measurement.getEventChannel(createdChannel).setSmallDots(false);
        measurement.getEventChannel(createdChannel).setContiguous(false);
        AnsiString comment = "RRI z mejo: "+ AnsiString(threshold)+
                            ", zgornjo mejo: " +AnsiString(limit)+
                            ", napako z vrednostjo: "+AnsiString(errorValue);

        measurement.getEventChannel(createdChannel).appendComment(comment, true);
    }


    // counters for numbers of each kind of peaks (disregard minority)
    int rightPeaks = 0;
    int leftPeaks = 0;
    //go over derivative and search for waves above the threshold
    int i = 0;
    int indexPreviousPeak = -1;
    bool beatDirectionClear = false; 
    double previous_confirmed_peak = 0;
    for(;i < numSamples; i++){
        if(fabs(derivative[i])<threshold)
            continue;

        //find highest peak on derivative wave
        int iPeak1 = i;
        double peak1Val = fabs(derivative[i]);
        int j=i;
        for(; j< numSamples && fabs(derivative[j])>threshold; j++){
            if(fabs(derivative[j]) > peak1Val){
                iPeak1 = j;
                peak1Val = fabs(derivative[j]);
            }
        }

        // if peak is too high or is near errorValue -> skip such peaks
        if(peak1Val > limit ||
            signal[iPeak1] == errorValue ||
            ( iPeak1>0 && signal[iPeak1-1] == errorValue)||
            ( iPeak1<(numSamples-1) && signal[iPeak1+1] == errorValue)
            ){
            i = i+(int)(frequency * 0.05);
            continue;
        }

        //find highest peak left or right +-0.05s
        //i==begining of peak, j = end of peak, iPeak1 = peak of first wave
        int iPeak2 = iPeak1;
        double peak2Val;

        //if first iPeak1 is positive than next one should be negative and vice versa
        int k=1;

        if(!beatDirectionClear){
            if(derivative[iPeak1] >= 0){
                peak2Val = 10e+100;
                for(; k<1+(int)(frequency * 0.05); k++){
                    if(iPeak1 - k >= 0 && derivative[iPeak1-k] < peak2Val){
                        peak2Val = derivative[iPeak1-k];
                        iPeak2 = iPeak1-k;
                    }
                    if(iPeak1 + k < numSamples && derivative[iPeak1+k] < peak2Val){
                        peak2Val = derivative[iPeak1+k];
                        iPeak2 = iPeak1+k;
                    }
                }
            }
            else{
                peak2Val = -(10e+100);
                for(; k<1+(int)(frequency * 0.05); k++){
                    if(iPeak1 - k >= 0 && derivative[iPeak1-k] > peak2Val){
                        peak2Val = derivative[iPeak1-k];
                        iPeak2 = iPeak1-k;
                    }
                    if(iPeak1 + k < numSamples && derivative[iPeak1+k] > peak2Val){
                        peak2Val = derivative[iPeak1+k];
                        iPeak2 = iPeak1+k;
                    }
                }
            }
        }else{//beatDirectionClear
            if(derivative[iPeak1] >= 0){
                peak2Val = 10e+100;
                for(; k<1+(int)(frequency * 0.05); k++){
                    if(leftPeaks > rightPeaks && iPeak1 - k >= 0 && derivative[iPeak1-k] < peak2Val){
                        peak2Val = derivative[iPeak1-k];
                        iPeak2 = iPeak1-k;
                    }
                    if(leftPeaks < rightPeaks && iPeak1 + k < numSamples && derivative[iPeak1+k] < peak2Val){
                        peak2Val = derivative[iPeak1+k];
                        iPeak2 = iPeak1+k;
                    }
                }
            }
            else{
                peak2Val = -(10e+100);
                for(; k<1+(int)(frequency * 0.05); k++){
                    if(leftPeaks > rightPeaks && iPeak1 - k >= 0 && derivative[iPeak1-k] > peak2Val){
                        peak2Val = derivative[iPeak1-k];
                        iPeak2 = iPeak1-k;
                    }
                    if(leftPeaks < rightPeaks && iPeak1 + k < numSamples && derivative[iPeak1+k] > peak2Val){
                        peak2Val = derivative[iPeak1+k];
                        iPeak2 = iPeak1+k;
                    }
                }
            }
        }

        // we must search from highest peak to lower ones
        if(fabs(peak2Val) > fabs(peak1Val) && iPeak2>iPeak1){
            i=iPeak2-1;
            continue;
        }


        if(!beatDirectionClear){
            if(iPeak1 <iPeak2){ rightPeaks++; }
            else          { leftPeaks++; }

            if(abs(leftPeaks - rightPeaks) > 50)
                beatDirectionClear = true;
        }

        if( (rightPeaks>leftPeaks && iPeak1 > iPeak2) ||
            (rightPeaks<leftPeaks && iPeak1 < iPeak2))
        {
            i = max(iPeak1, iPeak2) -1 + 0.05*frequency;
            continue;
        }

        //both peaks should be ~equal in height (P and T- wave < 1/4 of R)
        if(fabs(peak2Val) < fabs(peak1Val)/4
            ||fabs(peak1Val) < fabs(peak2Val)/4
        //both peaks should be higher than threshold
        //    || fabs(peak2Val) < threshold
            ){
            //i=j+(int)(frequency * 0.05);
            //continue;
        }

        //find actual peak on signal between iPeak1 and iPeak2 (derivatives)
        int currPeak = iPeak1;
        double currPeakVal = fabs(signal[iPeak1]);
        int k1=iPeak1;
        while(k1 != iPeak2){
            if(iPeak1<iPeak2)  k1++;
            else               k1--;
            if(fabs(signal[k1]) > currPeakVal){
                currPeak = k1;
                currPeakVal = fabs(signal[k1]);
            }
        }

        //edge case where we detect same peak twice (once from front, once from the back)
        // do not add event if HR > MaxHR
        double currVal = measurement.sampleIndex2Time(currPeak) - measurement.sampleIndex2Time(indexPreviousPeak);
        if(currVal < 60/MaxHR)
        {
            i = j+(int)(frequency * 0.05);
            continue;
        }


        if(indexPreviousPeak + 5*frequency < currPeakVal){
            previous_confirmed_peak = 0;
        }

        //previous and currnet peak should be ~equal in height
        if(previous_confirmed_peak > fabs(currPeakVal*3)){
            i = j+(int)(frequency * 0.02);
            continue;
        }

        eventValues.push_back(currVal);
        currAvg += currVal;

        //confirm peak
        if(createdChannel!=-1){
            LibCore::Event newEvent;
            double time = measurement.sampleIndex2Time(currPeak);
            double value = 0;
            if(nextEventIndex > 1){
                value = (time - measurement.getEventChannel(createdChannel)[nextEventIndex-1].time);
                newEvent = LibCore::Event(time, value);
            }else if(nextEventIndex == 1){
                value = (time - measurement.getEventChannel(createdChannel)[nextEventIndex-1].time);
                newEvent = LibCore::Event(time, value);
                //fix first event to the same value
                measurement.getEventChannel(createdChannel)[nextEventIndex-1].value = value;
            }else{
                //first event gets value 0 than second event fixes it
                newEvent = LibCore::Event(time, value);
            }
            measurement.getEventChannel(createdChannel).insertElem(nextEventIndex++, newEvent);
        }
        previous_confirmed_peak = fabs(currPeakVal);
        indexPreviousPeak = currPeak;
        i = j+(int)(frequency * 0.05);
    }

    //remove all event that are below MinHR (only on event channel, since this should have only a small effect on statistics)
    if(createdChannel!=-1){
            measurement.refineBeatRateWithInterpolationSameChannel(numSelectedChannel, createdChannel);
        try{
            measurement.getEventChannel(createdChannel).removeExtremeEvents(60/MaxHR, 60/MinHR);
        }catch (LibCore::EKGException *e){
            selectChannel(measurement.getNumEventChannels()-1, true, false);
            deleteChannel();
            selectChannel(0, false, false);
            Screen->Cursor = crDefault;
            return;
        }
        if(measurement.getNumEventChannels() <= createdChannel)
            return;
        measurement.getEventChannel(createdChannel).sortEventsByAscendingTime();

        LibCore::CEventChannel::Statistics stat;
        try{
            stat = measurement.getEventChannel(createdChannel).calcStatistics();
        }catch(Exception *e){
            int sadkljasldkj = 0;
        }
        //return by reference
        *number = stat.numOfEvents;
        *deviation = stat.stDevValue;
        *avgValue = (stat.avgValue != 0)?60/stat.avgValue:0.0; //to BPM

        char buff[1000];
        sprintf(buff, "Stevilo utripov: %i\r\nRRI: %0.2f +- %0.2f\%",
           	    		stat.numOfEvents, stat.avgValue, stat.stDevValue);
      	if(!automatic) Application->MessageBox(buff, "Statistika utripov." , MB_OK);
    }else{
        //return by reference
        if(eventValues.size() == 0){
            *number   = nextEventIndex;
            *deviation= 0;
            *avgValue = 0;
            return;
        }
        *number   = eventValues.size();
        *avgValue = (currAvg/(*number));
        for (std::vector<double>::iterator iV = eventValues.begin();
                           iV != eventValues.end();
                           ++iV){
            *deviation += (*iV - (*avgValue)) * (*iV - (*avgValue));
        }
        *deviation = sqrt((*deviation)/(*number));

        *avgValue =(*avgValue != 0)? 60/(*avgValue):0;  //to BPM
    }
}



void TDocumentForm::findBeatNumAndDevConv(int numSamples, double threshold, double limit, double errorValue, double* signal, double* derivative, double* number, double* deviation, double* avgValue, bool createChannel, bool automatic){

double MaxHR = 250;
    double MinHR = 24;

    double currDeviation = 0;
    double currAvg = 0;
    std::vector<double> eventIndices;

    double frequency = measurement.getSamplingRate();
    int convMaskLength = frequency * 0.3;
    double *convMask = (double*) calloc(sizeof(double), convMaskLength);
    double *convMaskSig = (double*) calloc(sizeof(double), convMaskLength);

    //create Channel if so desired
    int createdChannel = -1;
    int nextEventIndex = 0;
    if(createChannel){
        createdChannel = measurement.addEventChannel("RRI iz M", "\min");
        measurement.getEventChannel(createdChannel).reallocTable(10000);
        measurement.getEventChannel(createdChannel).setSmallDots(false);
        measurement.getEventChannel(createdChannel).setContiguous(false);
        AnsiString comment = "RRI z mejo: "+ AnsiString(threshold)+
                            ", zgornjo mejo: " +AnsiString(limit)+
                            ", napako z vrednostjo: "+AnsiString(errorValue);

        measurement.getEventChannel(createdChannel).appendComment(comment, true);
    }


    //go over derivative and search for waves above the threshold
    int indexPreviousPeak = -1;
    int i = 0;
    bool beatDirectionClear = false; 

    for(;i < numSamples; i++){
        if(fabs(derivative[i])<threshold)
            continue;

        //find highest peak on derivative wave
        int iPeak1 = i;
        double peak1Val = fabs(derivative[i]);
        int j=i;
        for(; j< numSamples && fabs(derivative[j])>threshold; j++){
            if(fabs(derivative[j]) > peak1Val){
                iPeak1 = j;
                peak1Val = fabs(derivative[j]);
            }
        }

        // if peak is too high or is near errorValue -> skip such peaks
        if(peak1Val > limit ||
            signal[iPeak1] == errorValue ||
            ( iPeak1>0 && signal[iPeak1-1] == errorValue)||
            ( iPeak1<(numSamples-1) && signal[iPeak1+1] == errorValue)
            ){
            i = i+(int)(frequency * 0.05);
            continue;
        }

        //find highest peak left or right +-0.05s
        //i==begining of peak, j = end of peak, iPeak1 = peak of first wave
        int iPeak2 = iPeak1;
        double peak2Val;

        //if first iPeak1 is positive than next one should be negative and vice versa
        int k=1;
        if(derivative[iPeak1] >= 0){
            peak2Val = 10e+100;
            for(; k<1+(int)(frequency * 0.05); k++){
                if(iPeak1 - k >= 0 && derivative[iPeak1-k] < peak2Val){
                    peak2Val = derivative[iPeak1-k];
                    iPeak2 = iPeak1-k;
                }
                if(iPeak1 + k < numSamples && derivative[iPeak1+k] < peak2Val){
                    peak2Val = derivative[iPeak1+k];
                    iPeak2 = iPeak1+k;
                }
            }
        }
        else{
            peak2Val = -(10e+100);
            for(; k<1+(int)(frequency * 0.05); k++){
                if(iPeak1 - k >= 0 && derivative[iPeak1-k] > peak2Val){
                    peak2Val = derivative[iPeak1-k];
                    iPeak2 = iPeak1-k;
                }
                if(iPeak1 + k < numSamples && derivative[iPeak1+k] > peak2Val){
                    peak2Val = derivative[iPeak1+k];
                    iPeak2 = iPeak1+k;
                }

            }
        }

        // we must search from highest peak to lower ones
        if(fabs(peak2Val) > fabs(peak1Val) && iPeak2>iPeak1){
            i=iPeak2-1;
            continue;
        }


        //both peaks should be ~equal in height (P and T- wave < 1/4 of R)
        if(fabs(peak2Val) < fabs(peak1Val)/4
            ||fabs(peak1Val) < fabs(peak2Val)/4
        //both peaks should be higher than threshold
        //    || fabs(peak2Val) < threshold
            ){
            i=j+(int)(frequency * 0.05);
            continue;
        }

        //find actual peak on signal between iPeak1 and iPeak2 (derivatives)
        int currPeak = iPeak1;
        double currPeakVal = fabs(signal[iPeak1]);
        int k1=iPeak1;
        while(k1 != iPeak2){
            if(iPeak1<iPeak2)  k1++;
            else               k1--;
            if(fabs(signal[k1]) > currPeakVal){
                currPeak = k1;
                currPeakVal = fabs(signal[k1]);
            }
        }

        //edge case where we detect same peak twice (once from front, once from the back)
        // do not add event if HR > MaxHR
        double currVal = measurement.sampleIndex2Time(currPeak) - measurement.sampleIndex2Time(indexPreviousPeak);
        if(currVal < 60/MaxHR)
        {
            i = j+(int)(frequency * 0.05);
            continue;
        }


        for(int cm = 0; cm < convMaskLength; cm++){
            convMask[cm] = 0.9*convMask[cm] +  0.01*derivative[currPeak + cm -convMaskLength/2];
            convMaskSig[cm] = 0.9*convMask[cm] +  0.01*signal[currPeak + cm -convMaskLength/2];
        }

        indexPreviousPeak = currPeak;
        i = j+(int)(frequency * 0.05);
    }


/* for debug
    double normalizeV1 = -10e100, normalizeV2 = -10e100;
    double *temporarySamples = (double*)calloc(sizeof(double), numSamples);
    double *temporarySamples2 = (double*)calloc(sizeof(double), numSamples);
    for(int i=convMaskLength/2; i<numSamples - convMaskLength/2; i++){
        temporarySamples[i] = 0;
        temporarySamples2[i] = 0;
        for(int j = 0; j< convMaskLength; j++){
            if(signal[i-j+convMaskLength/2] == errorValue)
            {
                //temporarySamples[i] -= convMaskLength;
                //temporarySamples2[i] -= convMaskLength;
                continue;
            }
            temporarySamples[i]  += derivative[i-j+convMaskLength/2] * convMask[j];
            temporarySamples2[i] += (signal[i-j+convMaskLength/2] - signal[i-convMaskLength/2]) * (convMaskSig[j] - convMaskSig[0]);
        }
        if(temporarySamples[i] > normalizeV1)
            normalizeV1 = temporarySamples[i];
        if(temporarySamples2[i] > normalizeV2)
            normalizeV2 = temporarySamples2[i];

    }


    int ch1 = measurement.addChannel("compare derivative", "NONE");
    for(int i = 0; i< numSamples;i++)
        measurement[ch1][i] = max(0.0, temporarySamples[i]/normalizeV1);

    int ch2 = measurement.addChannel("compare sample", "NONE");
    for(int i = 0; i< numSamples;i++)
        measurement[ch2][i] = max(0.0, temporarySamples2[i]/normalizeV2);


    int ch3 = measurement.addChannel("Convolution addition", "NONE");
    double maxAddit = -10e100;
    for(int i = 0; i< numSamples;i++){
        measurement[ch3][i] = measurement[ch1][i] + measurement[ch2][i];
    }

    int ch4 = measurement.addChannel("Convolution multiplication", "NONE");
    double maxMulti = -10e100;
    for(int i = 0; i< numSamples;i++){
        measurement[ch4][i] = measurement[ch1][i] * measurement[ch2][i];
        maxMulti = max(maxMulti, fabs(measurement[ch4][i]));
    }

    for(int i = 0; i< numSamples;i++)
        measurement[ch4][i] = measurement[ch4][i]/maxMulti;

    free(temporarySamples);
    free(temporarySamples2);
    */

}


void TDocumentForm::mobEcgGetBeatEventsNew(bool automatic) {

    if(eventChannelSelected || annotationChannelSelected){
        Application->MessageBox("Prosimo izberite signalni kanal!", "Napaka" , MB_OK);
        return;
    }

    double errorValue;

    double AMvalue;
    double AMmaxValue = 140; //AMmaxValue increased from 120 because regular beats with values ~125 found
    double filterFreq = 40;

    double startMinAmValue = 6;
    double minAmValue = startMinAmValue;
    double maxAmValue = 42;
    double amValueIncrement = 6;

    HeartBeatParam->amMaxValue->Text = AMmaxValue;

    Screen->Cursor = crHourGlass;

//get minimum number of events found
    double minDetectedHeartBeat = measurement.getMinimumHeartBeatEvents(numSelectedChannel);

//low pass
//    signalChan.
    double *temporarySamples = (double*)malloc(measurement[numSelectedChannel].getNumElems() * sizeof(double));
    // by Matjaz to Miha:
    // TODO: refactor all mallocs because:
    //
    // DO 
    // NOT
    // USE
    // malloc and its familly
    // ever
    // in c++
    // 
    // #include <vector>
    // ...
    // std::vector<double> temporarySamples(measurement[numSelectedChannel].getNumElems());
    // ..
    // uporablja� dalje enako kot do sedaj le da ni ve� treba free. Return sredi funkcije? ni problema, ker se vse samo pohendla

    LibCore::CChannel &signal = measurement[numSelectedChannel]; //open measurement channel
    errorValue = signal.getMinValue();

	int TminInSamples = 1;                         // for all samples
	int TmaxInSamples = signal.getNumElems();


    int filtHalfWindow = (int)(measurement.getSamplingRate()/ filterFreq/2); // the half width of filter window
    if (TmaxInSamples < (filtHalfWindow*2+1)){
        if(!automatic)
            Application->MessageBox("Frekvenca signalnega kanala je prenizka za izbrano filtriranje!", "Napaka" , MB_OK );
        free(temporarySamples);
        return;
    }
    if (filtHalfWindow == 0){
        if(!automatic)
            Application->MessageBox("Frekvenca signalnega kanala je previsoka za izbrano filtriranje!", "Napaka" , MB_OK );
        free(temporarySamples);
        return;
    }
    int i;
    for (i = 0; i<filtHalfWindow; i++)
        temporarySamples[TminInSamples-1+i] = measurement[numSelectedChannel][TminInSamples-1+i];   //fill first filtHalfWindow elements by unfiltered data

    for (; i < TmaxInSamples-filtHalfWindow; i++)
	{
        double numSkippedSamples = 0;
        temporarySamples[i] = measurement[numSelectedChannel][i];
        if(temporarySamples[i] == errorValue)
            continue;
        for (int j = 1; j<=filtHalfWindow; j++)
        {
            if(measurement[numSelectedChannel][i-j] == errorValue)
                numSkippedSamples+= (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1);
            else
                temporarySamples[i] += (measurement[numSelectedChannel][i-j] * (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1));
            if(measurement[numSelectedChannel][i+j] == errorValue)
                numSkippedSamples+= (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1);
            else
                temporarySamples[i] += (measurement[numSelectedChannel][i+j] * (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1));
        }
        temporarySamples[i] = temporarySamples[i] / (filtHalfWindow + 1 - numSkippedSamples);
	}

	for (; i < TmaxInSamples; i++)
        temporarySamples[i] = measurement[numSelectedChannel][i];   //fill last filtHalfWindow elements by unfiltered data

  
//symetrical differential of filtered signal
    double *temporarySamples2 = (double*)malloc(measurement[numSelectedChannel].getNumElems() * sizeof(double));
    double maxSampleValue = -(10e100);
	for (int i = 1; i < measurement[numSelectedChannel].getNumElems()-1; i++)
	{
        if(temporarySamples[i-1] == errorValue || temporarySamples[i+1] == errorValue)
            temporarySamples2[i-1] = 0;
        else
            temporarySamples2[i-1] = (temporarySamples[i+1] - temporarySamples[i-1]) / (2/(measurement.getSamplingRate()));
        maxSampleValue = max(fabs(temporarySamples2[i-1]), maxSampleValue);
    }

//-------------IN TESTING--------------------
    //second derivative
    /*
    double *temporarySamples3 = (double*)malloc(measurement[numSelectedChannel].getNumElems() * sizeof(double));
    maxSampleValue = -(10e100);
	for (int i = 1; i < measurement[numSelectedChannel].getNumElems()-1; i++)
	{
        if(temporarySamples2[i-1] == errorValue || temporarySamples2[i+1] == errorValue)
            temporarySamples3[i-1] = 0;
        else
            temporarySamples3[i-1] = (temporarySamples2[i+1] - temporarySamples2[i-1]) / (2/(measurement.getSamplingRate()));
        maxSampleValue = max(fabs(temporarySamples3[i-1]), maxSampleValue);
    }

    memcpy(temporarySamples2, temporarySamples3, measurement[numSelectedChannel].getNumElems() * sizeof(double));
    */
    //free(temporarySamples3);
//--------------------------
    free(temporarySamples);

/*
    measurement.addChannel("derivative", "NONE");
    for( int i = 0; i< measurement[numSelectedChannel].getNumElems();i++)
        measurement[measurement.getNumChannels()-1][i] = temporarySamples2[i];
*/

    if(maxSampleValue > maxAmValue * 10){
        if(!automatic && Application->MessageBox("Vrednosti odvoda so prevelike. Analiza tega posnetka lahko traja dolgo �asa. Nadaljujem?", "Dolgo izvajanje programa" , MB_OKCANCEL )
            != IDOK)
            free(temporarySamples2);
            return; //uporabnik ne �eli nadaljevati
    }

    AMvalue = minAmValue;
    double minBpmStd = 10e100; // std is abnormally high...

    HeartBeatParam->Series1->Clear();
    HeartBeatParam->Series2->Clear();

    AMvalue = minAmValue;
    while(AMvalue <= maxAmValue){
        double numBeatsFound = 0;
        double stdDeviationCalculated = 0;
        double averageValueCalculated = 0;

        findBeatNumAndDev(measurement[numSelectedChannel].getNumElems(), AMvalue, AMmaxValue, errorValue,  &measurement[numSelectedChannel][0], temporarySamples2, &numBeatsFound, &stdDeviationCalculated, &averageValueCalculated, false, automatic);

        HeartBeatParam->Series1->AddXY(AMvalue, stdDeviationCalculated, FormatFloat("0.#",averageValueCalculated),TColor(clRed));
        HeartBeatParam->Series2->AddXY(AMvalue, numBeatsFound, "", TColor(clBlue));

        if(numBeatsFound < minDetectedHeartBeat){
            break;
        }

        if(stdDeviationCalculated < minBpmStd){// && AMvalue > (0.5/4.0)*measurement.getSamplingRate()){ // AMvalue deriv. should be at least 0.5mV difference (but still show results if user boundaries are so chosen)
            minAmValue = AMvalue;
            minBpmStd = stdDeviationCalculated;
        }
        AMvalue += amValueIncrement;
    }

    HeartBeatParam->amValueInput->Text = AnsiString(minAmValue);

    HeartBeatParam->Chart1->LeftAxis->Maximum = HeartBeatParam->Series1->MaxYValue()*1.2;
    HeartBeatParam->Chart1->LeftAxis->Minimum = 0;
    HeartBeatParam->Chart1->BottomAxis->Maximum = AMvalue - 1.5;
    HeartBeatParam->Chart1->BottomAxis->Minimum = startMinAmValue-1.5;

    HeartBeatParam->Chart2->LeftAxis->Maximum = HeartBeatParam->Series2->MaxYValue()*1.2;
    HeartBeatParam->Chart2->LeftAxis->Minimum = 0;
    HeartBeatParam->Chart2->BottomAxis->Maximum = AMvalue - 1.5;
    HeartBeatParam->Chart2->BottomAxis->Minimum = startMinAmValue-1.5;


    HeartBeatParam->redLine1->ParentChart = HeartBeatParam->Chart1;
    HeartBeatParam->redLine1->XYStyle =  xysAxisOrigin;
    HeartBeatParam->redLine1->X0 = minAmValue;
    HeartBeatParam->redLine1->X1 = 5;
    HeartBeatParam->redLine1->Y0 = minBpmStd;
    HeartBeatParam->redLine1->Y1 = 500;

    HeartBeatParam->redLine2->ParentChart = HeartBeatParam->Chart2;
    HeartBeatParam->redLine2->XYStyle =  xysAxisOrigin;
    HeartBeatParam->redLine2->X0 = minAmValue;
    HeartBeatParam->redLine2->X1 = 5;
    HeartBeatParam->redLine2->Y0 = HeartBeatParam->Series2->MaxYValue();
    HeartBeatParam->redLine2->Y1 = 500;


    Screen->Cursor = crDefault;
    if(automatic || HeartBeatParam->ShowModal() == IDOK)
    {
        // AM beat detection
        Screen->Cursor = crHourGlass;
        AMvalue = HeartBeatParam->amValueInput->Text.ToDouble();
        AMmaxValue = HeartBeatParam->amMaxValue->Text.ToDouble();
        double numBeatsFound = 0;
        double stdDeviationCalculated = 0;
        double averageValueCalculated = 0;
        findBeatNumAndDev(measurement[numSelectedChannel].getNumElems(), AMvalue, AMmaxValue, errorValue,  &measurement[numSelectedChannel][0], temporarySamples2, &numBeatsFound, &stdDeviationCalculated, &averageValueCalculated, true, automatic);
//        findBeatNumAndDevConv(measurement[numSelectedChannel].getNumElems(), AMvalue, AMmaxValue, errorValue,  &measurement[numSelectedChannel][0], temporarySamples2, &numBeatsFound, &stdDeviationCalculated, &averageValueCalculated, true, automatic);
    }
    free(temporarySamples2);
    refreshWholeWindow(automatic);
    Screen->Cursor = crDefault;
}



void TDocumentForm::mobEcgGetBeatEventsv3(bool automatic, double filterFreq, double exp1, double amplituteThr, double thau, bool derivative) {
    double maxBPM = 250;
    double minBPM = 10;
    bool DEBUG_DETECTOR = false;


    //check that signal channel is selected
    if(eventChannelSelected || annotationChannelSelected){
        Application->MessageBox("Prosimo izberite signalni kanal!", "Napaka" , MB_OK);
        return;
    }

    //save to temporary samples is being changed 
    LibCore::CChannel &signal = measurement[numSelectedChannel]; //open measurement channel
    double *temporarySamples = (double*)malloc(signal.getNumElems() * sizeof(double));

    //get error value
    double errorValue = signal.getMinValue();
    int TminInSamples = 1; 
    int TmaxInSamples = signal.getNumElems();

    int filtHalfWindow = (int)(measurement.getSamplingRate()/ filterFreq/2); // the half width of filter window
    if (TmaxInSamples < (filtHalfWindow*2+1)){
        if(!automatic)
            Application->MessageBox("Frekvenca signalnega kanala je prenizka za izbrano filtriranje!", "Napaka" , MB_OK );
        return;
    }
    if (filtHalfWindow == 0){
        if(!automatic)
            Application->MessageBox("Frekvenca signalnega kanala je previsoka za izbrano filtriranje!", "Napaka" , MB_OK );
        return;
    }

    int i;
    for (i = 0; i<filtHalfWindow; i++)
        temporarySamples[TminInSamples-1+i] = measurement[numSelectedChannel][TminInSamples-1+i];   //fill first filtHalfWindow elements by unfiltered data

    for (  ; i < TmaxInSamples-filtHalfWindow; i++)
	{
        double numSkippedSamples = 0;
        temporarySamples[i] = measurement[numSelectedChannel][i];
        if(temporarySamples[i] == errorValue)
            continue;
        for (int j = 1; j<=filtHalfWindow; j++)
        {
            if(measurement[numSelectedChannel][i-j] == errorValue)
                numSkippedSamples+= (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1);
            else
                temporarySamples[i] += (measurement[numSelectedChannel][i-j] * (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1));
            if(measurement[numSelectedChannel][i+j] == errorValue)
                numSkippedSamples+= (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1);
            else
                temporarySamples[i] += (measurement[numSelectedChannel][i+j] * (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1));
        }
        temporarySamples[i] = temporarySamples[i] / (filtHalfWindow + 1 - numSkippedSamples);
	}

	for ( ; i < TmaxInSamples; i++)
        temporarySamples[i] = measurement[numSelectedChannel][i];   //fill last filtHalfWindow elements by unfiltered data


    //differentialSample will be used to detect beats further (if difference signal is not desired -> use temporary samples)        
    double *samples;

    if(DEBUG_DETECTOR){
        int ch2 = measurement.addChannel("ACTUAL FILTERED SAMPLE SIGNAL", "NONE");
        for(int i = 0; i< signal.getNumElems();i++)
            measurement[ch2][i] = temporarySamples[i];
    }

    //work on derivative or on sample
    if(derivative){
        samples = (double*)malloc(signal.getNumElems() * sizeof(double));
        samples[0] = 0;
        samples[signal.getNumElems()-1] = 0;
    	for (int i = 1; i < signal.getNumElems()-1; i++)
    	{
            if(temporarySamples[i+1] == errorValue || temporarySamples[i-1] == errorValue)
                samples[i] = 0;
            else
                samples[i] = (temporarySamples[i+1] - temporarySamples[i-1]) / (2/(measurement.getSamplingRate()));
        }
        free(temporarySamples);

        if(DEBUG_DETECTOR){
            int ch3 = measurement.addChannel("ACTUAL DERIVATIVE SIGNAL", "NONE");
            for(int i = 0; i< signal.getNumElems();i++)
                measurement[ch3][i] = samples[i];
        }

    }else{
        samples = temporarySamples;
    }

    //save indices of maximas and minimas on samples
    std::vector<int> maxima;

    int numSamp = signal.getNumElems();
    if(numSamp < 2){
        free(samples);
        return;
    }

    bool positiveDerivative = samples[0] < samples[1];
    for(int i=0;i<numSamp-1; i++){
        //todo in O(n) go through samples and find minmas and maximums, then save this somewhere
        if(measurement[numSelectedChannel][i] == errorValue
         ||measurement[numSelectedChannel][i+1] == errorValue)
         {
            continue;
         }
         if(i>0 && measurement[numSelectedChannel][i-1] == errorValue){
            positiveDerivative = samples[i] < samples[i+1];
         }

        if(positiveDerivative && samples[i] > samples[i+1]){
            maxima.push_back(i);
            positiveDerivative = false;
        }else if(!positiveDerivative && samples[i] < samples[i+1]){
            maxima.push_back(i);
            positiveDerivative = true;
        }
    }

    if(maxima.size() == 0){
        free(samples);
        return;
    }


    int maximaChannel = measurement.addEventChannel("RRI iz M", "s");
    measurement.getEventChannel(maximaChannel).setContiguous(false);


    int debugChannel=0, debugChannel2=0, debugChannel3 = 0, debugUp = 0, debugDown = 0;
    if(DEBUG_DETECTOR){
        debugChannel = measurement.addEventChannel("value ", "s");
        measurement.getEventChannel(debugChannel).setContiguous(false);
        debugChannel2 = measurement.addEventChannel("threshold ", "s");

        debugChannel3 = measurement.addEventChannel("peak ", "s");
        measurement.getEventChannel(debugChannel3).setContiguous(false);

        debugUp = measurement.addEventChannel("UP", "s");
        measurement.getEventChannel(debugUp).setContiguous(false);

        debugDown = measurement.addEventChannel("DOWN", "s");
        measurement.getEventChannel(debugDown).setContiguous(false);
     }


    //confirm peaks
    double frequency = measurement.getSamplingRate();
    double result;
    int left, middle, right;


    std::vector<LibCore::Event> peakIndex;
    double currMaxima = 0;
    int threshold = maxima[0];
    double minimumThreshold = amplituteThr*signal.channelMultiplier;
    if(derivative)
        minimumThreshold = minimumThreshold*frequency;

    minimumThreshold = minimumThreshold*minimumThreshold*minimumThreshold/frequency*minimumThreshold/frequency;

    bool knownDirection = false;
    int countDirectionUp = 0, countDirectionDown = 0;
    bool hysteresisUp = false, hysteresisDown = false;
    int LastBeatIndex = 0;
    int LastBeatMaximaIndex = 0;

    for(int i=1; i<maxima.size()-1; i++){

        if(DEBUG_DETECTOR){
            LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), 1);
            measurement.getEventChannel(debugChannel3).append(*tmpEvent2);
        }

        if(measurement[numSelectedChannel][maxima[i]-1] == errorValue
        || measurement[numSelectedChannel][maxima[i]] == errorValue
        || measurement[numSelectedChannel][maxima[i]+1] == errorValue)
        {
            continue;
        }

        if(DEBUG_DETECTOR){
            LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]+1), 2);
            measurement.getEventChannel(debugChannel3).append(*tmpEvent2);
        }

        result = 0;
        left = maxima[i-1];
        middle = maxima[i];
        right= maxima[i+1];

        //absolute values
        result = fabs(samples[left]-samples[middle]) * fabs(samples[middle] - samples[right]);
        //derivatives (check if for some reason division by 0 is possible 
        if(left!=middle)
            result = result * (fabs(samples[left]-samples[middle])/(middle - left));
        else
            result = 0;
        if(middle != right)
            result = result * (fabs(samples[middle]-samples[right])/(right - middle));
        else
            result = 0;

        //fix currMaxima

        double x1 = ((maxima[i]-LastBeatMaximaIndex)/frequency)/thau;
        double x2 = ((maxima[i-1]-LastBeatMaximaIndex)/frequency)/thau;
        double alpha = pow(x1,exp1) - pow(x2,exp1);
        alpha = pow(0.5, alpha);

//        double alpha = exp((((maxima[i-1] - maxima[i])/frequency)/thau));
        currMaxima = max(minimumThreshold ,(currMaxima * alpha) + result *(1-alpha));

        if(DEBUG_DETECTOR){
            LibCore::Event *tmpEvent = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), result);
            measurement.getEventChannel(debugChannel).append(*tmpEvent);
            if(result < currMaxima){
                LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), currMaxima);
                measurement.getEventChannel(debugChannel2).append(*tmpEvent2);
            }
        }
        //find actual peak on signal not derivative (first right peak from maxima)

        int signalPeak = maxima[i];
        if(derivative){
            if(measurement[numSelectedChannel][signalPeak]<measurement[numSelectedChannel][signalPeak+1]){
                while(measurement[numSelectedChannel][signalPeak]<measurement[numSelectedChannel][signalPeak+1])
                    signalPeak++;
            }else{
                while(measurement[numSelectedChannel][signalPeak]>measurement[numSelectedChannel][signalPeak+1])
                    signalPeak++;
            }
        }

        if(countDirectionUp>countDirectionDown+20 && !hysteresisDown){
            hysteresisUp = true;
        }
        if(countDirectionUp+20<countDirectionDown && !hysteresisUp){
            hysteresisDown = true;
        }

        //check if previous is smaller but very close ( < 0.11s before and smaller in value)
        if(peakIndex.size() > 0 && (measurement.sampleIndex2Time(signalPeak) - peakIndex.back().time < 0.11) && peakIndex.back().value < result){
            if(samples[signalPeak]<samples[signalPeak+1])
                countDirectionUp+=2;
            else
                countDirectionDown+=2;

            if(DEBUG_DETECTOR){
                LibCore::Event *tmpEvent1 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), countDirectionUp);
                measurement.getEventChannel(debugUp).append(*tmpEvent1);
                LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), countDirectionDown);
                measurement.getEventChannel(debugDown).append(*tmpEvent2);
            }

            currMaxima = result;
            peakIndex.back().time = measurement.sampleIndex2Time(signalPeak);
            peakIndex.back().value = result;
            LastBeatIndex = signalPeak;
            LastBeatMaximaIndex = maxima[i];
            if(DEBUG_DETECTOR){
                LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), currMaxima);
                measurement.getEventChannel(debugChannel2).append(*tmpEvent2);
            }            
            continue;
        }

        if(result > currMaxima
            //&& (peakIndex.size() == 0 || (peakIndex.size() > 0)) //TODO here compare neighbouring heights
        ){
            if(samples[signalPeak]<samples[signalPeak+1])
                countDirectionUp++;
            else
                countDirectionDown--;

            if(DEBUG_DETECTOR){
                LibCore::Event *tmpEvent1 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), countDirectionUp);
                measurement.getEventChannel(debugUp).append(*tmpEvent1);
                LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), countDirectionDown);
                measurement.getEventChannel(debugDown).append(*tmpEvent2);
            }

            /* //uncomment when this will work as expected
            if(hysteresisDown && samples[signalPeak]>0){
                if(DEBUG_DETECTOR){
                    LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), currMaxima);
                    measurement.getEventChannel(debugChannel2).append(*tmpEvent2);
                }
                continue;
            }
            else if(hysteresisUp && samples[signalPeak]<0){
                if(DEBUG_DETECTOR){
                    LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), currMaxima);
                    measurement.getEventChannel(debugChannel2).append(*tmpEvent2);
                }
                continue;
            }
            */


            currMaxima = result;
            if(peakIndex.size() > 0 && measurement.sampleIndex2Time(signalPeak) == peakIndex.back().time){
                peakIndex.back().value = max(peakIndex.back().value, result);
                continue;
            }
            LibCore::Event *tmpEvent = new LibCore::Event(measurement.sampleIndex2Time(signalPeak), currMaxima);
            peakIndex.push_back(*tmpEvent);
            LastBeatIndex = signalPeak;
            LastBeatMaximaIndex = maxima[i];
            
            if(DEBUG_DETECTOR){
                LibCore::Event *tmpEvent2 = new LibCore::Event(measurement.sampleIndex2Time(maxima[i]), currMaxima);
                measurement.getEventChannel(debugChannel2).append(*tmpEvent2);
            }

            continue;
        }


    }

    //put peakIndex to event channel 
    for(int i = 0; i< peakIndex.size(); i++)
        measurement.getEventChannel(maximaChannel).append(peakIndex[i]);

    
    //remove what is very far from average
    LibCore::CEventChannel::Statistics stat = measurement.getEventChannel(maximaChannel).calcStatistics();
    measurement.getEventChannel(maximaChannel).removeExtremeEvents(stat.minValue/10, stat.maxValue*10);

    //smartly remove two events very close by
    measurement.getEventChannel(maximaChannel).removeExtremeRRIEvents(maxBPM);

    measurement.refineBeatRateWithInterpolationSameChannel(numSelectedChannel, maximaChannel);

    measurement.getEventChannel(maximaChannel).removeExtremeEvents(60/maxBPM, 60/minBPM);

    measurement.refineBeatRateWithInterpolationSameChannel(numSelectedChannel, maximaChannel);


    refreshWholeWindow(automatic);
    free(samples);

}

