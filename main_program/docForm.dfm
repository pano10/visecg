object DocumentForm: TDocumentForm
  Left = 0
  Top = 17
  Width = 958
  Height = 351
  Caption = 'DocumentForm'
  Color = clBtnFace
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  FormStyle = fsMDIChild
  Icon.Data = {
    0000010001002020000001002000A81000001600000028000000200000004000
    0000010020000000000080100000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000000000FFFF0000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000FFFF0000FFFF0000
    FFFF000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000FFFF0000FFFF0000
    FFFF000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000
    FFFF0000FFFF0000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF000000000000
    00000000FFFF0000FFFF00000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000FFFF0000FFFF0000FFFF000000000000
    00000000FFFF0000FFFF00000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000FFFF0000FFFF0000FFFF00000000000000000000
    0000000000000000FFFF0000FFFF000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000FFFF0000FFFF0000FFFF00000000000000000000
    0000000000000000FFFF0000FFFF0000FFFF0000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000FFFF0000FFFF0000FFFF0000000000000000000000000000
    000000000000000000000000FFFF0000FFFF0000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000000000
    000000000000000000000000FFFF0000FFFF0000FFFF00000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000FFFF0000FFFF0000FFFF000000000000000000000000000000000000
    00000000000000000000000000000000FFFF0000FFFF0000FFFF000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000000000
    00000000000000000000000000000000FFFF0000FFFF0000FFFF000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000000000FFFF0000
    FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000
    FFFF000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000FFFF0000FFFF0000
    FFFF0000FFFF0000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000FFFF0000FFFF0000
    FFFF000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000FFFF0000FFFF0000
    FFFF000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000000000FFFF0000
    FFFF0000FFFF0000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000
    FFFF000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000000000FFFF0000
    FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
    00000000000000000000000000000000FFFF0000FFFF0000FFFF000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000FFFF0000FFFF0000FFFF000000000000000000000000000000000000
    00000000000000000000000000000000FFFF0000FFFF0000FFFF000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000FFFF0000FFFF0000FFFF000000000000000000000000000000000000
    00000000000000000000000000000000FFFF0000FFFF0000FFFF000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000
    FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
    00000000FFFF0000FFFF0000FFFF000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000
    0000000000000000000000000000000000000000FFFF0000FFFF000000000000
    00000000FFFF0000FFFF00000000000000000000000000000000000000000000
    FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000
    FFFF00000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000000000
    00000000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF0000
    FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000FFFF0000FFFF0000
    FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000
    0000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000
    FFFF0000FFFF0000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000FFFF0000FFFF00000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000FFFF0000FFFF000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FFFF
    FFFFFFFEFFFFFFFC7FFFFFFC7FFFFFF83FFFFFF99FFFFFF19FFFFFE3CFFFFFE3
    C7FFFFC7E7FFFF87E3FFFF8FF1FFFF0FF1FFFF1FF8FFFE1FF87FFC3FFC7FFC7F
    FE3FF87FFE1FF8FFFF1FF1FFFF8FF1FFFF8FF1F81F8FF8F99F1FF861861FFC03
    C03FFF9FF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Scaled = False
  Visible = True
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object lblStartDateTime: TLabel
    Left = 8
    Top = 280
    Width = 92
    Height = 15
    Caption = 'Za�etek meritve: '
  end
  object lblStartDateTimeVar: TLabel
    Left = 104
    Top = 280
    Width = 110
    Height = 15
    Caption = 'lblStartDateTimeVar'
  end
  object Chart: TChart
    Left = 248
    Top = 40
    Width = 347
    Height = 377
    Cursor = crCross
    AllowPanning = pmVertical
    AllowZoom = False
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    MarginTop = 3
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.LabelStyle = talValue
    BottomAxis.MinorGrid.Color = clGray
    BottomAxis.MinorGrid.Style = psDot
    BottomAxis.Title.Caption = '�as od za�etka meritve [s]'
    LeftAxis.MinorGrid.Color = clGray
    LeftAxis.MinorGrid.Style = psDot
    Legend.Font.Charset = EASTEUROPE_CHARSET
    Legend.Font.Color = clBlack
    Legend.Font.Height = -11
    Legend.Font.Name = 'Arial'
    Legend.Font.Style = []
    Legend.LegendStyle = lsSeries
    Legend.Visible = False
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.AutomaticMinimum = False
    RightAxis.Grid.Visible = False
    RightAxis.LabelStyle = talValue
    RightAxis.MinorTicks.Visible = False
    RightAxis.Ticks.Visible = False
    RightAxis.TicksInner.Visible = False
    TopAxis.Automatic = False
    TopAxis.AutomaticMaximum = False
    TopAxis.AutomaticMinimum = False
    TopAxis.Axis.Visible = False
    TopAxis.Grid.Visible = False
    TopAxis.MinorTicks.Visible = False
    TopAxis.TickInnerLength = 10
    TopAxis.TickLength = 3
    TopAxis.Ticks.Width = 2
    TopAxis.TicksInner.SmallDots = True
    View3D = False
    OnAfterDraw = ChartAfterDraw
    TabOrder = 0
    OnMouseMove = ChartMouseMove
    OnMouseUp = ChartMouseUp
    OnMouseWheel = ChartMouseWheel
  end
  object horzScroll: TScrollBar
    Left = 248
    Top = 408
    Width = 339
    Height = 16
    LargeChange = 1000
    Max = 100000
    PageSize = 0
    SmallChange = 10
    TabOrder = 1
    TabStop = False
    OnChange = horzScrollChange
  end
  object gridChannelView: TStringGrid
    Left = 0
    Top = 40
    Width = 241
    Height = 233
    DefaultColWidth = 90
    DefaultRowHeight = 18
    RowCount = 2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Small Fonts'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goEditing, goTabs, goAlwaysShowEditor]
    ParentFont = False
    ParentShowHint = False
    ScrollBars = ssVertical
    ShowHint = False
    TabOrder = 2
    OnDblClick = gridChannelViewDblClick
    OnDrawCell = gridChannelViewDrawCell
    OnMouseMove = gridChannelViewMouseMove
    OnMouseWheelDown = gridChannelViewMouseWheelDown
    OnMouseWheelUp = gridChannelViewMouseWheelUp
    OnSetEditText = gridChannelViewSetEditText
    RowHeights = (
      18
      18)
  end
  object ToolBar: TToolBar
    Left = 0
    Top = 0
    Width = 933
    Height = 59
    AutoSize = True
    BorderWidth = 1
    ButtonWidth = 57
    Caption = 'ToolBar'
    EdgeBorders = [ebTop, ebBottom]
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    ShowCaptions = True
    TabOrder = 3
    object btnUnzoom: TToolButton
      Left = 0
      Top = 2
      Hint = 'Zmanj�aj po x za dvakrat'
      Caption = '-(x2)'
      ImageIndex = 1
      OnClick = btnUnzoomClick
    end
    object btnZoom: TToolButton
      Left = 57
      Top = 2
      Hint = 'Pove�aj po x za dvakrat'
      Caption = '+ (x2)'
      ImageIndex = 0
      OnClick = btnZoomClick
    end
    object btnZoomCursor: TToolButton
      Left = 114
      Top = 2
      Hint = 
        'Prika�i od prvega do zadnjega kurzorja oz. okolico prvega, �e ob' +
        'staja le eden'
      Caption = '+ (kurzor)'
      ImageIndex = 8
      OnClick = btnZoomCursorClick
    end
    object ToolButton5: TToolButton
      Left = 171
      Top = 2
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object show10secBtn: TButton
      Left = 179
      Top = 2
      Width = 50
      Height = 22
      Caption = '10 s'
      TabOrder = 1
      OnClick = show10secBtnClick
    end
    object show30secBtn: TButton
      Left = 229
      Top = 2
      Width = 50
      Height = 22
      Caption = '30 s'
      TabOrder = 2
      OnClick = show30secBtnClick
    end
    object show60secBtn: TButton
      Left = 279
      Top = 2
      Width = 50
      Height = 22
      Caption = '60 s'
      TabOrder = 3
      OnClick = show60secBtnClick
    end
    object show10minbutton: TButton
      Left = 329
      Top = 2
      Width = 50
      Height = 22
      Caption = '10 min'
      TabOrder = 4
      OnClick = show10minbuttonClick
    end
    object show30minbutton: TButton
      Left = 379
      Top = 2
      Width = 50
      Height = 22
      Caption = '30 min'
      TabOrder = 5
      OnClick = show30minbuttonClick
    end
    object Button3: TButton
      Left = 429
      Top = 2
      Width = 50
      Height = 22
      Caption = '1 h'
      TabOrder = 6
      OnClick = Button3Click
    end
    object show4hbutton: TButton
      Left = 479
      Top = 2
      Width = 50
      Height = 22
      Caption = '4 h '
      TabOrder = 7
      OnClick = show4hbuttonClick
    end
    object btnViewAll: TToolButton
      Left = 529
      Top = 2
      Hint = 'Prika�i celotno dol�ino'
      Caption = 'vse'
      ImageIndex = 2
      OnClick = btnViewAllClick
    end
    object ToolButton2: TToolButton
      Left = 0
      Top = 2
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 5
      Wrap = True
      Style = tbsSeparator
    end
    object btnTrueYscale: TToolButton
      Left = 0
      Top = 29
      Hint = 'Prika�i okno za nastavljanje lastnosti y-osi'
      Caption = 'Uredi y-os'
      ImageIndex = 8
      OnClick = btnTrueYscaleClick
    end
    object ToolButton4: TToolButton
      Left = 57
      Top = 29
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object btnShowChannelTable: TToolButton
      Left = 65
      Top = 29
      Hint = 'Prika�i/skrij tabelo kanalov'
      Caption = 'Tabela'
      ImageIndex = 4
      Style = tbsCheck
      OnClick = btnShowChannelTableClick
    end
    object btnShowLegend: TToolButton
      Left = 122
      Top = 29
      Hint = 'Prika�i/skrij legendo v grafu'
      Caption = 'Legenda'
      ImageIndex = 3
      Style = tbsCheck
      OnClick = btnShowLegendClick
    end
    object ToolButton1: TToolButton
      Left = 179
      Top = 29
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 3
      Style = tbsSeparator
    end
    object btnShowAllChannels: TToolButton
      Left = 187
      Top = 29
      Hint = 'Prika�i vse kanale'
      Caption = 'Pok. vse'
      ImageIndex = 5
      OnClick = btnShowAllChannelsClick
    end
    object btnHideAllChannels: TToolButton
      Left = 244
      Top = 29
      Hint = 'Skrij vse kanale razen izbranega'
      Caption = 'Skrij vse'
      ImageIndex = 6
      OnClick = btnHideAllChannelsClick
    end
    object ToolButton3: TToolButton
      Left = 301
      Top = 29
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object comboCursorColor: TComboBox
      Left = 309
      Top = 29
      Width = 59
      Height = 22
      Hint = 'Barva kurzorja, ki ga postavljamo z levim gumbom'
      Style = csDropDownList
      DropDownCount = 3
      ItemHeight = 14
      TabOrder = 0
      Items.Strings = (
        'moder'
        'zelen'
        'rde�')
    end
    object btnRemoveAllCursors: TToolButton
      Left = 368
      Top = 29
      Hint = 'Odstrani vse tri kurzorje'
      Caption = 'Odstr. k.'
      ImageIndex = 7
      OnClick = btnRemoveAllCursorsClick
    end
  end
  object groupCursors: TGroupBox
    Left = 0
    Top = 296
    Width = 241
    Height = 129
    Caption = 'Kurzorji'
    TabOrder = 4
    object lblBlueCursor: TLabel
      Left = 8
      Top = 16
      Width = 76
      Height = 15
      Caption = 'lblBlueCursor'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lblGreenCursor: TLabel
      Left = 8
      Top = 32
      Width = 85
      Height = 15
      Caption = 'lblGreenCursor'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clGreen
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lblRedCursor: TLabel
      Left = 8
      Top = 48
      Width = 74
      Height = 15
      Caption = 'lblRedCursor'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 72
      Width = 40
      Height = 15
      Caption = 'moder-'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 48
      Top = 72
      Width = 32
      Height = 15
      Caption = 'zelen:'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clGreen
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lblBlueGreenDiff: TLabel
      Left = 80
      Top = 72
      Width = 90
      Height = 15
      Caption = 'lblBlueGreenDiff'
    end
    object Label3: TLabel
      Left = 8
      Top = 88
      Width = 40
      Height = 15
      Caption = 'moder-'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 48
      Top = 88
      Width = 27
      Height = 15
      Caption = 'rde�:'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lblBlueRedDiff: TLabel
      Left = 80
      Top = 88
      Width = 79
      Height = 15
      Caption = 'lblBlueRedDiff'
    end
    object Label6: TLabel
      Left = 8
      Top = 104
      Width = 33
      Height = 15
      Caption = 'zelen-'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clGreen
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 48
      Top = 104
      Width = 27
      Height = 15
      Caption = 'rde�:'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lblGreenRedDiff: TLabel
      Left = 80
      Top = 104
      Width = 88
      Height = 15
      Caption = 'lblGreenRedDiff'
    end
  end
end
