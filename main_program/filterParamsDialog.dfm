object filterParamsDlg: TfilterParamsDlg
  Left = 451
  Top = 430
  Width = 185
  Height = 195
  AutoSize = True
  Caption = 'Filtriranje'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel
    Left = 0
    Top = 0
    Width = 73
    Height = 49
    AutoSize = False
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 161
    Height = 13
    Caption = 'Frekvenca za nizkoprepustni filter:'
  end
  object Label2: TLabel
    Left = 104
    Top = 112
    Width = 73
    Height = 49
    AutoSize = False
  end
  object RadioGroup1: TRadioGroup
    Left = 8
    Top = 56
    Width = 161
    Height = 65
    Caption = 'Okno'
    TabOrder = 0
  end
  object radioRectangularWindow: TRadioButton
    Left = 16
    Top = 72
    Width = 113
    Height = 17
    Caption = 'pravokotno'
    TabOrder = 1
  end
  object radioTriangularWindow: TRadioButton
    Left = 16
    Top = 96
    Width = 113
    Height = 17
    Caption = 'trikotno'
    Checked = True
    TabOrder = 2
    TabStop = True
  end
  object editFrequency: TEdit
    Left = 88
    Top = 24
    Width = 81
    Height = 21
    TabOrder = 3
    Text = '50'
  end
  object BitBtn1: TBitBtn
    Left = 104
    Top = 128
    Width = 67
    Height = 25
    TabOrder = 4
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 8
    Top = 128
    Width = 67
    Height = 25
    TabOrder = 5
    Kind = bkCancel
  end
end
