object EventReportSetting: TEventReportSetting
  Left = 510
  Top = 243
  Width = 713
  Height = 579
  VertScrollBar.Visible = False
  AutoSize = True
  Caption = 'EventReportSetting'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 0
    Width = 239
    Height = 13
    Caption = 'Izberite dogodek katerega �elite prikazativ poro�ilu'
  end
  object Label2: TLabel
    Left = 368
    Top = 24
    Width = 79
    Height = 13
    Caption = 'Izberite dogodek'
  end
  object Label3: TLabel
    Left = 0
    Top = 24
    Width = 71
    Height = 13
    Caption = 'Izberite meritev'
  end
  object Label4: TLabel
    Left = 64
    Top = 328
    Width = 156
    Height = 26
    Caption = 'Vpi�ite dol�ino prikazane meritve okoli dogodka (v minutah): '
    WordWrap = True
  end
  object Label6: TLabel
    Left = 64
    Top = 400
    Width = 31
    Height = 13
    Caption = 'Avtor: '
  end
  object Label7: TLabel
    Left = 368
    Top = 360
    Width = 52
    Height = 13
    Caption = 'Uporabnik:'
  end
  object Label8: TLabel
    Left = 384
    Top = 384
    Width = 33
    Height = 13
    Caption = 'Starost'
  end
  object Label9: TLabel
    Left = 392
    Top = 408
    Width = 24
    Height = 13
    Caption = 'Te�a'
  end
  object ErrorMsgLabel: TLabel
    Left = 368
    Top = 64
    Width = 141
    Height = 13
    Caption = 'Meritev ne vsebuje dogodkov'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 392
    Top = 432
    Width = 24
    Height = 13
    Caption = ' Spol'
  end
  object Label10: TLabel
    Left = 392
    Top = 456
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Rojen'
  end
  object Label11: TLabel
    Left = 337
    Top = 480
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pozicija elektrode'
  end
  object ComboBox1: TComboBox
    Left = 0
    Top = 40
    Width = 337
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    OnChange = ComboBox1Change
  end
  object ComboBox2: TComboBox
    Left = 368
    Top = 40
    Width = 337
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    OnChange = ComboBox2Change
  end
  object Details1: TMemo
    Left = 0
    Top = 80
    Width = 337
    Height = 241
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object Details2: TMemo
    Left = 368
    Top = 80
    Width = 337
    Height = 241
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 3
  end
  object OkButton: TButton
    Left = 440
    Top = 520
    Width = 145
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 4
  end
  object CancelButton: TButton
    Left = 72
    Top = 520
    Width = 145
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object timeMinutes: TEdit
    Left = 200
    Top = 344
    Width = 33
    Height = 21
    TabOrder = 6
    Text = '4'
  end
  object AuthorInp: TEdit
    Left = 104
    Top = 392
    Width = 121
    Height = 21
    TabOrder = 7
    Text = 'AuthorInp'
  end
  object UserInp: TEdit
    Left = 432
    Top = 352
    Width = 241
    Height = 21
    TabOrder = 8
  end
  object AgeInp: TEdit
    Left = 432
    Top = 376
    Width = 89
    Height = 21
    TabOrder = 9
  end
  object WeightInp: TEdit
    Left = 432
    Top = 400
    Width = 89
    Height = 21
    TabOrder = 10
  end
  object GenderInp: TEdit
    Left = 432
    Top = 424
    Width = 121
    Height = 21
    TabOrder = 11
  end
  object BirthInp: TEdit
    Left = 432
    Top = 448
    Width = 121
    Height = 21
    TabOrder = 12
  end
  object PositionInp: TEdit
    Left = 432
    Top = 472
    Width = 121
    Height = 21
    TabOrder = 13
  end
end
