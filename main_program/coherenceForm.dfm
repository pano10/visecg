object CoherenceParamsForm: TCoherenceParamsForm
  Left = 516
  Top = 384
  Width = 321
  Height = 235
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Parametri koherence'
  Color = clBtnFace
  Constraints.MinHeight = 31
  Constraints.MinWidth = 160
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 71
    Width = 183
    Height = 13
    Caption = 'Vnesi �eleno �tevilo oken (od 1 do 15 )'
  end
  object Label2: TLabel
    Left = 5
    Top = 103
    Width = 185
    Height = 13
    Caption = 'Vnesi �eleno prekrivanje (med 0 in 0.8 )'
  end
  object Label3: TLabel
    Left = 5
    Top = 7
    Width = 211
    Height = 13
    Caption = 'Vnesi �tevilko prvega dogodkovnega kanala'
  end
  object Label4: TLabel
    Left = 5
    Top = 39
    Width = 217
    Height = 13
    Caption = 'Vnesi �tevilko drugega dogodkovnega kanala'
  end
  object ErrorLabel: TLabel
    Left = 29
    Top = 177
    Width = 168
    Height = 13
    Caption = 'Napake v vne�enih podatkih!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Dummy: TLabel
    Left = 0
    Top = 0
    Width = 3
    Height = 13
    AutoSize = False
  end
  object Label13: TLabel
    Left = 240
    Top = 160
    Width = 73
    Height = 41
    AutoSize = False
  end
  object Label5: TLabel
    Left = 5
    Top = 135
    Width = 159
    Height = 13
    Caption = 'Prika�i frekvence med 0 in ... [Hz]'
  end
  object OverlapEdit: TEdit
    Left = 237
    Top = 99
    Width = 65
    Height = 21
    TabOrder = 2
    Text = '0.5'
    OnChange = OverlapEditChange
  end
  object OKBitBtn: TBitBtn
    Left = 149
    Top = 171
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object CancelBitBtn: TBitBtn
    Left = 229
    Top = 171
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Prekli�i'
    ModalResult = 2
    TabOrder = 4
    NumGlyphs = 2
  end
  object Channel1Edit: TEdit
    Left = 237
    Top = 3
    Width = 49
    Height = 21
    TabOrder = 6
    Text = '0'
    OnChange = Channel1EditChange
  end
  object Channel1UpDown: TUpDown
    Left = 286
    Top = 3
    Width = 16
    Height = 21
    Associate = Channel1Edit
    Min = 0
    Position = 0
    TabOrder = 5
    Wrap = False
  end
  object Channel2Edit: TEdit
    Left = 237
    Top = 35
    Width = 49
    Height = 21
    TabOrder = 0
    Text = '0'
    OnChange = Channel2EditChange
  end
  object NumOfWindowsEdit: TEdit
    Left = 237
    Top = 67
    Width = 49
    Height = 21
    TabOrder = 1
    Text = '0'
    OnChange = NumOfWindowsEditChange
  end
  object NumOfWindowsUpDown: TUpDown
    Left = 286
    Top = 67
    Width = 16
    Height = 21
    Associate = NumOfWindowsEdit
    Min = 0
    Position = 0
    TabOrder = 7
    Wrap = False
  end
  object Channel2UpDown: TUpDown
    Left = 286
    Top = 35
    Width = 16
    Height = 21
    Associate = Channel2Edit
    Min = 0
    Position = 0
    TabOrder = 8
    Wrap = False
  end
  object EditMaxFreq: TEdit
    Left = 237
    Top = 131
    Width = 65
    Height = 21
    TabOrder = 9
    Text = '0.5'
    OnChange = EditMaxFreqChange
  end
end
