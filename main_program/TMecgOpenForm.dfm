object MecgOpenForm: TMecgOpenForm
  Left = 445
  Top = 404
  Width = 320
  Height = 169
  Caption = 'Odpiranje MECG datoteke'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object DescriptionLabel: TLabel
    Left = 8
    Top = 8
    Width = 279
    Height = 13
    Caption = 
      'Vpi�i �tevilke kanalov, ki jih �eli� uvoziti, lo�ene s presledki' +
      '.'
  end
  object Label1: TLabel
    Left = 8
    Top = 32
    Width = 129
    Height = 13
    Caption = '�tevilo kanalov v datoteki: '
  end
  object NumChannelsLabel: TLabel
    Left = 144
    Top = 32
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object SaveSettingsCheckBox: TCheckBox
    Left = 8
    Top = 80
    Width = 297
    Height = 17
    Caption = 'Nastavi izbrane kanale za privzete.'
    TabOrder = 1
  end
  object ChannelNumInput: TEdit
    Left = 8
    Top = 56
    Width = 297
    Height = 21
    TabOrder = 0
  end
  object OkBitBtn1: TBitBtn
    Left = 8
    Top = 104
    Width = 75
    Height = 25
    TabOrder = 2
    Kind = bkOK
  end
  object CancelBitBtn: TBitBtn
    Left = 232
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Prekli�i'
    TabOrder = 3
    Kind = bkCancel
  end
  object SelectAllBitBtn: TBitBtn
    Left = 208
    Top = 24
    Width = 97
    Height = 25
    Caption = 'Izberi vse kanale'
    TabOrder = 4
    OnClick = SelectAllBitBtnClick
  end
end
