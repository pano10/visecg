object EulaForm: TEulaForm
  Left = 387
  Top = 180
  Width = 445
  Height = 579
  Caption = 'Pravno obvestilo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 411
    Height = 13
    Caption = 
      'Pozorno preberite spodnji tekst. V primeru, da se z njim ne stri' +
      'njate, se bo program zaprl.'
  end
  object BitBtn1: TBitBtn
    Left = 256
    Top = 504
    Width = 75
    Height = 25
    Caption = 'Da'
    TabOrder = 0
    Kind = bkYes
  end
  object BitBtn2: TBitBtn
    Left = 88
    Top = 504
    Width = 75
    Height = 25
    Caption = 'Ne'
    TabOrder = 1
    Kind = bkNo
  end
  object Memo1: TRichEdit
    Left = 16
    Top = 48
    Width = 401
    Height = 441
    HideScrollBars = False
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 2
  end
end
