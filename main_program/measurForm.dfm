object MeasureForm: TMeasureForm
  Left = 904
  Top = 158
  BorderStyle = bsDialog
  Caption = 'Nova meritev'
  ClientHeight = 440
  ClientWidth = 239
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 92
    Height = 13
    Caption = 'Trajanje meritve (s):'
  end
  object spinDuration: TCSpinEdit
    Left = 112
    Top = 8
    Width = 89
    Height = 22
    TabStop = True
    MaxValue = 7200
    ParentColor = False
    TabOrder = 0
    Value = 30
  end
  object checkManualStop: TCheckBox
    Left = 112
    Top = 40
    Width = 129
    Height = 17
    Caption = 'do ro�ne zaustavitve'
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 64
    Width = 225
    Height = 337
    Caption = 'Merjeni kanali'
    TabOrder = 2
    object Label2: TLabel
      Left = 8
      Top = 24
      Width = 37
      Height = 13
      Caption = 'Vrata 1:'
    end
    object Label3: TLabel
      Left = 8
      Top = 80
      Width = 37
      Height = 13
      Caption = 'Vrata 2:'
    end
    object Label4: TLabel
      Left = 8
      Top = 208
      Width = 37
      Height = 13
      Caption = 'Vrata 3:'
    end
    object checkColinDigital: TCheckBox
      Left = 24
      Top = 48
      Width = 177
      Height = 17
      Caption = 'COLIN digital'
      Enabled = False
      TabOrder = 0
    end
    object checkPressure: TCheckBox
      Left = 24
      Top = 104
      Width = 185
      Height = 17
      Caption = 'tlak'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object checkEKG: TCheckBox
      Left = 24
      Top = 232
      Width = 193
      Height = 17
      Caption = 'EKG (trije kanali)'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 2
    end
    object checkP4: TCheckBox
      Left = 8
      Top = 280
      Width = 145
      Height = 17
      Caption = 'aux digital (vrata 4)'
      Enabled = False
      TabOrder = 3
      Visible = False
    end
    object checkMarkers: TCheckBox
      Left = 8
      Top = 312
      Width = 169
      Height = 17
      Caption = 'zaznamki (tipke 1-9)'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object checkCH2: TCheckBox
      Left = 24
      Top = 128
      Width = 137
      Height = 17
      Caption = 'analogni kanal CH2'
      TabOrder = 5
    end
    object checkCH3: TCheckBox
      Left = 24
      Top = 152
      Width = 121
      Height = 17
      Caption = 'analogni kanal CH3'
      TabOrder = 6
    end
    object checkCH4: TCheckBox
      Left = 24
      Top = 176
      Width = 121
      Height = 17
      Caption = 'analogni kanal CH4'
      TabOrder = 7
    end
    object checkBreathing: TCheckBox
      Left = 24
      Top = 256
      Width = 97
      Height = 17
      Caption = 'dihanje'
      TabOrder = 8
    end
  end
  object BitBtn1: TBitBtn
    Left = 160
    Top = 408
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 80
    Top = 408
    Width = 75
    Height = 25
    TabOrder = 4
    Kind = bkCancel
  end
end
