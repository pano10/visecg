object dialogViewY: TdialogViewY
  Left = 3
  Top = 552
  BorderStyle = bsDialog
  Caption = 'Lastnosti y-osi'
  ClientHeight = 228
  ClientWidth = 396
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 377
    Height = 73
    Caption = 'Napisi na y-osi:'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object radioUntrueYscale: TRadioButton
      Left = 16
      Top = 24
      Width = 233
      Height = 17
      Hint = 
        'Ostali kanali se prika�ejo glede na y-zoom in offset: �e ima kan' +
        'al n-krat ve�ji y-zoom od izbranega, na zaslonu pokrije n-krat v' +
        'e�ji prostor; �e ima kanal offset 1, njegova najni�ja vrednost n' +
        'a ekranu sovpade z najvi�jo vrednostjo prej�njega kanala.'
      Caption = 'ustrezajo &izbranemu kanalu'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = radioUntrueYscaleClick
    end
    object radioTrueYscale: TRadioButton
      Left = 16
      Top = 48
      Width = 289
      Height = 17
      Hint = 
        'Vsi vidni kanali so narisani na enotni y-osi in se prekrivajo na' +
        ' zaslonu, �e se prekrivajo njihove vrednosti.'
      Caption = 'ustrezajo &vsem kanalom (y-zoom in offset se ignorira)'
      TabOrder = 1
      OnClick = radioTrueYscaleClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 88
    Width = 377
    Height = 97
    Caption = 'Na y-osi je prikazano:'
    TabOrder = 2
    object radioViewAll: TRadioButton
      Left = 16
      Top = 24
      Width = 289
      Height = 17
      Hint = 
        'Prika�i celotno podro�je od minimalne vrednosti v celi meritvi d' +
        'o maksimalne vrednosti v celi meritvi. Premikanje in zoom po x-o' +
        'si ne spreminja y-osi.'
      Caption = 'y-podro�je vseh vidnih kanalov v &celotni meritvi'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = radioViewAllClick
    end
    object radioViewCurrent: TRadioButton
      Left = 16
      Top = 48
      Width = 345
      Height = 17
      Hint = 
        'Prika�i podro�je od minimalne do maksimalne vrednosti v trenutno' +
        ' prikazanem delu meritve. Premikanje in zoom po x-osi avtomatsko' +
        ' spreminja y-os.'
      Caption = 
        'y-podro�je vseh vidnih kanalov na &trenutno prikazanem delu meri' +
        'tve'
      TabOrder = 1
      OnClick = radioViewCurrentClick
    end
    object radioViewManual: TRadioButton
      Left = 16
      Top = 72
      Width = 113
      Height = 17
      Hint = 
        'Ro�no nastavljanje: shift+kole��ek na mi�ki (premikanje), shift+' +
        'ctrl+kole��ek (zoom). Pri ostalih dveh izbirah ro�no spreminjanj' +
        'e ni mo�no.'
      Caption = '&ro�no'
      TabOrder = 2
      OnClick = radioViewManualClick
    end
  end
  object BitBtn1: TBitBtn
    Left = 312
    Top = 192
    Width = 75
    Height = 25
    Caption = '&Zapri'
    TabOrder = 0
    Kind = bkClose
  end
end
