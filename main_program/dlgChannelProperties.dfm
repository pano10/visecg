object dlgChannelProps: TdlgChannelProps
  Left = 282
  Top = 98
  Width = 663
  Height = 431
  Caption = 'Lastnosti kanala'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object labelChannelType: TLabel
    Left = 8
    Top = 24
    Width = 66
    Height = 13
    Caption = 'Signalni kanal'
  end
  object Label1: TLabel
    Left = 8
    Top = 48
    Width = 20
    Height = 13
    Caption = 'Ime:'
  end
  object Label2: TLabel
    Left = 8
    Top = 112
    Width = 48
    Height = 13
    Caption = 'Komentar:'
  end
  object Label3: TLabel
    Left = 8
    Top = 80
    Width = 31
    Height = 13
    Caption = 'Enota:'
  end
  object editChannelName: TEdit
    Left = 64
    Top = 48
    Width = 441
    Height = 21
    TabOrder = 0
  end
  object checkContiguous: TCheckBox
    Left = 120
    Top = 24
    Width = 97
    Height = 17
    Caption = 'zvezen'
    TabOrder = 1
    Visible = False
  end
  object memoComment: TMemo
    Left = 64
    Top = 112
    Width = 449
    Height = 153
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object BitBtn1: TBitBtn
    Left = 440
    Top = 272
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 360
    Top = 272
    Width = 75
    Height = 25
    TabOrder = 4
    Kind = bkCancel
  end
  object editUnit: TEdit
    Left = 64
    Top = 80
    Width = 161
    Height = 21
    TabOrder = 5
  end
  object checkSmallDots: TCheckBox
    Left = 240
    Top = 24
    Width = 97
    Height = 17
    Caption = 'male pike'
    TabOrder = 6
    Visible = False
  end
end
