object FolderOverviewSettings: TFolderOverviewSettings
  Left = 670
  Top = 142
  Width = 297
  Height = 291
  AutoSize = True
  Caption = 'Nastavitve pregleda'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 8
    Width = 81
    Height = 13
    Caption = 'Nizka meja BPM:'
  end
  object Label2: TLabel
    Left = 0
    Top = 48
    Width = 86
    Height = 13
    Caption = 'Visoka meja BPM:'
  end
  object Label3: TLabel
    Left = 24
    Top = 80
    Width = 179
    Height = 13
    Caption = 'Meja standardne deviacije BPM (v %):'
  end
  object Label4: TLabel
    Left = 32
    Top = 96
    Width = 157
    Height = 13
    Caption = '(Vrednost prikazana s sivo barvo)'
  end
  object Label5: TLabel
    Left = 32
    Top = 136
    Width = 200
    Height = 13
    Caption = 'Izbrana primarna lokacija pregleda datotek'
  end
  object Label6: TLabel
    Left = 8
    Top = 192
    Width = 144
    Height = 13
    Caption = 'Dol�ina BPM pregleda (v urah)'
  end
  object lowBPMinput: TEdit
    Left = 96
    Top = 0
    Width = 161
    Height = 21
    TabOrder = 0
    Text = 'lowBPMinput'
  end
  object highBPMinput: TEdit
    Left = 96
    Top = 40
    Width = 161
    Height = 21
    TabOrder = 1
    Text = 'highBPMinput'
  end
  object stdInput: TEdit
    Left = 56
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'stdInput'
  end
  object BitBtn1: TBitBtn
    Left = 168
    Top = 232
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 24
    Top = 232
    Width = 75
    Height = 25
    TabOrder = 4
    Kind = bkCancel
  end
  object folderLocation: TEdit
    Left = 0
    Top = 160
    Width = 185
    Height = 21
    TabOrder = 5
    Text = 'C.\VisECG\measurements'
  end
  object Button1: TButton
    Left = 192
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Izberi mapo'
    TabOrder = 6
    OnClick = Button1Click
  end
  object lengthBPM: TEdit
    Left = 168
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 7
    Text = 'lengthBPM'
  end
end
