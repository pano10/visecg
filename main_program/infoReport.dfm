object ReportInfoForm: TReportInfoForm
  Left = 526
  Top = 257
  Width = 203
  Height = 315
  AutoSize = True
  Caption = 'ReportInfoForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Avtor: '
  end
  object Label2: TLabel
    Left = 0
    Top = 32
    Width = 52
    Height = 13
    Caption = 'Uporabnik:'
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 33
    Height = 13
    Caption = 'Starost'
  end
  object Label4: TLabel
    Left = 24
    Top = 80
    Width = 24
    Height = 13
    Caption = 'Te�a'
  end
  object Label5: TLabel
    Left = 32
    Top = 104
    Width = 21
    Height = 13
    Caption = 'Spol'
  end
  object Label6: TLabel
    Left = 24
    Top = 128
    Width = 28
    Height = 13
    Caption = 'Rojen'
  end
  object Label7: TLabel
    Left = 8
    Top = 168
    Width = 97
    Height = 13
    Caption = 'Prika�i prvih meritev:'
  end
  object Label8: TLabel
    Left = 8
    Top = 208
    Width = 108
    Height = 13
    Caption = 'Prika�i prvih dogodkov'
  end
  object AuthorInp: TEdit
    Left = 64
    Top = 0
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object UserInp: TEdit
    Left = 64
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object AgeInp: TEdit
    Left = 64
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object WeightInp: TEdit
    Left = 64
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object BitBtn1: TBitBtn
    Left = 0
    Top = 256
    Width = 75
    Height = 25
    TabOrder = 4
    Kind = bkCancel
  end
  object BitBtn2: TBitBtn
    Left = 120
    Top = 256
    Width = 75
    Height = 25
    TabOrder = 5
    Kind = bkOK
  end
  object GenderInp: TEdit
    Left = 64
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object BirthInp: TEdit
    Left = 64
    Top = 120
    Width = 121
    Height = 21
    TabOrder = 7
  end
  object MeasurementNumberInput: TEdit
    Left = 64
    Top = 184
    Width = 121
    Height = 21
    TabOrder = 8
  end
  object EventNumberInput: TEdit
    Left = 64
    Top = 224
    Width = 121
    Height = 21
    TabOrder = 9
  end
end
