object dlgMeasurementData: TdlgMeasurementData
  Left = 386
  Top = 24
  BorderStyle = bsDialog
  Caption = 'Podatki o meritvi'
  ClientHeight = 709
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Microsoft Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefaultPosOnly
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 8
    Top = 8
    Width = 57
    Height = 13
    Caption = 'Ime meritve:'
  end
  object Label7: TLabel
    Left = 8
    Top = 64
    Width = 143
    Height = 13
    Caption = 'Datum in �as za�etka meritve:'
  end
  object labelSamplingRate: TLabel
    Left = 8
    Top = 168
    Width = 103
    Height = 13
    Caption = 'Frekvenca vzor�enja:'
  end
  object Label9: TLabel
    Left = 8
    Top = 88
    Width = 48
    Height = 13
    Caption = 'Komentar:'
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 384
    Width = 529
    Height = 281
    Caption = 'Podatki o pacientu'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 32
      Width = 67
      Height = 13
      Caption = 'Priimek in ime:'
    end
    object Label2: TLabel
      Left = 8
      Top = 64
      Width = 3
      Height = 13
    end
    object Label3: TLabel
      Left = 272
      Top = 72
      Width = 68
      Height = 13
      Caption = 'Datum rojstva:'
    end
    object Label4: TLabel
      Left = 8
      Top = 104
      Width = 48
      Height = 13
      Caption = 'Diagnoza:'
    end
    object Label5: TLabel
      Left = 8
      Top = 184
      Width = 48
      Height = 13
      Caption = 'Komentar:'
    end
    object editName: TEdit
      Left = 80
      Top = 28
      Width = 281
      Height = 21
      MaxLength = 50
      TabOrder = 0
    end
    object RadioGroup1: TRadioGroup
      Left = 8
      Top = 56
      Width = 241
      Height = 41
      Caption = 'Spol'
      TabOrder = 1
    end
    object radioFemale: TRadioButton
      Left = 24
      Top = 72
      Width = 65
      Height = 17
      Caption = '�enska'
      TabOrder = 2
    end
    object radioMale: TRadioButton
      Left = 96
      Top = 72
      Width = 65
      Height = 17
      Caption = 'mo�ki'
      TabOrder = 3
    end
    object radioUnspecified: TRadioButton
      Left = 152
      Top = 72
      Width = 89
      Height = 17
      Caption = 'nedefinirano'
      Checked = True
      TabOrder = 4
      TabStop = True
    end
    object memoDiagnosis: TMemo
      Left = 8
      Top = 120
      Width = 513
      Height = 57
      ScrollBars = ssVertical
      TabOrder = 5
    end
    object memoComment: TMemo
      Left = 8
      Top = 200
      Width = 513
      Height = 73
      ScrollBars = ssVertical
      TabOrder = 6
    end
    object dateOfBirthPicker: TDateTimePicker
      Left = 352
      Top = 68
      Width = 105
      Height = 21
      CalAlignment = dtaLeft
      Date = 2
      Time = 2
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      MaxDate = 73050
      MinDate = -7303
      ParseInput = False
      TabOrder = 7
    end
  end
  object btnOK: TBitBtn
    Left = 464
    Top = 672
    Width = 75
    Height = 25
    TabOrder = 1
    Kind = bkOK
  end
  object btnCancel: TBitBtn
    Left = 384
    Top = 672
    Width = 75
    Height = 25
    TabOrder = 2
    Kind = bkCancel
  end
  object editMeasurementName: TEdit
    Left = 8
    Top = 24
    Width = 529
    Height = 21
    TabOrder = 3
  end
  object datePicker: TDateTimePicker
    Left = 160
    Top = 60
    Width = 113
    Height = 21
    CalAlignment = dtaLeft
    Date = 38026.4699826505
    Time = 38026.4699826505
    DateFormat = dfShort
    DateMode = dmComboBox
    Enabled = False
    Kind = dtkDate
    ParseInput = False
    TabOrder = 4
  end
  object timePicker: TDateTimePicker
    Left = 280
    Top = 60
    Width = 98
    Height = 21
    CalAlignment = dtaLeft
    Date = 38026.4709772917
    Time = 38026.4709772917
    DateFormat = dfShort
    DateMode = dmComboBox
    Enabled = False
    Kind = dtkTime
    ParseInput = False
    TabOrder = 5
  end
  object memoMeasurementComment: TMemo
    Left = 8
    Top = 104
    Width = 529
    Height = 57
    ScrollBars = ssVertical
    TabOrder = 6
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 192
    Width = 529
    Height = 185
    Caption = 'Kanali ([dog.]�t., ime (enota))'
    TabOrder = 7
    object listChannels: TListBox
      Left = 8
      Top = 16
      Width = 513
      Height = 161
      ItemHeight = 13
      TabOrder = 0
    end
  end
end
